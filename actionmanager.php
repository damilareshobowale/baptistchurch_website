<?php
	require_once("config.php");
	require_once("inc_dbfunctions.php");
	
$actionmanager = New Actionmanager();

	
//Check what to do

if(isset($_POST['command']) && $_POST['command'] == "register")
{
	$actionmanager->register();
}
elseif(isset($_POST['command']) && $_POST['command'] == "admin_login")
{
	$actionmanager->admin_login();
}
elseif(isset($_POST['command']) && $_POST['command'] == "admin_news_add")
{
	$actionmanager->admin_news_add();
}
elseif(isset($_POST['command']) && $_POST['command'] == "admin_news_edit")
{
	$actionmanager->admin_news_edit();
}
elseif(isset($_POST['command']) && $_POST['command'] == "admin_events_add")
{
	$actionmanager->admin_events_add();
}
elseif(isset($_POST['command']) && $_POST['command'] == "admin_events_edit")
{
	$actionmanager->admin_events_edit();
}
elseif(isset($_POST['command']) && $_POST['command'] == "admin_causes_add")
{
	$actionmanager->admin_causes_add();
}
elseif(isset($_POST['command']) && $_POST['command'] == "admin_causes_edit")
{
	$actionmanager->admin_causes_edit();
}
elseif(isset($_POST['command']) && $_POST['command'] == "admin_ministries_add")
{
	$actionmanager->admin_ministries_add();
}
elseif(isset($_POST['command']) && $_POST['command'] == "admin_ministries_edit")
{
	$actionmanager->admin_ministries_edit();
}
elseif(isset($_POST['command']) && $_POST['command'] == "admin_sermons_add")
{
	$actionmanager->admin_sermons_add();
}
elseif(isset($_POST['command']) && $_POST['command'] == "admin_sermons_edit")
{
	$actionmanager->admin_sermons_edit();
}
elseif(isset($_POST['command']) && $_POST['command'] == "admin_blogs_add")
{
	$actionmanager->admin_blogs_add();
}
elseif(isset($_POST['command']) && $_POST['command'] == "admin_blogs_edit")
{
	$actionmanager->admin_blogs_edit();
}
elseif(isset($_POST['command']) && $_POST['command'] == "admin_articles_add")
{
	$actionmanager->admin_articles_add();
}
elseif(isset($_POST['command']) && $_POST['command'] == "admin_articles_edit")
{
	$actionmanager->admin_articles_edit();
}
elseif(isset($_POST['command']) && $_POST['command'] == "admin_store_add")
{
	$actionmanager->admin_store_add();
}
elseif(isset($_POST['command']) && $_POST['command'] == "admin_store_edit")
{
	$actionmanager->admin_store_edit();
}
elseif(isset($_POST['command']) && $_POST['command'] == "admin_branches_add")
{
	$actionmanager->admin_branches_add();
}
elseif(isset($_POST['command']) && $_POST['command'] == "admin_branches_edit")
{
	$actionmanager->admin_branches_edit();
}
elseif(isset($_POST['command']) && $_POST['command'] == "admin_staff_add")
{
	$actionmanager->admin_staff_add();
}
elseif(isset($_POST['command']) && $_POST['command'] == "admin_staff_edit")
{
	$actionmanager->admin_staff_edit();
}
elseif(isset($_POST['command']) && $_POST['command'] == "admin_pages_edit")
{
	$actionmanager->admin_pages_edit();
}
elseif(isset($_POST['command']) && $_POST['command'] == "admin_settings_update")
{
	$actionmanager->admin_settings_update();
}
elseif(isset($_POST['command']) && $_POST['command'] == "admin_testimonies_edit")
{
	$actionmanager->admin_testimonies_edit();
}
elseif(isset($_POST['command']) && $_POST['command'] == "prayerrequests_add")
{
	$actionmanager->prayerrequests_add();
}
elseif(isset($_POST['command']) && $_POST['command'] == "testimonies_add")
{
	$actionmanager->testimonies_add();
}
elseif(isset($_POST['command']) && $_POST['command'] == "donations_add")
{
	$actionmanager->donations_add();
}
elseif(isset($_POST['command']) && $_POST['command'] == "newsletter_subscribe")
{
    $actionmanager->newsletter_subscribe();
}

class Actionmanager
{

    //Process login form
    function admin_login()
    {
            $mycon = databaseConnect();
            $username = $_POST['username'];
            $password = $_POST['password'];
            $thedate = date("Y-m-d H:i:s");

            //check if account exists
            $sql = "SELECT * FROM `admin` WHERE `username` = :username AND `password` = :password LIMIT 1";
            //$sql = "SELECT * FROM `users` WHERE `username` = :username LIMIT 1";

            $stmt = $mycon->prepare($sql);

            $stmt->bindValue(":username",$username,PDO::PARAM_STR);
            $stmt->bindValue(":password",$password,PDO::PARAM_STR);

            $stmt->execute();

            if($stmt->rowCount() < 1)
            {
                    showAlert("You have provided an invalid login details. Please try again.");
                    return;
            }

            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            createCookie("userid",$userid);
            createCookie("userlogin","YES");
            createCookie("username",$username);
            createCookie("fullname",$row['title']." ".$row['surname']." ".$row['firstname']);

            createCookie("adminlogin","YES");

            openPage("admin/gallery_list.php");



    }
    //End proLogin()//////////////////



    //Process new news
    function admin_news_add()
    {
            $mycon = databaseConnect();

            $newsdate = $_POST['txtdate'];
            $headline = $_POST['txtheadline'];
            $content = $_POST['txtcontent'];
            $caption = $_POST['txtcaption'];
        $thedate = date("Y-m-d");
        $banner = $_FILES['picture'];

        //check if a page name was specified
        if(strlen($headline) < 1)
        {
            showAlert("Please enter the headline of this content before saving.");
            return;

        }

            $sql = "INSERT INTO `news` SET `newsdate` = :newsdate
                            ,`headline` = :headline
                            ,`caption` = :caption
                            ,`content` = :content
                            ,`thedate` = :thedate";
            $myrec =  $mycon->prepare($sql);
            $myrec->bindValue(":newsdate",$newsdate);
            $myrec->bindValue(":headline",$headline);
            $myrec->bindValue(":caption",$caption);
            $myrec->bindValue(":content",$content);
            $myrec->bindValue(":thedate",$thedate);
            $myrec->execute();

            if($myrec->rowCount() < 1)
            {
                    showAlert("There was an error saving this event. Please try again.");
                    return;
            }
            else
            {
                    $thecode = $mycon->lastInsertId();
                    if(strpos(strtoupper($banner['type']),"IMAGE") > -1) 
                    {
                            move_uploaded_file($banner['tmp_name'],"pictures/news/$thecode.jpg");
                    }

                    showAlert("Done!!");
                    openPage("admin/news_list.php");
            }
    }
    //End admin_news_add()/////////////////



    //Process edit news
    function admin_news_edit()
    {
            $mycon = databaseConnect();

            $thecode = $_POST['code'];
            $newsdate = $_POST['txtdate'];
            $headline = $_POST['txtheadline'];
            $content = $_POST['txtcontent'];
            $caption = $_POST['txtcaption'];
        $thedate = date("Y-m-d");
        $banner = $_FILES['picture'];

        //check if a page name was specified
        if(strlen($headline) < 1)
        {
            showAlert("Please enter the headline of this content before saving.");
            return;

        }

            $sql = "UPDATE `news` SET `newsdate` = :newsdate
                            ,`headline` = :headline
                            ,`caption` = :caption
                            ,`content` = :content
                            ,`thedate` = :thedate WHERE `code` = :code";
            $myrec =  $mycon->prepare($sql);
            $myrec->bindValue(":code",$thecode);
            $myrec->bindValue(":newsdate",$newsdate);
            $myrec->bindValue(":headline",$headline);
            $myrec->bindValue(":caption",$caption);
            $myrec->bindValue(":content",$content);
            $myrec->bindValue(":thedate",$thedate);
            $myrec->execute();

            if($myrec->rowCount() < 1)
            {
                    showAlert("There was an error saving this event. Please try again.");
                    return;
            }
            else
            {
                    if(strpos(strtoupper($banner['type']),"IMAGE") > -1) 
                    {
                            move_uploaded_file($banner['tmp_name'],"pictures/news/$thecode.jpg");
                    }

                    showAlert("Done!!");
                    openPage("admin/news_list.php");
            }
    }
    //End admin_news_edit()/////////////////



    //Process new events
    function admin_events_add()
    {
            $mycon = databaseConnect();

            $eventdate = $_POST['txtdate'];
            $title = $_POST['title'];
            $content = $_POST['details'];
            $caption = $_POST['caption'];
        $thedate = date("Y-m-d");
        $banner = $_FILES['picture'];

        //check if a page name was specified
        if(strlen($title) < 1)
        {
            showAlert("Please enter the title of this event before saving.");
            return;

        }

            $sql = "INSERT INTO `events` SET `eventdate` = :eventdate
                            ,`title` = :title
                            ,`caption` = :caption
                            ,`details` = :details
                            ,`thedate` = :thedate";
            $myrec =  $mycon->prepare($sql);
            $myrec->bindValue(":eventdate",$eventdate);
            $myrec->bindValue(":title",$title);
            $myrec->bindValue(":caption",$caption);
            $myrec->bindValue(":details",$details);
            $myrec->bindValue(":thedate",$thedate);
            $myrec->execute();

            if($myrec->rowCount() < 1)
            {
                    showAlert("There was an error saving this event. Please try again.");
                    return;
            }
            else
            {
                    $thecode = $mycon->lastInsertId();
                    if(strpos(strtoupper($banner['type']),"IMAGE") > -1) 
                    {
                            move_uploaded_file($banner['tmp_name'],"pictures/events/$thecode.jpg");
                    }

                    showAlert("Done!!");
                    openPage("admin/events_list.php");
            }
    }
    //End admin_events_add()/////////////////



    //Process new events
    function admin_events_edit()
    {
            $mycon = databaseConnect();

            $thecode = $_POST['code'];
            $eventdate = $_POST['txtdate'];
            $headline = $_POST['title'];
            $details = $_POST['details'];
            $caption = $_POST['caption'];
        $thedate = date("Y-m-d");
        $banner = $_FILES['picture'];

        //check if a page name was specified
        if(strlen($headline) < 1)
        {
            showAlert("Please enter the title of this event before saving.");
            return;

        }

            $sql = "UPDATE `events` SET `eventdate` = :eventdate
                            ,`title` = :title
                            ,`caption` = :caption
                            ,`details` = :details
                            ,`thedate` = :thedate WHERE `code` = :code";
            $myrec =  $mycon->prepare($sql);
            $myrec->bindValue(":code",$thecode);
            $myrec->bindValue(":eventdate",$eventdate);
            $myrec->bindValue(":title",$headline);
            $myrec->bindValue(":caption",$caption);
            $myrec->bindValue(":details",$details);
            $myrec->bindValue(":thedate",$thedate);

            if(!$myrec->execute())
            {
                    showAlert("There was an error saving this event. Please try again.");
                    return;
            }
            else
            {
                    if(strpos(strtoupper($banner['type']),"IMAGE") > -1) 
                    {
                            move_uploaded_file($banner['tmp_name'],"pictures/events/$thecode.jpg");
                    }

                    showAlert("Done!!");
                    openPage("admin/events_list.php");
            }
    }
    //End admin_news_edit()/////////////////


    //Process new cause
    function admin_causes_add()
    {
            $mycon = databaseConnect();

            $title = $_POST['txttitle'];
            $caption = $_POST['caption'];
            $details = $_POST['details'];
        $banner = $_FILES['picture'];

        //check if a page name was specified
        if(strlen($title) < 1)
        {
            showAlert("Please enter the title of this content before saving.");
            return;

        }

            $sql = "INSERT INTO `causes` SET `title` = :title
                            ,`caption` = :caption
                            ,`details` = :details";
            $myrec =  $mycon->prepare($sql);
            $myrec->bindValue(":title",$title);
            $myrec->bindValue(":caption",$caption);
            $myrec->bindValue(":details",$details);
            $myrec->execute();

            if($myrec->rowCount() < 1)
            {
                    showAlert("There was an error saving this content. Please try again.");
                    return;
            }
            else
            {
                    $thecode = $mycon->lastInsertId();
                    if(strpos(strtoupper($banner['type']),"IMAGE") > -1) 
                    {
                            move_uploaded_file($banner['tmp_name'],"pictures/causes/$thecode.jpg");
                    }

                    showAlert("Done!!");
                    openPage("admin/causes_list.php");
            }
    }
    //End admin_cause_add()/////////////////


    //Process update causes
    function admin_causes_edit()
    {
            $mycon = databaseConnect();

            $thecode = $_POST['code'];
            $title = $_POST['txttitle'];
            $caption = $_POST['caption'];
            $details = $_POST['details'];
        $banner = $_FILES['picture'];
            $position = $_POST['position'];

        //check if a page name was specified
        if(strlen($title) < 1)
        {
            showAlert("Please enter the title of this content before saving.");
            return;

        }

            $sql = "UPDATE `causes` SET `title` = :title
                            ,`caption` = :caption
                            ,`pos` = :position
                            ,`details` = :details WHERE `code` = :code";
            $myrec =  $mycon->prepare($sql);
            $myrec->bindValue(":code",$thecode);
            $myrec->bindValue(":title",$title);
            $myrec->bindValue(":position",$position);
            $myrec->bindValue(":caption",$caption);
            $myrec->bindValue(":details",$details);

            if(!$myrec->execute())
            {
                    showAlert("There was an error saving this content. Please try again.");
                    return;
            }
            else
            {
                    if(strpos(strtoupper($banner['type']),"IMAGE") > -1) 
                    {
                            move_uploaded_file($banner['tmp_name'],"pictures/causes/$thecode.jpg");
                    }

                    showAlert("Done!!");
                    openPage("admin/causes_list.php");
            }
    }
    //End admin_news_edit()/////////////////

    //Process new ministry
    function admin_ministries_add()
    {
            $mycon = databaseConnect();

            $title = $_POST['txttitle'];
            $caption = $_POST['caption'];
            $details = $_POST['details'];
        $banner = $_FILES['picture'];

        //check if a page name was specified
        if(strlen($title) < 1)
        {
            showAlert("Please enter the title of this content before saving.");
            return;

        }

            $sql = "INSERT INTO `ministries` SET `title` = :title
                            ,`caption` = :caption
                            ,`details` = :details";
            $myrec =  $mycon->prepare($sql);
            $myrec->bindValue(":title",$title);
            $myrec->bindValue(":caption",$caption);
            $myrec->bindValue(":details",$details);
            $myrec->execute();

            if($myrec->rowCount() < 1)
            {
                    showAlert("There was an error saving this content. Please try again.");
                    return;
            }
            else
            {
                    $thecode = $mycon->lastInsertId();
                    if(strpos(strtoupper($banner['type']),"IMAGE") > -1) 
                    {
                            move_uploaded_file($banner['tmp_name'],"pictures/ministries/$thecode.jpg");
                    }

                    showAlert("Done!!");
                    openPage("admin/ministries_list.php");
            }
    }
    //End admin_ministries_add()/////////////////

    //Process update ministries
    function admin_ministries_edit()
    {
            $mycon = databaseConnect();

            $thecode = $_POST['code'];
            $title = $_POST['txttitle'];
            $caption = $_POST['caption'];
            $details = $_POST['details'];
        $banner = $_FILES['picture'];
            $position = $_POST['position'];

        //check if a page name was specified
        if(strlen($title) < 1)
        {
            showAlert("Please enter the title of this content before saving.");
            return;

        }

            $sql = "UPDATE `ministries` SET `title` = :title
                            ,`caption` = :caption
                            ,`pos` = :position
                            ,`details` = :details WHERE `code` = :code";
            $myrec =  $mycon->prepare($sql);
            $myrec->bindValue(":code",$thecode);
            $myrec->bindValue(":title",$title);
            $myrec->bindValue(":position",$position);
            $myrec->bindValue(":caption",$caption);
            $myrec->bindValue(":details",$details);

            if(!$myrec->execute())
            {
                    showAlert("There was an error saving this content. Please try again.");
                    return;
            }
            else
            {
                    if(strpos(strtoupper($banner['type']),"IMAGE") > -1) 
                    {
                            move_uploaded_file($banner['tmp_name'],"pictures/ministries/$thecode.jpg");
                    }

                    showAlert("Done!!");
                    openPage("admin/ministries_list.php");
            }
    }
    //End admin_ministries_edit()/////////////////

    //Process new sermon
    function admin_sermons_add()
    {
            $mycon = databaseConnect();

            $title = $_POST['txttitle'];
            $caption = $_POST['caption'];
            $details = $_POST['details'];
            $thedate = $_POST['txtdate'];
        $banner = $_FILES['picture'];

        //check if a page name was specified
        if(strlen($title) < 1)
        {
            showAlert("Please enter the title of this content before saving.");
            return;

        }

            $sql = "INSERT INTO `sermons` SET `title` = :title
                            ,`caption` = :caption
                            ,`thedate` = :thedate
                            ,`details` = :details";
            $myrec =  $mycon->prepare($sql);
            $myrec->bindValue(":title",$title);
            $myrec->bindValue(":caption",$caption);
            $myrec->bindValue(":thedate",$thedate);
            $myrec->bindValue(":details",$details);
            $myrec->execute();

            if($myrec->rowCount() < 1)
            {
                    showAlert("There was an error saving this content. Please try again.");
                    return;
            }
            else
            {
                    $thecode = $mycon->lastInsertId();
                    if(strpos(strtoupper($banner['type']),"IMAGE") > -1) 
                    {
                            move_uploaded_file($banner['tmp_name'],"pictures/sermons/$thecode.jpg");
                    }

                    showAlert("Done!!");
                    openPage("admin/sermons_list.php");
            }
    }
    //End admin_sermons_add()/////////////////

    //Process update sermons
    function admin_sermons_edit()
    {
            $mycon = databaseConnect();

            $thecode = $_POST['code'];
            $title = $_POST['txttitle'];
            $caption = $_POST['caption'];
            $details = $_POST['details'];
            $thedate = $_POST['txtdate'];
        $banner = $_FILES['picture'];
            $position = $_POST['position'];

        //check if a page name was specified
        if(strlen($title) < 1)
        {
            showAlert("Please enter the title of this content before saving.");
            return;

        }

            $sql = "UPDATE `sermons` SET `title` = :title
                            ,`caption` = :caption
                            ,`pos` = :position
                            ,`thedate` = :thedate
                            ,`details` = :details WHERE `code` = :code";
            $myrec =  $mycon->prepare($sql);
            $myrec->bindValue(":code",$thecode);
            $myrec->bindValue(":title",$title);
            $myrec->bindValue(":position",$position);
            $myrec->bindValue(":thedate",$thedate);
            $myrec->bindValue(":caption",$caption);
            $myrec->bindValue(":details",$details);

            if(!$myrec->execute())
            {
                    showAlert("There was an error saving this content. Please try again.");
                    return;
            }
            else
            {
                    if(strpos(strtoupper($banner['type']),"IMAGE") > -1) 
                    {
                            move_uploaded_file($banner['tmp_name'],"pictures/sermons/$thecode.jpg");
                    }

                    showAlert("Done!!");
                    openPage("admin/sermons_list.php");
            }
    }
    //End admin_sermons_edit()/////////////////

    //Process new blog
    function admin_blogs_add()
    {
            $mycon = databaseConnect();

            $title = $_POST['txttitle'];
            $caption = $_POST['caption'];
            $details = $_POST['details'];
            $thedate = $_POST['txtdate'];
        $banner = $_FILES['picture'];

        //check if a page name was specified
        if(strlen($title) < 1)
        {
            showAlert("Please enter the title of this content before saving.");
            return;

        }

            $sql = "INSERT INTO `blogs` SET `title` = :title
                            ,`caption` = :caption
                            ,`thedate` = :thedate
                            ,`details` = :details";
            $myrec =  $mycon->prepare($sql);
            $myrec->bindValue(":title",$title);
            $myrec->bindValue(":caption",$caption);
            $myrec->bindValue(":thedate",$thedate);
            $myrec->bindValue(":details",$details);
            $myrec->execute();

            if($myrec->rowCount() < 1)
            {
                    showAlert("There was an error saving this content. Please try again.");
                    return;
            }
            else
            {
                    $thecode = $mycon->lastInsertId();
                    if(strpos(strtoupper($banner['type']),"IMAGE") > -1) 
                    {
                            move_uploaded_file($banner['tmp_name'],"pictures/blogs/$thecode.jpg");
                    }

                    showAlert("Done!!");
                    openPage("admin/blogs_list.php");
            }
    }
    //End admin_blogs_add()/////////////////

    //Process update blog
    function admin_blogs_edit()
    {
            $mycon = databaseConnect();

            $thecode = $_POST['code'];
            $title = $_POST['txttitle'];
            $caption = $_POST['caption'];
            $details = $_POST['details'];
            $thedate = $_POST['txtdate'];
        $banner = $_FILES['picture'];
            $position = $_POST['position'];

        //check if a page name was specified
        if(strlen($title) < 1)
        {
            showAlert("Please enter the title of this content before saving.");
            return;

        }

            $sql = "UPDATE `blogs` SET `title` = :title
                            ,`caption` = :caption
                            ,`pos` = :position
                            ,`thedate` = :thedate
                            ,`details` = :details WHERE `code` = :code";
            $myrec =  $mycon->prepare($sql);
            $myrec->bindValue(":code",$thecode);
            $myrec->bindValue(":title",$title);
            $myrec->bindValue(":position",$position);
            $myrec->bindValue(":thedate",$thedate);
            $myrec->bindValue(":caption",$caption);
            $myrec->bindValue(":details",$details);

            if(!$myrec->execute())
            {
                    showAlert("There was an error saving this content. Please try again.");
                    return;
            }
            else
            {
                    if(strpos(strtoupper($banner['type']),"IMAGE") > -1) 
                    {
                            move_uploaded_file($banner['tmp_name'],"pictures/blogs/$thecode.jpg");
                    }

                    showAlert("Done!!");
                    openPage("admin/blogs_list.php");
            }
    }
    //End admin_blogd_edit()/////////////////


    function admin_articles_add()
    {
        $mycon = databaseConnect();

        $title = $_POST['txttitle'];
        $caption = $_POST['caption'];
        $details = $_POST['details'];
        $thedate = date("Y-m-d H:i:s");
        $banner1 = $_FILES['picture_small'];
        $banner2 = $_FILES['picture_large'];

        //check if a page name was specified
        if(strlen($title) < 1)
        {
            showAlert("Please enter the title of this content before saving.");
            return;

        }

            $sql = "INSERT INTO `articles` SET `title` = :title
                            ,`caption` = :caption
                            ,`thedate` = :thedate
                            ,`details` = :details";
            $myrec =  $mycon->prepare($sql);
            $myrec->bindValue(":title",$title);
            $myrec->bindValue(":caption",$caption);
            $myrec->bindValue(":thedate",$thedate);
            $myrec->bindValue(":details",$details);
            $myrec->execute();

            if($myrec->rowCount() < 1)
            {
                    showAlert("There was an error saving this content. Please try again.");
                    return;
            }
            else
            {
                $thecode = $mycon->lastInsertId();
                if(strpos(strtoupper($banner1['type']),"IMAGE") > -1) 
                {
                    move_uploaded_file($banner1['tmp_name'],"pictures/articles/{$thecode}_1.jpg");
                }
                if(strpos(strtoupper($banner2['type']),"IMAGE") > -1) 
                {
                    move_uploaded_file($banner2['tmp_name'],"pictures/articles/{$thecode}_2.jpg");
                }

                showAlert("Done!!");
                openPage("admin/articles_list.php");
            }
    }


    function admin_articles_edit()
    {
            $mycon = databaseConnect();

        $thecode = $_POST['code'];
        $title = $_POST['txttitle'];
        $caption = $_POST['caption'];
        $details = $_POST['details'];
        $banner1 = $_FILES['picture_small'];
        $banner2 = $_FILES['picture_large'];

        //check if a page name was specified
        if(strlen($title) < 1)
        {
            showAlert("Please enter the title of this content before saving.");
            return;

        }

            $sql = "UPDATE `articles` SET `title` = :title
                            ,`caption` = :caption
                            ,`details` = :details WHERE `code` = :code";
            $myrec =  $mycon->prepare($sql);
            $myrec->bindValue(":code",$thecode);
            $myrec->bindValue(":title",$title);
            $myrec->bindValue(":caption",$caption);
            $myrec->bindValue(":details",$details);

            if(!$myrec->execute())
            {
                    showAlert("There was an error saving this content. Please try again.");
                    return;
            }
            else
            {
                if(strpos(strtoupper($banner1['type']),"IMAGE") > -1) 
                {
                    move_uploaded_file($banner1['tmp_name'],"pictures/articles/{$thecode}_1.jpg");
                    showAlert("saved pictuer1");
                }
                if(strpos(strtoupper($banner2['type']),"IMAGE") > -1) 
                {
                    move_uploaded_file($banner2['tmp_name'],"pictures/articles/{$thecode}_2.jpg");
                    showAlert("saved pictuer2");
                }

                showAlert("Done!!");
                openPage("admin/articles_list.php");
            }
    }


    function admin_store_add()
    {
        $mycon = databaseConnect();

        $name = $_POST['name'];
        $price = $_POST['price'];
        $caption = $_POST['caption'];
        $details = $_POST['details'];
        $thedate = date("Y-m-d H:i:s");
        $banner1 = $_FILES['picture_small'];
        $banner2 = $_FILES['picture_large'];

        //check if a page name was specified
        if(strlen($name) < 1)
        {
            showAlert("Please enter the name of this item before saving.");
            return;

        }
        if(strlen($price) < 1)
        {
            showAlert("Please enter the price of this item before saving.");
            return;

        }

            $sql = "INSERT INTO `store` SET `name` = :name
                            ,`price` = :price
                            ,`caption` = :caption
                            ,`thedate` = :thedate
                            ,`details` = :details";
            $myrec =  $mycon->prepare($sql);
            $myrec->bindValue(":name",$name);
            $myrec->bindValue(":price",$price);
            $myrec->bindValue(":caption",$caption);
            $myrec->bindValue(":thedate",$thedate);
            $myrec->bindValue(":details",$details);
            $myrec->execute();

            if($myrec->rowCount() < 1)
            {
                    showAlert("There was an error saving this product. Please try again.");
                    return;
            }
            else
            {
                $thecode = $mycon->lastInsertId();
                if(strpos(strtoupper($banner1['type']),"IMAGE") > -1) 
                {
                    move_uploaded_file($banner1['tmp_name'],"pictures/store/{$thecode}_1.jpg");
                }
                if(strpos(strtoupper($banner2['type']),"IMAGE") > -1) 
                {
                    move_uploaded_file($banner2['tmp_name'],"pictures/store/{$thecode}_2.jpg");
                }

                showAlert("Done!!");
                openPage("admin/store_list.php");
            }
    }

    function admin_store_edit()
    {
            $mycon = databaseConnect();

        $thecode = $_POST['code'];
        $name = $_POST['name'];
        $price = $_POST['price'];
        $caption = $_POST['caption'];
        $details = $_POST['details'];
        $banner1 = $_FILES['picture_small'];
        $banner2 = $_FILES['picture_large'];

        //check if a page name was specified
        if(strlen($name) < 1)
        {
            showAlert("Please enter the name of this item before saving.");
            return;

        }
        if(strlen($price) < 1)
        {
            showAlert("Please enter the price of this item before saving.");
            return;

        }

            $sql = "UPDATE `store` SET `name` = :name
                            ,`caption` = :caption
                            ,`price` = :price
                            ,`details` = :details WHERE `code` = :code";
            $myrec =  $mycon->prepare($sql);
            $myrec->bindValue(":code",$thecode);
            $myrec->bindValue(":name",$name);
            $myrec->bindValue(":price",$price);
            $myrec->bindValue(":caption",$caption);
            $myrec->bindValue(":details",$details);

            if(!$myrec->execute())
            {
                    showAlert("There was an error saving this product. Please try again.");
                    return;
            }
            else
            {
                if(strpos(strtoupper($banner1['type']),"IMAGE") > -1) 
                {
                    move_uploaded_file($banner1['tmp_name'],"pictures/store/{$thecode}_1.jpg");
                }
                if(strpos(strtoupper($banner2['type']),"IMAGE") > -1) 
                {
                    move_uploaded_file($banner2['tmp_name'],"pictures/store/{$thecode}_2.jpg");
                }

                showAlert("Done!!");
                openPage("admin/store_list.php");
            }
    }

    //Process new branches
    function admin_branches_add()
    {
        $mycon = databaseConnect();

        $country = $_POST['country'];
        $city = $_POST['city'];
        $name = $_POST['name'];
        $address = $_POST['address'];
        $phone = $_POST['phone'];
        $email = $_POST['email'];
        $services = $_POST['services'];
        $banner = $_FILES['picture'];

        //check if a page name was specified
        if(strlen($name) < 1)
        {
            showAlert("Please enter the name of this branch before saving.");
            return;

        }

            $sql = "INSERT INTO `branches` SET `country` = :country
                            ,`city` = :city
                            ,`name` = :name
                            ,`address` = :address
                            ,`phone` = :phone
                            ,`email` = :email
                            ,`services` = :services";
            $myrec =  $mycon->prepare($sql);
            $myrec->bindValue(":country",$country);
            $myrec->bindValue(":city",$city);
            $myrec->bindValue(":name",$name);
            $myrec->bindValue(":address",$address);
            $myrec->bindValue(":phone",$phone);
            $myrec->bindValue(":email",$email);
            $myrec->bindValue(":services",$services);
            $myrec->execute();

            if($myrec->rowCount() < 1)
            {
                    showAlert("There was an error saving this branch. Please try again.");
                    return;
            }
            else
            {
                    $thecode = $mycon->lastInsertId();
                    if(strpos(strtoupper($banner['type']),"IMAGE") > -1) 
                    {
                            move_uploaded_file($banner['tmp_name'],"pictures/branches/$thecode.jpg");
                    }

                    showAlert("Done!!");
                    openPage("admin/branches_list.php");
            }
    }

    //Process edit branches
    function admin_branches_edit()
    {
        $mycon = databaseConnect();

        $code = $_POST['code'];
        $name = $_POST['name'];
        $address = $_POST['address'];
        $phone = $_POST['phone'];
        $email = $_POST['email'];
        $services = $_POST['services'];
        $banner = $_FILES['picture'];

        //check if a page name was specified
        if(strlen($name) < 1)
        {
            showAlert("Please enter the name of this branch before saving.");
            return;

        }

            $sql = "UPDATE `branches` SET `name` = :name
                            ,`address` = :address
                            ,`phone` = :phone
                            ,`email` = :email
                            ,`services` = :services WHERE `code` = :code";
            $myrec =  $mycon->prepare($sql);
            $myrec->bindValue(":code",$code);
            $myrec->bindValue(":name",$name);
            $myrec->bindValue(":address",$address);
            $myrec->bindValue(":phone",$phone);
            $myrec->bindValue(":email",$email);
            $myrec->bindValue(":services",$services);

            if(!$myrec->execute())
            {
                    showAlert("There was an error saving this branch. Please try again.");
                    return;
            }
            else
            {
                    if(strpos(strtoupper($banner['type']),"IMAGE") > -1) 
                    {
                            move_uploaded_file($banner['tmp_name'],"pictures/branches/$code.jpg");
                    }

                    showAlert("Done!!");
                    openPage("admin/branches_list.php");
            }
    }



    //Process update pages
    function admin_pages_edit()
    {
            $mycon = databaseConnect();

            $thecode = $_POST['code'];
            $title = $_POST['txttitle'];
            $caption = $_POST['caption'];
            $details = $_POST['details'];
        $banner = $_FILES['picture'];

        //check if a page name was specified
        if(strlen($title) < 1)
        {
            showAlert("Please enter the title of this content before saving.");
            return;

        }

            $sql = "UPDATE `pages` SET `title` = :title
                            ,`caption` = :caption
                            ,`details` = :details WHERE `code` = :code";
            $myrec =  $mycon->prepare($sql);
            $myrec->bindValue(":code",$thecode);
            $myrec->bindValue(":title",$title);
            $myrec->bindValue(":caption",$caption);
            $myrec->bindValue(":details",$details);

            if(!$myrec->execute())
            {
                    showAlert("There was an error saving this content. Please try again.");
                    return;
            }
            else
            {
                    if(strpos(strtoupper($banner['type']),"IMAGE") > -1) 
                    {
                            move_uploaded_file($banner['tmp_name'],"pictures/pages/$thecode.jpg");
                    }

                    showAlert("Done!!");
                    openPage("admin/pages_edit.php?code=$thecode");
            }
    }
    //End admin_pages_edit()/////////////////


    //Process new staff
    function admin_staff_add()
    {
            $mycon = databaseConnect();

            $name = $_POST['txtname'];
            $caption = $_POST['caption'];
            $details = $_POST['details'];
        $banner = $_FILES['picture'];

        //check if a page name was specified
        if(strlen($name) < 1)
        {
            showAlert("Please enter the name of this staff before saving.");
            return;

        }

            $sql = "INSERT INTO `staff` SET `name` = :name
                            ,`caption` = :caption
                            ,`details` = :details";
            $myrec =  $mycon->prepare($sql);
            $myrec->bindValue(":name",$name);
            $myrec->bindValue(":caption",$caption);
            $myrec->bindValue(":details",$details);
            $myrec->execute();

            if($myrec->rowCount() < 1)
            {
                    showAlert("There was an error saving this staff. Please try again.");
                    return;
            }
            else
            {
                    $thecode = $mycon->lastInsertId();
                    if(strpos(strtoupper($banner['type']),"IMAGE") > -1) 
                    {
                            move_uploaded_file($banner['tmp_name'],"pictures/staff/$thecode.jpg");
                    }

                    showAlert("Done!!");
                    openPage("admin/staff_list.php");
            }
    }
    //End admin_staff_add()/////////////////



    //Process edit staff
    function admin_staff_edit()
    {
            $mycon = databaseConnect();

            $thecode = $_POST['code'];
            $name = $_POST['txtname'];
            $caption = $_POST['caption'];
            $details = $_POST['details'];
            $position = $_POST['position'];
        $banner = $_FILES['picture'];

        //check if a page name was specified
        if(strlen($name) < 1)
        {
            showAlert("Please enter the name of this staff before saving.");
            return;

        }

            $sql = "UPDATE `staff` SET `name` = :name
                            ,`caption` = :caption
                            ,`pos` = :position
                            ,`details` = :details WHERE `code` = :code";
            $myrec =  $mycon->prepare($sql);
            $myrec->bindValue(":code",$thecode);
            $myrec->bindValue(":name",$name);
            $myrec->bindValue(":caption",$caption);
            $myrec->bindValue(":details",$details);
            $myrec->bindValue(":position",$position);

            if(!$myrec->execute())
            {
                    showAlert("There was an error saving this staff. Please try again.");
                    return;
            }
            else
            {
                    if(strpos(strtoupper($banner['type']),"IMAGE") > -1) 
                    {
                            move_uploaded_file($banner['tmp_name'],"pictures/staff/$thecode.jpg");
                    }

                    showAlert("Done!!");
                    openPage("admin/staff_list.php");
            }
    }
    //End admin_staff_edit()/////////////////


    //Process edit home page
    function admin_homepage_edit()
    {
            $mycon = databaseConnect();

            $about_title = $_POST['about_title'];
            $about_content = $_POST['about_content'];
            $bottomtext = $_POST['bottomtext'];
        $picture_about = $_FILES['picture_about'];
        $picture_event = $_FILES['picture_events'];
            $tv_title = $_POST['tv_title'];
            $tv_caption = $_POST['tv_caption'];
            $tv_video = $_POST['tv_video'];
            $tv_text = $_POST['tv_text'];


            $sql = "UPDATE `homepage` SET `about_title` = :about_title
                            ,`about_content` = :about_content
                            ,`bottomtext` = :bottomtext
                            ,`tv_title` = :tv_title
                            ,`tv_caption` = :tv_caption
                            ,`tv_video` = :tv_video
                            ,`tv_text` = :tv_text";
            $myrec =  $mycon->prepare($sql);
            $myrec->bindValue(":about_title",$about_title);
            $myrec->bindValue(":about_content",$about_content);
            $myrec->bindValue(":bottomtext",$bottomtext);
            $myrec->bindValue(":tv_title",$tv_title);
            $myrec->bindValue(":tv_video",$tv_video);
            $myrec->bindValue(":tv_caption",$tv_caption);
            $myrec->bindValue(":tv_text",$tv_text);

            if(!$myrec->execute())
            {
                    showAlert("There was an error updating content. Please try again.");
                    return;
            }
            else
            {
                    if(strpos(strtoupper($picture_about['type']),"IMAGE") > -1) 
                    {
                            move_uploaded_file($picture_about['tmp_name'],"pictures/home_picture_about.jpg");
                    }
                    if(strpos(strtoupper($picture_event['type']),"IMAGE") > -1) 
                    {
                            move_uploaded_file($picture_event['tmp_name'],"pictures/home_picture_event.jpg");
                    }

                    showAlert("Done!!");
                    openPage("admin/homepage.php");
            }
    }
    //End admin_homepage_edit()/////////////////


        function admin_settings_update()
        {

            //$topbanner = $_FILES['topbanner'];
            //$pastorphoto = $_FILES['pastorphoto'];


            if(!isset($_POST['settings']) || !is_array($_POST['settings']))
            {
                    showAlert("Settings could not be parsed. Please refresh this page and try again.");
                    return;
            }

            $mycon = databaseConnect();

            //loop through and add or edit
            foreach($_POST['settings'] as $key => $value)
            {
                $sql = "REPLACE INTO `settings` SET `item_key` = :key, `item_value` = :value";

                $myrec =  $mycon->prepare($sql);
                $myrec->bindValue(":key",$key);
                $myrec->bindValue(":value",$value);
                $myrec->execute();
            }

            //loop through and save images
            foreach($_FILES['pictures']['name'] as $key => $value)
            {
                $picture = $_FILES["pictures"];
                if(strpos(strtoupper($picture['type'][$key]),"IMAGE") > -1) 
                {
                        move_uploaded_file($picture['tmp_name'][$key],"pictures/$key.jpg");
                }
            }

            /*
            //save images
            if(strpos(strtoupper($topbanner['type']),"IMAGE") > -1) 
            {
                    move_uploaded_file($topbanner['tmp_name'],"pictures/home_picture_topbanner.jpg");
            }
            if(strpos(strtoupper($pastorphoto['type']),"IMAGE") > -1) 
            {
                    move_uploaded_file($pastorphoto['tmp_name'],"pictures/home_picture_pastorphoto.jpg");
            }
            */
            showAlert("The settings has been updated!");
            //openPage("admin/settings.php");
        }


    //Process uedit testimonies
    function admin_testimonies_edit()
    {
            $mycon = databaseConnect();

            $thecode = $_POST['code'];
            $fullname = $_POST['fullname'];
            $address = $_POST['address'];
            $testimony = $_POST['testimony'];
            $status = $_POST['status'];
            $category = $_POST['category'];
        $picture = $_FILES['picture'];

        //check if a page name was specified
        if(strlen($fullname) < 1)
        {
            showAlert("Please enter the fullname of this sender before saving.");
            return;

        }

            $sql = "UPDATE `testimonies` SET `fullname` = :fullname
                            ,`address` = :address
                            ,`testimony` = :testimony
                            ,`status` = :status
                            ,`category` = :category WHERE `code` = :code";
            $myrec =  $mycon->prepare($sql);
            $myrec->bindValue(":fullname",$fullname);
            $myrec->bindValue(":address",$address);
            $myrec->bindValue(":testimony",$testimony);
            $myrec->bindValue(":status",$status);
            $myrec->bindValue(":category",$category);
            $myrec->bindValue(":code",$thecode);

            if(!$myrec->execute())
            {
                    showAlert("There was an error saving this testimony. Please try again.");
                    return;
            }
            else
            {
                    if(strpos(strtoupper($picture['type']),"IMAGE") > -1) 
                    {
                            move_uploaded_file($picture['tmp_name'],"pictures/testimonies/$thecode.jpg");
                    }

                    showAlert("Done!!");
                    openPage("admin/testimonies_list.php");
            }
    }
    //End admin_testimonies_edit()/////////////////

    //Process new prayer request
    function prayerrequests_add()
    {
            $mycon = databaseConnect();

        $firstname = $_POST['firstname'];
        $surname = $_POST['surname'];
        $phone = $_POST['phone'];
        $email = $_POST['email'];
        $message = $_POST['message'];
        $thedate = date("Y-m-d H:i:s");

        //check if a page name was specified
            $msg = "";
            if(strlen($firstname) < 1) $msg .= "**Enter your first name.";
            if(strlen($surname) < 1) $msg .= "**Enter your surname.";
            if(strlen($phone) < 1) $msg .= "**Enter your phone number.";
            if(strlen($email) < 1) $msg .= "**Enter your email address.";
            if(strlen($message) < 1) $msg .= "**Enter your prayer request.";

            if($msg != "")
            {
                    showAlert("Please correct the following before saving:**$msg");
                    return;
            }

            $captcha = $_POST['captcha'];
            //check if the captcha is correct
            if(isset($_SESSION["captcha"]) && $_SESSION["captcha"] != $captcha)
            {
                    showAlert("Please enter the exact code displayed.");
                    return;
            }

            $sql = "INSERT INTO `prayerrequests` SET `lastname` = :lastname
                            ,`firstname` = :firstname
                            ,`phone` = :phone
                            ,`email` = :email
                            ,`request` = :message
                            ,`thedate` = :thedate";
            $myrec =  $mycon->prepare($sql);
            $myrec->bindValue(":lastname",$surname);
            $myrec->bindValue(":firstname",$firstname);
            $myrec->bindValue(":phone",$phone);
            $myrec->bindValue(":email",$email);
            $myrec->bindValue(":message",$message);
            $myrec->bindValue(":thedate",$thedate);
            $myrec->execute();

            if($myrec->rowCount() < 1)
            {
                    showAlert("There was an error sending your request. Please try again.");
                    return;
            }
            else
            {

                    showAlert("Your prayer request was send successfuly!!");
                    openPage("testimonies.php");
            }
    }

    //Process new testimony
    function testimonies_add()
    {
            $mycon = databaseConnect();

            $fullname = $_POST['fullname'];
            $address = $_POST['address'];
            $testimony = $_POST['testimony'];
            $picture = $_FILES['picture'];
        $thedate = date("Y-m-d");

            $captcha = $_POST['captcha'];

            //check if the captcha is correct
            if(isset($_SESSION["captcha"]) && $_SESSION["captcha"] != $captcha)
            {
                    showAlert("Please enter the exact code displayed.");
                    return;
            }

            //check if the fields where filled
            $msg = "";
            if(strlen($fullname) < 1) $msg .= "**Enter your fullname.";
            if(strlen($address) < 1) $msg .= "**Enter your address.";
            if(strlen($testimony) < 10) $msg .= "**Enter your testimony.";

            if($msg != "")
            {
                    showAlert("Please correct the following before sending:**$msg");
                    return;
            }

            if(strpos(strtoupper($picture['type']),"IMAGE") > -1) 
            {
            }
            else
            {
                    showAlert("You MUST select your photograph to post testimony.");
                    return;
            }


            $sql = "INSERT INTO `testimonies` SET `fullname` = :fullname
                            ,`address` = :address
                            ,`testimony` = :testimony
                            ,`thedate` = :thedate";
            $myrec =  $mycon->prepare($sql);
            $myrec->bindValue(":fullname",$fullname);
            $myrec->bindValue(":address",$address);
            $myrec->bindValue(":testimony",$testimony);
            $myrec->bindValue(":thedate",$thedate);
            $myrec->execute();

            if($myrec->rowCount() < 1)
            {
                    showAlert("There was an error sending this request. Please try again.");
                    return;
            }
            else
            {
                    $thecode = $mycon->lastInsertId();
                    if(strpos(strtoupper($picture['type']),"IMAGE") > -1) 
                    {
                            move_uploaded_file($picture['tmp_name'],"pictures/testimonies/$thecode.jpg");
                    }

                    showAlert("Your testimony has been sent!!");
                    openPage("index.php");
            }
    }
    //End testimonies_add()/////////////////


    //Process new donation
    function donations_add()
    {
        $mycon = databaseConnect();

        $donation = $_POST['donation'];
        $currency = $_POST['currency'];
        $amount = str_replace(",","",$_POST['amount']);
        $title = $_POST['title'];
        $surname = $_POST['surname'];
        $othernames = $_POST['othernames'];
        $email = $_POST['email'];
        $phone = $_POST['phone'];
        $comment = $_POST['comment'];
        $thedate = date("Y-m-d H:i:s");

        //check if a event name was specified
        if(strlen($surname) < 1)
        {
            showAlert("Please enter the name of this event before saving.");
            return;
        }

        $sql = "INSERT INTO `donations` SET `donation` = :donation
                        ,`currency` = :currency
                        ,`amount` = :amount
                        ,`title` = :title
                        ,`surname` = :surname
                        ,`othernames` = :othernames
                        ,`phone` = :phone
                        ,`email` = :email
                        ,`comment` = :comment
                        ,`thedate` = :thedate";
        $myrec =  $mycon->prepare($sql);
        $myrec->bindValue(":donation",$donation);
        $myrec->bindValue(":currency",$currency);
        $myrec->bindValue(":amount",$amount);
        $myrec->bindValue(":title",$title);
        $myrec->bindValue(":surname",$surname);
        $myrec->bindValue(":othernames",$othernames);
        $myrec->bindValue(":phone",$phone);
        $myrec->bindValue(":email",$email);
        $myrec->bindValue(":comment",$comment);
        $myrec->bindValue(":thedate",$thedate);
        $myrec->execute();

        if($myrec->rowCount() < 1)
        {
                showAlert("There was an error initiating your donation. Please try again.");
                return;
        }
        else
        {
                $donation_id = $mycon->lastInsertId();

                openPage("donations-pay.php?code=$donation_id");
        }
    }
    //End admin_events_add()/////////////////


    //Process new feedback
    function feedbacks_add()
    {
            $mycon = databaseConnect();

            $fullname = $_POST['fullname'];
            $address = $_POST['address'];
            $testimony = $_POST['testimony'];
            $picture = $_FILES['picture'];
        $thedate = date("Y-m-d");

            $captcha = $_POST['captcha'];

            //check if the captcha is correct
            if(isset($_SESSION["captcha"]) && $_SESSION["captcha"] != $captcha)
            {
                    showAlert("Please enter the exact code displayed.");
                    return;
            }

            //check if the fields where filled
            $msg = "";
            if(strlen($fullname) < 1) $msg .= "**Enter your fullname.";
            if(strlen($address) < 1) $msg .= "**Enter your address.";
            if(strlen($testimony) < 10) $msg .= "**Enter your testimony.";

            if($msg != "")
            {
                    showAlert("Please correct the following before sending:**$msg");
                    return;
            }

            $sql = "INSERT INTO `testimonies` SET `fullname` = :fullname
                            ,`address` = :address
                            ,`testimony` = :testimony
                            ,`thedate` = :thedate";
            $myrec =  $mycon->prepare($sql);
            $myrec->bindValue(":fullname",$fullname);
            $myrec->bindValue(":address",$address);
            $myrec->bindValue(":testimony",$testimony);
            $myrec->bindValue(":thedate",$thedate);
            $myrec->execute();

            if($myrec->rowCount() < 1)
            {
                    showAlert("There was an error sending this request. Please try again.");
                    return;
            }
            else
            {
                    $thecode = $mycon->lastInsertId();
                    if(strpos(strtoupper($picture['type']),"IMAGE") > -1) 
                    {
                            move_uploaded_file($picture['tmp_name'],"pictures/testimonies/$thecode.jpg");
                    }

                    showAlert("Your testimony has been sent!!");
                    openPage("index.php");
            }
    }
    //End feedbacks_add()/////////////////

    //Process new registration
    function register()
    {
            $mycon = databaseConnect();

            $accounttype = $_POST['accounttype'];
            $fullname = $_POST['fullname'];
            $country = $_POST['country'];
            $address = $_POST['address'];
            $phone = $_POST['phone'];
            $email = $_POST['email'];
            $branch = $_POST['branch'];
            $pastor = "";
            if(isset($_POST['pastor'])) $pastor = $_POST['pastor'];
            $leader = "";
            if(isset($_POST['leader'])) $leader = $_POST['leader'];
            $leaderphone = $_POST['leaderphone'];
            $username = $_POST['username'];
            $password = $_POST['password'];
            $password2 = $_POST['password2'];
            $picture = $_FILES['picture'];
        $thedate = date("Y-m-d H:i:s");

            $captcha = $_POST['captcha'];

            //check if the captcha is correct
            if(isset($_SESSION["captcha"]) && $_SESSION["captcha"] != $captcha)
            {
                    showAlert("Please enter the exact code displayed.");
                    return;
            }

            //check if the fields where filled
            $msg = "";
            if(strlen($fullname) < 1) $msg .= "**Enter your fullname.";
            if(strlen($address) < 1) $msg .= "**Enter your address.";
            if(strlen($email) < 1 || count(explode("@",$email)) <> 2) $msg .= "**Enter your valid email address";
            if(strlen($phone) < 1) $msg .= "**Enter your phone number.";
            if(strlen($branch) < 1) $msg .= "**Enter your branch.";
            if(strlen($username) < 1) $msg .= "**Choose a login username.";
            if(strlen($password) < 1) $msg .= "**Choose a login password.";
            if(strlen($pastor) < 1) $msg .= "**Enter your branch pastor's name.";
            if(strlen($leader) < 1) $msg .= "**Enter your House care leader's name.";
            if(strlen($leaderphone) < 1) $msg .= "**Enter your House care leader's phone number.";
            if($msg != "")
            {
                    showAlert("Please correct the following before sending:**$msg");
                    return;
            }

            if($password != $password2)
            {
                    showAlert("Your password confirmation does not match.");
                    return;

            }

            $password = generatePassword($password);

            //check if phone,email or username exists
            $myrec = $mycon->prepare("SELECT * FROM `members` WHERE `email` = :email OR `phone` = :phone OR `username` = :username LIMIT 1");
            $myrec->bindValue(":email",$email);
            $myrec->bindValue(":phone",$phone);
            $myrec->bindValue(":username",$username);
            $myrec->execute();

            if($myrec->rowCount() > 0)
            {
                    $row = $myrec->fetch(PDO::FETCH_ASSOC);
                    if($row['email'] = $email)
                    {
                            showAlert("The provided email address is already in use.");
                            return;
                    }
                    if($row['phone'] = $phone)
                    {
                            showAlert("The provided phone number is already in use.");
                            return;
                    }
                    if($row['username'] = $username)
                    {
                            showAlert("The provided username is already in use.");
                            return;
                    }
            }

            $sql = "INSERT INTO `members` SET `accounttype` = :accounttype
                            ,`fullname` = :fullname
                            ,`country` = :country
                            ,`address` = :address
                            ,`phone` = :phone
                            ,`email` = :email
                            ,`branch` = :branch
                            ,`pastor` = :pastor
                            ,`leader` = :leader
                            ,`leaderphone` = :leaderphone
                            ,`username` = :username
                            ,`password` = :password
                            ,`thedate` = :thedate";
            $myrec =  $mycon->prepare($sql);
            $myrec->bindValue(":accounttype",$accounttype);
            $myrec->bindValue(":fullname",$fullname);
            $myrec->bindValue(":country",$country);
            $myrec->bindValue(":address",$address);
            $myrec->bindValue(":phone",$phone);
            $myrec->bindValue(":email",$email);
            $myrec->bindValue(":branch",$branch);
            $myrec->bindValue(":pastor",$pastor);
            $myrec->bindValue(":leader",$leader);
            $myrec->bindValue(":leaderphone",$leaderphone);
            $myrec->bindValue(":username",$username);
            $myrec->bindValue(":password",$password);
            $myrec->bindValue(":thedate",$thedate);
            $myrec->execute();

            if($myrec->rowCount() < 1)
            {
                    showAlert("There was an error creating your account. Please try again.");
                    return;
            }
            else
            {
                    $thecode = $mycon->lastInsertId();
                    if(strpos(strtoupper($picture['type']),"IMAGE") > -1) 
                    {
                            move_uploaded_file($picture['tmp_name'],"pictures/members/$thecode.jpg");
                    }

                    if(strlen(getCookie("CurrentPageURL")) > 5)
                    {
                            openPage(getCookie("CurrentPageURL"));
                    }
                    else
                    {
                            showAlert("Your account has been created.");
                            openPage("index.php");
                    }
            }
    }
    //End register()/////////////////


    function newsletter_subscribe()
    {
            $mycon = databaseConnect();

            $email = $_POST['email'];

            $msg = "";
            if(count(explode("@",$email)) < 2)
            {
                showAlert("Please enter your valid email address.");
                return;
            }

            $thedate = date("Y-m-d H:i:s");
            $sql = "INSERT IGNORE INTO `mailinglist` SET `email` = :email,`groupname` = 'website',`thedate` = :thedate";
            $myrec = $mycon->prepare($sql);
            $myrec->bindValue(":email",$email);
            $myrec->bindValue(":thedate",$thedate);
            $sql .= "VALUES('$email','website','$thedate')";

            if(!mysql_query($sql,$mycon))
            {
                var_dump($mycon->errorInfo());
                showAlert("There was an error processing your request. Please try again later");
                return;
            }
            else
            {

                showAlert("Thank you! you have been subscribed.");
                //header("Location: actionmanager.php?fake=yes");
            }
    }
}



?>