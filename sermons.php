<?php
require_once("config.php");
require_once("inc_dbfunctions.php");
$mycon = databaseConnect();

$dataRead = New DataRead();

$contentslist = $dataRead->sermons_list($mycon, "", "");

$mp3sermonslist = $dataRead->mp3_sermons_list($mycon, "", "");

?>
<!DOCTYPE html>
<html lang="en">
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="<?php seoPageContent() ?>" />
	<meta name="description" content="<?php seoPageDescriptions() ?>">
	<title><?php pageTitle() ?></title>

	<!-- Bootstrap core CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<!-- Full Calender CSS -->
	<link href="css/fullcalendar.css" rel="stylesheet">
	<!-- Owl Carousel CSS -->
	<link href="css/owl.carousel.css" rel="stylesheet">
	<!-- Pretty Photo CSS -->
	<link href="css/prettyPhoto.css" rel="stylesheet">
	<!-- Bx-Slider StyleSheet CSS -->
	<link href="css/jquery.bxslider.css" rel="stylesheet"> 
	<!-- Font Awesome StyleSheet CSS -->
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="svg/style.css" rel="stylesheet">
	<!-- Widget CSS -->
	<link href="css/widget.css" rel="stylesheet">
	<!-- Typography CSS -->
	<link href="css/typography.css" rel="stylesheet">
	<!-- Shortcodes CSS -->
	<link href="css/shortcodes.css" rel="stylesheet">
	<!-- Custom Main StyleSheet CSS -->
	<link href="style.css" rel="stylesheet">
	<!-- Color CSS -->
	<link href="css/color.css" rel="stylesheet">
	<!-- Responsive CSS -->
	<link href="css/responsive.css" rel="stylesheet">
	<!-- SELECT MENU -->
	<link href="css/selectric.css" rel="stylesheet">
	<!-- SIDE MENU -->
	<link rel="stylesheet" href="css/jquery.sidr.dark.css">

</head>

<body>
	<!--KF KODE WRAPPER WRAP START-->
    <div class="kode_wrapper">
    	<!--HEADER START-->
            <?php require_once("inc_header.php"); ?>
	<!--HEADER END-->

        <!--Banner Wrap Start-->
        <div class="kf_inr_banner">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                    	<!--KF INR BANNER DES Wrap Start-->
                        <div class="kf_inr_ban_des">
                        	<div class="inr_banner_heading">
								<h3>Sermons</h3>
                        	</div>
                           
                            <div class="kf_inr_breadcrumb">
								<ul>
									<li><a href="index.php">Home</a></li>
									<li><a href="#">Sermons</a></li>
								</ul>
							</div>
                        </div>
                        <!--KF INR BANNER DES Wrap End-->
                    </div>
                </div>
            </div>
        </div>

        <!--Banner Wrap End-->

    	<!--Content Wrap Start-->
    	<div class="kf_content_wrap">
    				
    		<!--BLOG 3 PAGE START-->
    		<section>
    			<div class="container">
    				<div class="row">
                                    <div class="col-md-8">
    					<div class="abt_univ_wrap">
                                            <!-- HEADING 1 START-->
						<div class="kf_edu2_heading1">
                                                    <h3>Video Sermons</h3>
								</div>
                                <?php
                                    foreach($contentslist as $row)
                                    {
                                ?>
    					<div class="col-md-4">
    						<!--BLOG 3 WRAP START-->
    						<div class="blog_3_wrap">
    							<!--BLOG 3 SIDE BAR START-->
    							<ul class="blog_3_sidebar">
    								<li>
										<a href="sermons-view.php?code=<?php echo $row['sermon_id'] ?>">
											<?php echo date("d",strtotime($row['thedate'])) ?>
											<span><?php echo date("M",strtotime($row['thedate'])) ?></span>
										</a>
									</li>
    							</ul>
    							<!--BLOG 3 SIDE BAR END-->
    							<!--BLOG 3 DES START-->
								<div class="blog_3_des">
									<figure style="height: 150px">
                                                                            <img src="pictures/sermons/<?php echo $row['sermon_id'] ?>.jpg" alt="" style="height: auto; width: auto; max-width: 100%"/>
										<figcaption><a href="sermons-view.php?code=<?php echo $row['sermon_id'] ?>"><i class="fa fa-search-plus"></i></a></figcaption>
									</figure>
									<ul>
										<li><?php echo formatDate($row['thedate']) ?></li>
									</ul>
									<h5><?php echo $row['headline'] ?></h5>
									<p><?php echo $row['caption'] ?> </p>
									<a class="readmore" href="sermons-view.php?code=<?php echo $row['sermon_id'] ?>">
										read more
										<i class="fa fa-long-arrow-right"></i>
									</a>
								</div>
								<!--BLOG 3 DES END-->
    						</div>
    						<!--BLOG 3 WRAP END-->
    					</div>
                                <?php
                                    }
                                ?>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="abt_univ_wrap">
                                            <!-- HEADING 1 START-->
						<div class="kf_edu2_heading1">
                                                    <h3>MP3 Sermons</h3>
								</div>
                                             <?php
                                               foreach($mp3sermonslist as $row)
                                    {
                                            ?>
                                            <div class="blog_3_wrap">
    							<!--BLOG 3 SIDE BAR START-->
    							<ul class="blog_3_sidebar">
    								<li>
										<a href="sermons-view-mp3.php?code=<?php echo $row['sermon_id'] ?>">
											<?php echo date("d",strtotime($row['thedate'])) ?>
											<span><?php echo date("M",strtotime($row['thedate'])) ?></span>
										</a>
									</li>
    							</ul>
    							<!--BLOG 3 SIDE BAR END-->
    							<!--BLOG 3 DES START-->
								<div class="">
                                                                    <a href="pictures/sermons_mp3/<?php echo $row['sermon_id'] ?>.mp3" target="_blank"><h5><?php echo $row['title'] ?></h5></a>
                                                                    <p>by <?php echo $row['minister'] ?> - <?php echo formatDate($row['thedate']) ?></p>
                                                                    <div style="float: right; width: 50%">
                                                                        <a href="pictures/sermons_mp3/<?php echo $row['sermon_id'] ?>.mp3" target="_blank"><span style="text-decoration: underline; color: #000000">Play </span></a> || 
                                                                        <a href="pictures/sermons_mp3/<?php echo $row['sermon_id'] ?>.mp3" download><span style="text-decoration: underline; color: #000000"> Download </span></a>
                                                                    </div>
                                                                    <audio controls preload="none" style="width: 50%; float: left">
                                                                            <source src="pictures/sermons_mp3/<?php echo $row['sermon_id'] ?>.mp3" type="audio/mp3">
                                                                            Your browser does not support the audio element.
                                                                        </audio> 
									
								</div>
								<!--BLOG 3 DES END-->
    						</div>
                                            <?php
                                    }
                                    ?>
                                        </div>
                                        
                                    </div>
    				</div>
    			</div>
			</section>
			<!--BLOG 3 PAGE END-->
    	</div>
        <!--Content Wrap End-->
        
        <?php require_once("inc_footer.php"); ?>
                
    </div>
    <!--KF KODE WRAPPER WRAP END-->

    

	<!--Bootstrap core JavaScript-->
	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<!--Bx-Slider JavaScript-->
	<script src="js/jquery.bxslider.min.js"></script>
	<!--Owl Carousel JavaScript-->
	<script src="js/owl.carousel.min.js"></script>
	<!--Pretty Photo JavaScript-->
	<script src="js/jquery.prettyPhoto.js"></script>
	<!--Full Calender JavaScript-->
	<script src="js/moment.min.js"></script>
	<script src="js/fullcalendar.min.js"></script>
	<script src="js/jquery.downCount.js"></script>
	<!--Image Filterable JavaScript-->
	<script src="js/jquery-filterable.js"></script>
	<!--Accordian JavaScript-->
	<script src="js/jquery.accordion.js"></script>
	<!--Number Count (Waypoints) JavaScript-->
	<script src="js/waypoints-min.js"></script>
	<!--v ticker-->
	<script src="js/jquery.vticker.min.js"></script>
	<!--select menu-->
	<script src="js/jquery.selectric.min.js"></script>
	<!--Side Menu-->
	<script src="js/jquery.sidr.min.js"></script>
	<!--Custom JavaScript-->
	<script src="js/custom.js"></script>

    
</body>
</html>
