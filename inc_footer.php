<?php
require_once("config.php");
require_once("inc_dbfunctions.php");
$mycon = databaseConnect();

$dataRead = New DataRead();

?>
                <!--EDU2 FOOTER WRAP START-->
		<!--NEWS LETTERS START-->
		<div class="edu2_ft_topbar_wrap">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<div class="edu2_ft_topbar_des">
							<h5>Subscribe to our newsletter</h5>
						</div>
					</div>
					<div class="col-md-6">
						<div class="edu2_ft_topbar_des">
							<form>
								<input type="email" placeholder="Enter Valid Email Address">
								<button><i class="fa fa-paper-plane"></i>Submit</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--NEWS LETTERS END-->
		<!--FOOTER START-->
		<footer>
			<!--EDU2 FOOTER CONTANT WRAP START-->
				<div class="container">
					<div class="row">
						<!--EDU2 FOOTER CONTANT DES START-->
						<div class="col-md-3">
							<div class="widget widget-links">
								<h5>Information</h5>
								<ul>
									<li><a href="index.php">Home</a></li>
                                                                        <li><a href="sermons.php">Sermons</a></li>
                                                                        <li><a href="departments.php">Departments</a></li>
                                                                        <li><a href="contactus.php">Contact</a></li>
                                                                        <li><a href="http://christbaptistchurch.org.ng:2095/" target="_blank">Webmail</a></li>
								</ul>
							</div>
						</div>
						<!--EDU2 FOOTER CONTANT DES END-->

						<!--EDU2 FOOTER CONTANT DES START-->
						<div class="col-md-3">
							<div class="widget wiget-instagram">
								<h5>Photos</h5>
								<ul>
                                                            <?php
                                                                $gallery = $dataRead->gallery_list($mycon," ORDER BY rand() LIMIT 9",Array());
                                                                foreach($gallery as $row) {
                                                            ?>
									<li><a href="gallery.php"><img src="pictures/gallery/<?php echo $row['gallery_id'] ?>.jpg" alt=""/></a></li>
                                                            <?php
                                                                }
                                                            ?>
								</ul>
							</div>
						</div>
						<!--EDU2 FOOTER CONTANT DES END-->
						<!--EDU2 FOOTER CONTANT DES START-->
						<div class="col-md-3">
							<div class="widget widget-links">
								<h5>Service Days</h5>
								<ul>
                                                                    <li class="service_days" id="servicedays1">
Sunday
<br>- 1st Service@7:45am
<br>- Sunday School @9:30am
<br>- 2nd Service @ 10:30amadavat
                                                                    </li>
                                                                    <li class="service_days" id="servicedays2">
1st Sunday Of The Month
<br>- Sunday School @8:45am
<br>- Combined Service @10am

                                                                    </li>
                                                                    <li class="service_days" id="servicedays3">
2nd & 4th Sundays of The Month
<br>- Child Dedication
<br>- House fellowship @6pm

                                                                    </li>
                                                                    <li class="service_days" id="servicedays4">
Last Monday of the Month
<br>- Hours of Mercy (Lunch Break Prayer Fellowship) @1pm
                                                                    </li>
                                                                    <li class="service_days" id="servicedays5">
Tuesdays
<br>- Discipleship Classes @6pm
                                                                    </li>
                                                                    <li class="service_days" id="servicedays6">
Wednesday
<br>- Service of Grace & Truth (Mid-Week Service) @6pm
                                                                    </li>
                                                                    <li class="service_days" id="servicedays7">
Friday
<br>- WMS (Women's Fellowship) @6pm

                                                                    </li>
                                                                    <li class="service_days" id="servicedays8">
Saturday
<br>- Sunday School Preview @8am
<br>- Choir Practice @4pm

                                                                    </li>
								</ul>
							</div>
						</div>
						<!--EDU2 FOOTER CONTANT DES END-->


						<!--EDU2 FOOTER CONTANT DES START-->
						<div class="col-md-3">
							<div class="widget widget-contact">
								<h5>Contact</h5>
								<ul>
									<li>4A Oshunkeye (Redemption) Crescent, Gbagada Industrial Estate, Gbagada, Lagos. </li>
									<li>Phone : <a href="#"> (+234)-1-8962001</a></li>
									<li>Email : <a href="#"> Churchoffice@christbaptistchurch.org.ng</a></li>
								</ul>
							</div>
						</div>
						<!--EDU2 FOOTER CONTANT DES END-->
					</div>
				</div>
		</footer>
		<!--FOOTER END-->
		<!--COPYRIGHTS START-->
		<div class="edu2_copyright_wrap">
			<div class="container">
				<div class="row">

					<div class="col-md-3">
					</div>
					<div class="col-md-6">
						<div class="copyright_des">
							<span>&copy; <?php echo date("Y") ?> Christ Baptist Church, Gbagada. All Rights reserved. </span>
						</div>
					</div>
					<div class="col-md-3">
						<ul class="cards_wrap">
                                                    <li><a href="https://www.facebook.com/cbcgbagada" target="_blank"><img src="images/icon_facebook.png" alt=""/></a></li>
                                                    <li><a href="https://twitter.com/cbcgbagada"><img src="images/icon_twitter.png" target="_blank"/></a></li>
                                                    <li><a href="https://www.instagram.com/cbcgbagada/"><img src="images/icon_instagram.png" target="_blank"/></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!--COPYRIGHTS START-->
