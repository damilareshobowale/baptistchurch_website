    	<header id="header_2">
    		<!--kode top bar start-->
    		<div class="top_bar_2">
	    		<div class="container">
	    			<div class="row">
	    				<div class="col-md-5">
	    					<div class="pull-left">
                                                    <em class="contct_2"><i class="fa fa-envelope"></i>Churchoffice@christbaptistchurch.org.ng &nbsp; &nbsp; <i class="fa fa-phone"></i> (+234)-1-8962001 </em>
	    					</div>
	    				</div>
	    				<div class="col-md-7">
	    					<ul class="top_nav">
                                                        <li><a href="events.php">Church programme</a></li>
                                                        <li><a href="departments.php">Departments</a></li>
                                                        <li><a href="gallery.php">Gallery</a></li>
                                                        <li><a href="sermons.php">Sermons</a></li>
                                                        <li><a href="blogs.php">Blog</a></li>
	    					</ul>
	    				</div>
	    			</div>
	    		</div>
	    	</div>
    		<!--kode top bar end-->
        	
	    	<!--kode navigation start-->
    		<div class="kode_navigation">
    			<div id="mobile-header">
                	<a id="responsive-menu-button" href="#sidr-main"><i class="fa fa-bars"></i></a>
                </div>
    			<div class="container">
    				<div class="row">
    					<div class="col-md-4">
    						<div class="logo_wrap">
                                                    <a href="index.php"><img src="images/logo.png" alt=""></a>
    						</div>
    					</div>
    					<div class="col-md-8">
    						<!--kode nav_2 start-->
    						<div class="nav_2" id="navigation">
    							<ul>
                                                            <li><a href="index.php">home</a></li>
                                                            <li><a href="aboutus.php">About Us</a></li>
                                                            <li><a href="events.php">Events</a></li>
                                                            <li><a href="departments.php">Ministries</a></li>
                                                            <li><a href="#">Media</a>
                                                                <ul>
                                                                    <li><a href="gallery.php">Gallery</a></li>
                                                                    <li><a href="sermons.php">Sermons</a></li>
                                                                    <li><a href="blogs.php">Blog</a></li>
                                                                </ul>
                                                            </li>
                                                            <li><a href="contactus.php">Contact us</a></li>
    							</ul>
    						</div>
    						<!--kode nav_2 end-->
    					</div>
    				</div>
    			</div>
    		</div>
    		<!--kode navigation end-->
		</header>
