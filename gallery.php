<?php
require_once("config.php");
require_once("inc_dbfunctions.php");
$mycon = databaseConnect();

$dataRead = New DataRead();

$contentslist = $dataRead->gallery_list($mycon, "", "");
$categories = $dataRead->gallery_categories($mycon);

?>
<!DOCTYPE html>
<html lang="en">
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="<?php seoPageContent() ?>" />
	<meta name="description" content="<?php seoPageDescriptions() ?>">
	<title><?php pageTitle() ?></title>

	<!-- Bootstrap core CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<!-- Full Calender CSS -->
	<link href="css/fullcalendar.css" rel="stylesheet">
	<!-- Owl Carousel CSS -->
	<link href="css/owl.carousel.css" rel="stylesheet">
	<!-- Pretty Photo CSS -->
	<link href="css/prettyPhoto.css" rel="stylesheet">
	<!-- Bx-Slider StyleSheet CSS -->
	<link href="css/jquery.bxslider.css" rel="stylesheet"> 
	<!-- Font Awesome StyleSheet CSS -->
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="svg/style.css" rel="stylesheet">
	<!-- Widget CSS -->
	<link href="css/widget.css" rel="stylesheet">
	<!-- Typography CSS -->
	<link href="css/typography.css" rel="stylesheet">
	<!-- Shortcodes CSS -->
	<link href="css/shortcodes.css" rel="stylesheet">
	<!-- Custom Main StyleSheet CSS -->
	<link href="style.css" rel="stylesheet">
	<!-- Color CSS -->
	<link href="css/color.css" rel="stylesheet">
	<!-- Responsive CSS -->
	<link href="css/responsive.css" rel="stylesheet">
	<!-- SELECT MENU -->
	<link href="css/selectric.css" rel="stylesheet">
	<!-- SIDE MENU -->
	<link rel="stylesheet" href="css/jquery.sidr.dark.css">

</head>

<body>
	<!--KF KODE WRAPPER WRAP START-->
    <div class="kode_wrapper">
    	<!--HEADER START-->
            <?php require_once("inc_header.php"); ?>
	<!--HEADER END-->

        <!--Banner Wrap Start-->
        <div class="kf_inr_banner">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                    	<!--KF INR BANNER DES Wrap Start-->
                        <div class="kf_inr_ban_des">
                        	<div class="inr_banner_heading">
								<h3>Gallery</h3>
                        	</div>
                           
                            <div class="kf_inr_breadcrumb">
								<ul>
									<li><a href="index.php">Home</a></li>
									<li><a href="#">Photo Gallery</a></li>
								</ul>
							</div>
                        </div>
                        <!--KF INR BANNER DES Wrap End-->
                    </div>
                </div>
            </div>
        </div>

        <!--Banner Wrap End-->

    	<!--Content Wrap Start-->
    	<div class="kf_content_wrap">
    		<div class="gallery-masonery_page gallery inner-content-holder">
    			<div class="container">
	    			<div class="row">
	    				<ul id="filterable-item-filter-1">
							<li><a data-value="all">All</a></li>
                                                <?php foreach($categories as $row) { ?>        
							<li><a data-value="<?php echo strtolower(str_replace(" ","",$row['category'])) ?>"><?php echo $row['category'] ?></a></li>
                                                <?php } ?>
						</ul>

	    				<div id="filterable-item-holder-1">
                                            <?php foreach($contentslist as $row) { ?>
							<div class="filterable-item all <?php echo strtolower(str_replace(" ","",$row['category'])) ?> col-md-4 col-sm-6 col-xs-12">
								<div class="edu_masonery_thumb">
									<img src="pictures/gallery/<?php echo $row['gallery_id'] ?>.jpg" alt=""/>
									<div class="caption"><a href=""><?php echo $row['name'] ?></a></div>
									<a href="pictures/gallery/<?php echo $row['gallery_id'] ?>.jpg" rel="prettyPhoto[gallery2]" class="zoom"><i class="fa fa-search"></i></a>
								</div>	
							</div>
                                            <?php } ?>
						</div>
					</div>
    			</div>
    			<div class="row">
    				<div class="loadmore">
    					<a href="#" class="btn-3">load more</a>
    				</div>
    			</div>
    		</div>
    	</div>
        <!--Content Wrap End-->
        
        <?php require_once("inc_footer.php"); ?>
                
    </div>
    <!--KF KODE WRAPPER WRAP END-->

    

	<!--Bootstrap core JavaScript-->
	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<!--Bx-Slider JavaScript-->
	<script src="js/jquery.bxslider.min.js"></script>
	<!--Owl Carousel JavaScript-->
	<script src="js/owl.carousel.min.js"></script>
	<!--Pretty Photo JavaScript-->
	<script src="js/jquery.prettyPhoto.js"></script>
	<!--Full Calender JavaScript-->
	<script src="js/moment.min.js"></script>
	<script src="js/fullcalendar.min.js"></script>
	<script src="js/jquery.downCount.js"></script>
	<!--Image Filterable JavaScript-->
	<script src="js/jquery-filterable.js"></script>
	<!--Accordian JavaScript-->
	<script src="js/jquery.accordion.js"></script>
	<!--Number Count (Waypoints) JavaScript-->
	<script src="js/waypoints-min.js"></script>
	<!--v ticker-->
	<script src="js/jquery.vticker.min.js"></script>
	<!--select menu-->
	<script src="js/jquery.selectric.min.js"></script>
	<!--Side Menu-->
	<script src="js/jquery.sidr.min.js"></script>
	<!--Custom JavaScript-->
	<script src="js/custom.js"></script>

    
</body>
</html>
