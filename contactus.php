<?php
require_once("config.php");
require_once("inc_dbfunctions.php");
$mycon = databaseConnect();

$dataRead = New DataRead();

$contentsdetails = $dataRead->contents_get($mycon);
$pastorslist = $dataRead->pastors_list($mycon, "", "");

?>
<!DOCTYPE html>
<html lang="en">
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="<?php seoPageContent() ?>" />
	<meta name="description" content="<?php seoPageDescriptions() ?>">
	<title><?php pageTitle() ?></title>

	<!-- Bootstrap core CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<!-- Full Calender CSS -->
	<link href="css/fullcalendar.css" rel="stylesheet">
	<!-- Owl Carousel CSS -->
	<link href="css/owl.carousel.css" rel="stylesheet">
	<!-- Pretty Photo CSS -->
	<link href="css/prettyPhoto.css" rel="stylesheet">
	<!-- Bx-Slider StyleSheet CSS -->
	<link href="css/jquery.bxslider.css" rel="stylesheet"> 
	<!-- Font Awesome StyleSheet CSS -->
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="svg/style.css" rel="stylesheet">
	<!-- Widget CSS -->
	<link href="css/widget.css" rel="stylesheet">
	<!-- Typography CSS -->
	<link href="css/typography.css" rel="stylesheet">
	<!-- Shortcodes CSS -->
	<link href="css/shortcodes.css" rel="stylesheet">
	<!-- Custom Main StyleSheet CSS -->
	<link href="style.css" rel="stylesheet">
	<!-- Color CSS -->
	<link href="css/color.css" rel="stylesheet">
	<!-- Responsive CSS -->
	<link href="css/responsive.css" rel="stylesheet">
	<!-- SELECT MENU -->
	<link href="css/selectric.css" rel="stylesheet">
	<!-- SIDE MENU -->
	<link rel="stylesheet" href="css/jquery.sidr.dark.css">

</head>

<body>
	<!--KF KODE WRAPPER WRAP START-->
    <div class="kode_wrapper">
    	<!--HEADER START-->
            <?php require_once("inc_header.php"); ?>
	<!--HEADER END-->

        <!--Banner Wrap Start-->
        <div class="kf_inr_banner">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                    	<!--KF INR BANNER DES Wrap Start-->
                        <div class="kf_inr_ban_des">
                        	<div class="inr_banner_heading">
								<h3>about us</h3>
                        	</div>
                           
                            <div class="kf_inr_breadcrumb">
								<ul>
									<li><a href="index.php">Home</a></li>
									<li><a href="#">about us</a></li>
								</ul>
							</div>
                        </div>
                        <!--KF INR BANNER DES Wrap End-->
                    </div>
                </div>
            </div>
        </div>

        <!--Banner Wrap End-->

    	<!--Content Wrap Start-->
    	<div class="kf_content_wrap">
    		<div class="kf_location_wrap" style="overflow: hidden">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3963.751236094025!2d3.373850214856571!3d6.553058324636855!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x103b8d9c51b92879%3A0x9f95bec344c41119!2sGbagada+Industrial+Estate+Road!5e0!3m2!1sen!2sng!4v1470674360493" width="1200" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
    		</div>
    		<section>
    			<div class="container">
    				<div class="row">
    					<div class="contct_wrap">
    						<form>
		    					<div class="col-md-8">
		    						<div class="contact_heading">
		    							<h4>Send us a Message</h4>
		    						</div>
		    						<div class="row">
		    							<div class="col-md-6">
		    								<div class="contact_des">
		    									<div class="inputs_des">
		    										<span><i class="fa fa-user"></i>Your full Name</span>
		    										<input type="text">
		    									</div>

		    									<div class="inputs_des">
		    										<span><i class="fa fa-envelope-o"></i>E-mail address</span>
		    										<input type="text">
		    									</div>

		    									<div class="inputs_des">
		    										<span><i class="fa fa-file-text-o"></i>Subject</span>
		    										<input type="text">
		    									</div>
		    									<button>Submit</button>
		    								</div>
		    							</div>
		    							<div class="col-md-6">
		    								<div class="inputs_des">
		    									<span><i class="fa fa-comments-o"></i>Message</span>
		    									<textarea></textarea>
		    								</div>
		    							</div>
		    						</div>
		    					</div>

		    					<div class="col-md-4">
		    						<div class="contact_heading">
		    							<h4>Contact info</h4>
		    							<p>4A Oshunkeye (Redemption) Crescent, Gbagada Industrial Estate, Gbagada, Lagos.</p>
		    						</div>
		    						<ul class="contact_meta">
										<li><i class="fa fa-phone"></i><a href="#"> (+234)-1-8962001</a></li>
										<li><i class="fa fa-envelope-o"></i><a href="#"> Churchoffice@christbaptistchurch.org.ng</a></li>
									</ul>
									<div class="contact_heading social">
		    						</div>
		    						<ul class="cont_socil_meta">
										<li><a href="#"><i class="fa fa-youtube"></i></a></li>
										<li><a href="#"><i class="fa fa-skype"></i></a></li>
										<li><a href="#"><i class="fa fa-twitter"></i></a></li>
									</ul>
		    					</div>
		    				</form>
	    				</div>
    				</div>
    			</div>
    		</section>
            
    	</div>
        <!--Content Wrap End-->
        
        <?php require_once("inc_footer.php"); ?>
                
    </div>
    <!--KF KODE WRAPPER WRAP END-->

    

	<!--Bootstrap core JavaScript-->
	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<!--Bx-Slider JavaScript-->
	<script src="js/jquery.bxslider.min.js"></script>
	<!--Owl Carousel JavaScript-->
	<script src="js/owl.carousel.min.js"></script>
	<!--Pretty Photo JavaScript-->
	<script src="js/jquery.prettyPhoto.js"></script>
	<!--Full Calender JavaScript-->
	<script src="js/moment.min.js"></script>
	<script src="js/fullcalendar.min.js"></script>
	<script src="js/jquery.downCount.js"></script>
	<!--Image Filterable JavaScript-->
	<script src="js/jquery-filterable.js"></script>
	<!--Accordian JavaScript-->
	<script src="js/jquery.accordion.js"></script>
	<!--Number Count (Waypoints) JavaScript-->
	<script src="js/waypoints-min.js"></script>
	<!--v ticker-->
	<script src="js/jquery.vticker.min.js"></script>
	<!--select menu-->
	<script src="js/jquery.selectric.min.js"></script>
	<!--Side Menu-->
	<script src="js/jquery.sidr.min.js"></script>
	<!--Custom JavaScript-->
	<script src="js/custom.js"></script>

    
</body>
</html>
