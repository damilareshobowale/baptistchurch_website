<?php
require_once("config.php");
require_once("inc_dbfunctions.php");
$mycon = databaseConnect();


$dataRead = New DataRead();

$birthdaylist = $dataRead->birthdays_list($mycon, "", "");
if(!$birthdaylist)
{
    showAlert("The requested content was not found");
    openPage("index.php");
    exit;
}


$contentslist = $dataRead->blogs_list($mycon, " LIMIT 7", Array());

//get birthday happening for a week
?>
<!DOCTYPE html>
<html lang="en">
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="<?php seoPageContent() ?>" />
	<meta name="description" content="<?php seoPageDescriptions() ?>">
	<title><?php pageTitle() ?></title>

	<!-- Bootstrap core CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<!-- Full Calender CSS -->
	<link href="css/fullcalendar.css" rel="stylesheet">
	<!-- Owl Carousel CSS -->
	<link href="css/owl.carousel.css" rel="stylesheet">
	<!-- Pretty Photo CSS -->
	<link href="css/prettyPhoto.css" rel="stylesheet">
	<!-- Bx-Slider StyleSheet CSS -->
	<link href="css/jquery.bxslider.css" rel="stylesheet"> 
	<!-- Font Awesome StyleSheet CSS -->
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="svg/style.css" rel="stylesheet">
	<!-- Widget CSS -->
	<link href="css/widget.css" rel="stylesheet">
	<!-- Typography CSS -->
	<link href="css/typography.css" rel="stylesheet">
	<!-- Shortcodes CSS -->
	<link href="css/shortcodes.css" rel="stylesheet">
	<!-- Custom Main StyleSheet CSS -->
	<link href="style.css" rel="stylesheet">
	<!-- Color CSS -->
	<link href="css/color.css" rel="stylesheet">
	<!-- Responsive CSS -->
	<link href="css/responsive.css" rel="stylesheet">
	<!-- SELECT MENU -->
	<link href="css/selectric.css" rel="stylesheet">
	<!-- SIDE MENU -->
	<link rel="stylesheet" href="css/jquery.sidr.dark.css">

</head>

<body>
	<!--KF KODE WRAPPER WRAP START-->
    <div class="kode_wrapper">
    	<!--HEADER START-->
            <?php require_once("inc_header.php"); ?>
	<!--HEADER END-->

        <!--Banner Wrap Start-->
        <div class="kf_inr_banner">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                    	<!--KF INR BANNER DES Wrap Start-->
                        <div class="kf_inr_ban_des">
                        	<div class="inr_banner_heading">
								<h3>Members Birthdays</h3>
                        	</div>
                           
                            <div class="kf_inr_breadcrumb">
								<ul>
									<li><a href="index.php">Home</a></li>
									<li><a href="#">birthdays</a></li>
								</ul>
							</div>
                        </div>
                        <!--KF INR BANNER DES Wrap End-->
                    </div>
                </div>
            </div>
        </div>

        <!--Banner Wrap End-->

    	<!--Content Wrap Start-->
    	<div class="kf_content_wrap">
    		<section>
    			<div class="container">
    				<div class="row">
    					<div class="col-md-8">

    						<!-- HEADING 1 START-->
							<div class="kf_edu2_heading1">
								<h3>Birthdays this week</h3>
							</div>
						<!-- HEADING 1 END-->
                                                <div class="kf_edu2_tab_des" style="padding-bottom: 70px">
                                                <?php
                                                    $count = 0;
                                                    $monday = date("d",strtotime('monday this week'));
                                                    $sunday = date("d",strtotime('sunday this week'));
                                                    foreach($birthdaylist as $row)
                                                    {
                                                        if (date("d", strtotime($row["birthdaydate"])) >= $monday && date("d", strtotime($row["birthdaydate"])) <= $sunday)
                                                        {
                                                            
                                                        $count++;
                                                ?>
                                                    
                                                <!-- EDU COURSES WRAP START -->			
                                                    <div class="blog_3_wrap">
    							<!--BLOG 3 SIDE BAR START-->
    							<ul class="blog_3_sidebar">
    								<li>
										<a href="#">
											<?php echo date("d",strtotime($row['birthdaydate'])) ?>
											<span><?php echo date("M",strtotime($row['birthdaydate'])) ?></span>
                                                                                </a>
									</li>
    							</ul>
    							<!--BLOG 3 SIDE BAR END-->
    							<!--BLOG 3 DES START-->
								<div class="">
                                                                    <h5><?php echo $row['lastname']. " ".$row['firstname'] ?></h5>
                                                                    
                                                                    
								</div>
								<!--BLOG 3 DES END-->
    						</div>
                                                <!-- EDU COURSES WRAP END -->
                                                <?php
                                                    }
                                                    }
                                                        
                                                        ?>
						</div>
						<!--EDU2 COURSES TAB WRAP END-->
                                                <!-- HEADING 1 START-->
							<div class="kf_edu2_heading1">
								<h3>Birthdays This Month</h3>
							</div>
						<!-- HEADING 1 END-->
                                                <?php
                                                foreach($birthdaylist as $row)
                                                    {
                                                if (date("M", strtotime($row["birthdaydate"])) == date("M")&& date("d", strtotime($row["birthdaydate"])) != $sunday && date("d", strtotime($row["birthdaydate"])) < $monday)
                                                {
                                                        ?>
						
                                                    
                                                <!-- EDU COURSES WRAP START -->			
                                                    <div class="blog_3_wrap">
    							<!--BLOG 3 SIDE BAR START-->
    							<ul class="blog_3_sidebar">
    								<li>
                                                                    <a href="#">
                                                                    <?php echo date("d",strtotime($row['birthdaydate'])) ?>
                                                                    <span><?php echo date("M",strtotime($row['birthdaydate'])) ?></span>
                                                                    </a>
								</li>
    							</ul>
    							<!--BLOG 3 SIDE BAR END-->
    							<!--BLOG 3 DES START-->
								<div class="">
                                                                    <h5><?php echo $row['lastname']. " ".$row['firstname'] ?></h5>
                                                                    
                                                                    
								</div>
								<!--BLOG 3 DES END-->
    						</div>
                                                <!-- EDU COURSES WRAP END -->
                                                <?php
                                                    }
                                                        }
                                                        ?>
						</div>
						<!--EDU2 COURSES TAB WRAP END-->
    					

    					<!--KF_EDU_SIDEBAR_WRAP START-->
    					<div class="col-md-4">
    						<div class="kf-sidebar">
    							<!--KF SIDEBAR RECENT POST WRAP START-->
    							<div class="widget widget-recent-posts">
    								<h2>Recent Posts</h2>
    								<ul class="sidebar_rpost_des">
                                    	<!--LIST ITEM START-->
                                <?php
                                    foreach($contentslist as $row)
                                    {
                                ?>
                                        
                                   		<li>
                                            <figure style="height: 85px">
                                            	<img src="pictures/blogs/<?php echo $row['blog_id'] ?>.jpg" alt="">
                                                <figcaption><a href="blogs-view.php?code=<?php echo $row['blog_id'] ?>"><i class="fa fa-search-plus"></i></a></figcaption>
                                            </figure>
                                            <div class="kode-text">
                                                <h6><a href="blogs-view.php?code=<?php echo $row['blog_id'] ?>"><?php echo $row['headline'] ?></a></h6>
                                                <span><i class="fa fa-clock-o"></i><?php echo formatDate($row['thedate'],"no") ?></span>
                                            </div>
    									</li>
                                <?php
                                    }
                                ?>
                                        <!--LIST ITEM START-->
										<!--LIST ITEM START-->
    								</ul>
    							</div>
    							<!--KF SIDEBAR RECENT POST WRAP END-->

    						</div>
    					</div>
						<!--KF EDU SIDEBAR WRAP END-->

    				</div>
    			</div>
    		</section>
    				
    		<!--ABOUT UNIVERSITY START-->
    		<section>
    			<div class="container">
    				<div class="row">

                                </div>
    			</div>
    		</section>
    		<!--ABOUT UNIVERSITY END-->
    	</div>
        <!--Content Wrap End-->
        
        <?php require_once("inc_footer.php"); ?>
                
    </div>
    <!--KF KODE WRAPPER WRAP END-->

    

	<!--Bootstrap core JavaScript-->
	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<!--Bx-Slider JavaScript-->
	<script src="js/jquery.bxslider.min.js"></script>
	<!--Owl Carousel JavaScript-->
	<script src="js/owl.carousel.min.js"></script>
	<!--Pretty Photo JavaScript-->
	<script src="js/jquery.prettyPhoto.js"></script>
	<!--Full Calender JavaScript-->
	<script src="js/moment.min.js"></script>
	<script src="js/fullcalendar.min.js"></script>
	<script src="js/jquery.downCount.js"></script>
	<!--Image Filterable JavaScript-->
	<script src="js/jquery-filterable.js"></script>
	<!--Accordian JavaScript-->
	<script src="js/jquery.accordion.js"></script>
	<!--Number Count (Waypoints) JavaScript-->
	<script src="js/waypoints-min.js"></script>
	<!--v ticker-->
	<script src="js/jquery.vticker.min.js"></script>
	<!--select menu-->
	<script src="js/jquery.selectric.min.js"></script>
	<!--Side Menu-->
	<script src="js/jquery.sidr.min.js"></script>
	<!--Custom JavaScript-->
	<script src="js/custom.js"></script>

    
</body>
</html>
