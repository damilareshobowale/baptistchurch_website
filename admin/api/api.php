<?php
ob_start();

require_once("../config.php");

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//trap the calling url
$getvalues = "";
$thedate = date("Y-m-d H:i:s");
$getvalues .= "<br>=========== START NEW REQUEST ============ $thedate ================";
$getvalues .= "<br> ------------- URL PARA  ------------------";

foreach ( $_GET as $key => $value ) 
{
        //other code go here
    $getvalues .= "<br>Index : " . $key . " & Value : " . $value;
    $getvalues .= "<br/>";
}

file_put_contents("shortcode_api_parameter_test.htm",$getvalues,FILE_APPEND);

$getvalues = "<br>=========== FORM PARA ============ $thedate ================";

foreach ( $_POST as $key => $value ) 
{
        //other code go here
    $getvalues .= "<br>Index : " . $key . " & Value : " . $value;
    $getvalues .= "<br/>";
}

file_put_contents("shortcode_api_parameter_test.htm",$getvalues,FILE_APPEND);

$thedate = date("Y-m-d H:i:s");
$getvalues = "<br>=========== PAGE WAS CALLED ============ $thedate ================";

file_put_contents("shortcode_api_parameter_test.htm",$getvalues,FILE_APPEND);



/**
*		1.0.0 - Rezza - Original
*/

//Check that the transaction orginated from the txtNation server	
//if (isset($_GET['s_session']) && $_GET['s_session'] != "")
if (isset($_GET['s_session']))
{
	
	//Insert your DB save details here and generate return post string
	saveMessage();

}

//SAVES THE MESSAGE SENT BY THE USER
function saveMessage()
{
	$mycon = databaseConnect();
	$action = "incoming";
	$messageid = time().trim($_GET["s_session"]);
	$phone = str_replace("+","",trim($_GET["s_msisdn"]));
	$network = trim($_GET["p_network"]);
	$shortcode = str_replace("+","",trim($_GET["p_sn"]));
	//$shortcode = "32810";//$_POST["shortcode"];
	$country = "NG";
	$billing = "0.0";
	$message = trim($_GET["p_msg"]);
	$thedate = date("Y-m-d H:i:s");
	
	$keyword = explode(" ",$message,3);
	$keyword = trim($keyword[1]);

	$sql = "INSERT INTO `messages` SET `messageid` = :messageid"
                . ",`phone` = :phone"
                . ",`network` = :network"
                . ",`shortcode` = :shortcode"
                . ",`keyword` = :keyword"
                . ",`message` = :message"
                . ",`thedate` = :thedate";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":messageid",$messageid);
        $myrec->bindValue(":phone",$phone);
        $myrec->bindValue(":network",$network);
        $myrec->bindValue(":shortcode",$shortcode);
        $myrec->bindValue(":keyword",$keyword);
        $myrec->bindValue(":message",$message);
        $myrec->bindValue(":thedate",$thedate);
        
        $myrec->execute();
        
        //check what command was specified
        $messageparts = explode(" ",$message);
        if(count($messageparts) < 4) //no command was specified
        {
            sendSMS($phone, "Invalid SMS command");
            return;
        }
        
	$command = strtolower(trim($messageparts[2]));
        echo $command;
        
        if($command == "register")
        {
            if(!isset($messageparts[3]))
            {
                sendSMS($phone, "No service no. specified. Please send (Find ISS register SERVICE_NO)");
                return;
            }
            
            $service_no = $messageparts[3];
            $dataRead = New DataRead();
            $officerdetails = $dataRead->officers_getbyserviceno($mycon, $service_no);
            if(!$officerdetails)
            {
                sendSMS($phone, "The specified Service No. is invalid. Please send (Find ISS register SERVICE_NO)");
                return;
            }
            
            //activate the officer
            $sql = "UPDATE `officers` SET `status` = 5 WHERE `officer_id` = :officer_id";
            $myrec = $mycon->prepare($sql);
            $myrec->bindValue(":officer_id",$officerdetails['officer_id']);
            if(!$myrec->execute())
            {
                sendSMS($phone, "There was an error activating your profile. Please send (Find ISS register SERVICE_NO)");
                return;
            }
            else
            {
                sendSMS($phone, "Welcome to Imo State Security Service. Your profile has been fully activated. Please send (Find ISS news SERVICE_NO) to subscribe for security information.");
                return;
            }
        }
        
        if($command == "news")
        {
            if(!isset($messageparts[3]))
            {
                sendSMS($phone, "No service no. specified. Please send (Find ISS news SERVICE_NO)'");
                return;
            }
            
            $service_no = $messageparts[3];
            $dataRead = New DataRead();
            $officerdetails = $dataRead->officers_getbyserviceno($mycon, $service_no);
            if(!$officerdetails)
            {
                sendSMS($phone, "The specified Service No. is invalid. Please send (Find ISS news SERVICE_NO)'");
                return;
            }
            
            //subscribe the officer
            $enddate = date("Y-m-d",time() + 604800);
            $sql = "INSERT INTO `officers_subscriptions` SET `officer_id` = :officer_id, `startdate` = :startdate, `enddate` = :enddate";
            $myrec = $mycon->prepare($sql);
            $myrec->bindValue(":startdate",date("Y-m-d"));
            $myrec->bindValue(":enddate",$enddate);
            $myrec->bindValue(":officer_id",$officerdetails['officer_id']);
            if(!$myrec->execute())
            {
                sendSMS($phone, "There was an error activating your subscription. Please send (Find ISS news SERVICE_NO)");
                return;
            }
            else
            {
                sendSMS($phone, "Thank you. Your subscription will end on $enddate. Please send (Find ISS check SERVICE_NO) to view your information.");
                return;
            }
        }
        
        if($command == "check")
        {
            if(!isset($messageparts[3]))
            {
                sendSMS($phone, "No service no. specified. Please send (Find ISS check SERVICE_NO)'");
                return;
            }
            
            $service_no = $messageparts[3];
            $dataRead = New DataRead();
            $officerdetails = $dataRead->officers_getbyserviceno($mycon, $service_no);
            if(!$officerdetails)
            {
                sendSMS($phone, "The specified Service No. is invalid. Please send (Find ISS check SERVICE_NO)");
                return;
            }
            
            $reply = "Officer details: ".$officerdetails['firstname']." ".$officerdetails['surname'].", ".$officerdetails['gender'].", Rank: ".$officerdetails['service_rank'].". Attached to ".$officerdetails['service_location'];
            sendSMS($phone, $reply);
            return;           
        }
        
        if($command == "report")
        {
            if(!isset($messageparts[3]))
            {
                sendSMS($phone, "No service no. specified. Please send (Find ISS report SERVICE_NO)");
                return;
            }
            
            $service_no = $messageparts[3];
            $dataRead = New DataRead();
            $officerdetails = $dataRead->officers_getbyserviceno($mycon, $service_no);
            if(!$officerdetails)
            {
                sendSMS($phone, "The specified Service No. is invalid. Please send (Find ISS report SERVICE_NO)");
                return;
            }
            
            //subscribe the officer
            $enddate = date("Y-m-d",time() + 604800);
            $sql = "INSERT INTO `officers_incidents` SET `officer_id` = :officer_id, `thedate` = :thedate, `message` = :message";
            $myrec = $mycon->prepare($sql);
            $myrec->bindValue(":thedate",date("Y-m-d"));
            $myrec->bindValue(":message",$message);
            $myrec->bindValue(":officer_id",$officerdetails['officer_id']);
            if(!$myrec->execute())
            {
                sendSMS($phone, "There was an error processing your report. Please send (Find ISS report SERVICE_NO)");
                return;
            }
            else
            {
                sendSMS($phone, "Thank you. Your report has been logged. Please send (Find ISS check SERVICE_NO) to view officer's information.");
                return;
            }
        }

}
//END saveMessage()///////////////////////////////////////

?>