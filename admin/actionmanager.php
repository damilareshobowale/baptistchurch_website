<?php
	$loginpage = "yes";
	require_once("config.php");
	require_once("inc_dbfunctions.php");
	
$actionmanager = New Actionmanager();
	
//Check what to do

//var_dump($_POST);
if(isset($_POST['command']) && $_POST['command'] == "login")
{
	$actionmanager->admin_login();
}
elseif(isset($_POST['command']) && $_POST['command'] == "banners_add")
{
	$actionmanager->banners_add();
}
elseif(isset($_POST['command']) && $_POST['command'] == "gallery_add")
{
	$actionmanager->gallery_add();
}
elseif(isset($_POST['command']) && $_POST['command'] == "contents_edit")
{
	$actionmanager->contents_update();
}
elseif(isset($_POST['command']) && $_POST['command'] == "news_add")
{
	$actionmanager->news_add();
}
elseif(isset($_POST['command']) && $_POST['command'] == "news_edit")
{
	$actionmanager->news_update();
}
elseif(isset($_POST['command']) && $_POST['command'] == "events_add")
{
	$actionmanager->events_add();
}
elseif(isset($_POST['command']) && $_POST['command'] == "birthdays_add")
{
	$actionmanager->birthdays_add();
}
elseif(isset($_POST['command']) && $_POST['command'] == "events_edit")
{
	$actionmanager->events_update();
}
elseif(isset($_POST['command']) && $_POST['command'] == "birthdays_edit")
{
	$actionmanager->birthdays_update();
}
elseif(isset($_POST['command']) && $_POST['command'] == "teams_add")
{
	$actionmanager->teams_add();
}
elseif(isset($_POST['command']) && $_POST['command'] == "teams_edit")
{
	$actionmanager->teams_update();
}
elseif(isset($_POST['command']) && $_POST['command'] == "blogs_add")
{
	$actionmanager->blogs_add();
}
elseif(isset($_POST['command']) && $_POST['command'] == "blogs_edit")
{
	$actionmanager->blogs_update();
}
elseif(isset($_POST['command']) && $_POST['command'] == "sermons_add")
{
	$actionmanager->sermons_add();
}
elseif(isset($_POST['command']) && $_POST['command'] == "mp3_sermons_add")
{
	$actionmanager->mp3_sermons_add();
}
elseif(isset($_POST['command']) && $_POST['command'] == "sermons_edit")
{
	$actionmanager->sermons_update();
}
elseif(isset($_POST['command']) && $_POST['command'] == "mp3_sermons_edit")
{
	$actionmanager->mp3_sermons_update();
}
elseif(isset($_POST['command']) && $_POST['command'] == "departments_add")
{
	$actionmanager->departments_add();
}
elseif(isset($_POST['command']) && $_POST['command'] == "departments_edit")
{
	$actionmanager->departments_update();
}
elseif(isset($_POST['command']) && $_POST['command'] == "departments_content_edit")
{
	$actionmanager->departments_content_update();
}
elseif(isset($_POST['command']) && $_POST['command'] == "departments_gallery_add")
{
	$actionmanager->departments_gallery_add();
}
elseif(isset($_POST['command']) && $_POST['command'] == "departments_articles_add")
{
	$actionmanager->departments_articles_add();
}
elseif(isset($_POST['command']) && $_POST['command'] == "departments_articles_edit")
{
	$actionmanager->departments_articles_update();
}
elseif(isset($_POST['command']) && $_POST['command'] == "pastors_add")
{
	$actionmanager->pastors_add();
}
elseif(isset($_POST['command']) && $_POST['command'] == "admins-add")
{
	$actionmanager->admins_add();
}
elseif(isset($_POST['command']) && $_POST['command'] == "admins-edit")
{
	$actionmanager->admins_update();
}
elseif(isset($_POST['command']) && $_POST['command'] == "admins-groups-add")
{
	$actionmanager->admins_groups_add();
}
elseif(isset($_POST['command']) && $_POST['command'] == "admins-groups-edit")
{
	$actionmanager->admins_groups_update();
}
elseif(isset($_POST['command']) && $_POST['command'] == "profile-edit")
{
	$actionmanager->profile_update();
}
elseif(isset($_POST['command']) && $_POST['command'] == "profile-password-edit")
{
	$actionmanager->profile_update_password();
}



class Actionmanager
{
    //Process login form
    function admin_login()
    {
            $mycon = databaseConnect();
            $username = $_POST['username'];
            $password = $_POST['password'];
            $thedate = date("Y-m-d H:i:s");

            //check if account exists

            if(strlen($username) < 1 || strlen($password) < 1)
            {
                showAlert("Please enter your username and pasword.");
                return;
            }

            $password = generatePassword($password);

            if($username != "administrator")
            {
                return $this->department_login($username, $password);
            }
            
            $mycon = databaseConnect();
            $dataread = New DataRead();
            $login = $dataread->admins_login($mycon,$username,$password);

            if(!$login)
            {
                    showAlert("You have provided an invalid login details.");
                    return;
            }
            
            if($login['status'] != "5")
            {
                    //showAlert("Your account is not active. Please contact the administrator.");
                    //return;
            }

            createCookie("userid",$login['admin_id']);
            createCookie("userlogin","NO");
            createCookie("departmentlogin","NO");
            createCookie("username",$login['username']);
            //createCookie("accounttype",$login['accounttype']);
            createCookie("fullname",$login['surname']." ".$login['firstname']);

            $loginip = $_SERVER['REMOTE_ADDR'];
            $logindevice = $_SERVER['HTTP_USER_AGENT'];

            $datawrite = New DataWrite();
            $datawrite->admins_logins($mycon,$login['admin_id'],$loginip,$logindevice);
            $datawrite->lastlogin($mycon,$login['admin_id']);
			
            
            createCookie("adminlogin","YES");

            openPage("blogs-list.php");

    }
    //End proLogin()//////////////////

    function department_login($username, $password)
    {
            $mycon = databaseConnect();

            //check if account exists
            //$sql = "SELECT `name`,`department_id` FROM `departments` WHERE `username` = :username AND `password` = :password LIMIT 1";
            $sql = "SELECT `name`,`department_id` FROM `departments` WHERE `username` = :username LIMIT 1";
            //$password = generatePassword($password);

            $stmt = $mycon->prepare($sql);

            $stmt->bindValue(":username",$username,PDO::PARAM_STR);
            //$stmt->bindValue(":password",$password,PDO::PARAM_STR);

            $stmt->execute();

            if($stmt->rowCount() < 1)
            {
                    showAlert("You have provided an invalid login details2. Please try again.");
                    return;
            }

            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            createCookie("userid",$row['department_id']);
            createCookie("departmentlogin","YES");
            createCookie("adminlogin","NO");
            createCookie("fullname",$row['name']);

            openPage("departments-gallery.php");

    }
    //End proLogin()//////////////////

    function banners_add()
    {
	$name = $_POST['name'];
	$caption = $_POST['caption'];
	$link = $_POST['url'];
	$banner = $_FILES['banner'];
        
        $currentuserid = getCookie("userid");
        
        
        $thedate = date("Y-m-d");

	//check if the fields where filled
	$msg = "";
	if(strlen($name) < 1) $msg .= "**Enter the banner title";
        if(!strpos(strtoupper($banner['type']),"IMAGE") > -1) $msg .= "**Select the banner image to upload";
	
	if($msg != "")
	{
		showAlert("Please correct the following before saving:**$msg");
		return;
	}

        $mycon = databaseConnect();
        $datawrite = New DataWrite();
                
        $banner_id = $datawrite->banners_add($mycon, $name, $caption, $link);

        if(!$banner_id)
        {
            showAlert("There was an error saving this banner. Please try again.");
            return;
        }
        else
        {
             move_uploaded_file($banner['tmp_name'],"../pictures/banners/$banner_id.jpg");
            
            $datawrite->admin_activities($mycon,$currentuserid,"Created new banner #$banner_id ($name)");
                        
            openPage("banners.php");
        }
    }

    function gallery_add()
    {
	$name = $_POST['name'];
	$category = $_POST['category'];
	$picture = $_FILES['picture'];
        
        $currentuserid = getCookie("userid");
        
        
        $thedate = date("Y-m-d");

	//check if the fields where filled
	$msg = "";
	if(strlen($category) < 1) $msg .= "**Enter the picture category";
        if(!strpos(strtoupper($picture['type']),"IMAGE") > -1) $msg .= "**Select the picture image to upload";
	
	if($msg != "")
	{
		showAlert("Please correct the following before saving:**$msg");
		return;
	}

        $mycon = databaseConnect();
        $datawrite = New DataWrite();
                
        $gallery_id = $datawrite->gallery_add($mycon, $name, $category);

        if(!$gallery_id)
        {
            showAlert("There was an error saving this picture. Please try again.");
            return;
        }
        else
        {
             move_uploaded_file($picture['tmp_name'],"../pictures/gallery/$gallery_id.jpg");
            
            $datawrite->admin_activities($mycon,$currentuserid,"Created new picture gallery #$gallery_id ($name)");
                        
            openPage("gallery.php");
        }
    }


    function contents_update()
    {	
	if(!isset($_POST['contents']) || !is_array($_POST['contents']))
	{
            showAlert("Contents could not be parsed. Please refresh this page and try again.");
            return;
	}

        $currentuserid = getCookie("userid");
        
        $thedate = date("Y-m-d");

        $mycon = databaseConnect();
        $datawrite = New DataWrite();

        //loop through and add or edit
        foreach($_POST['contents'] as $key => $value)
        {
            $done = $datawrite->contents_update($mycon, $key, $value);
        }
                
        //loop through and save images
        foreach($_FILES['pictures']['name'] as $key => $value)
        {
            $picture = $_FILES["pictures"];
            if(strpos(strtoupper($picture['type'][$key]),"IMAGE") > -1) 
            {
                    move_uploaded_file($picture['tmp_name'][$key],"../pictures/$key.jpg");
            }
        }
        
        showAlert("Content has been updated.");
    }


    function news_add()
    {
        $mycon = databaseConnect();

        $newsdate = "";//$_POST['txtdate'];
        $headline = $_POST['headline'];
        $content = $_POST['content'];
        $caption = $_POST['caption'];
        $thedate = date("Y-m-d");
        $banner = $_FILES['banner'];

        //check if a page name was specified
        if(strlen($headline) < 1)
        {
            showAlert("Please enter the headline of this content before saving.");
            return;

        }
        
        $dataWrite = New DataWrite();


        $news_id = $dataWrite->news_add($mycon, $newsdate, $headline, $caption, $content, $thedate);
        if(!$news_id)
        {
                showAlert("There was an error saving this news. Please try again.");
                return;
        }
        else
        {
                if(strpos(strtoupper($banner['type']),"IMAGE") > -1) 
                {
                        move_uploaded_file($banner['tmp_name'],"../pictures/news/$news_id.jpg");
                }

                showAlert("Done!!");
                openPage("news-list.php");
        }
    }

    function news_update()
    {
        $mycon = databaseConnect();

        $news_id = $_POST['news_id'];
        $newsdate = "";//$_POST['txtdate'];
        $headline = $_POST['headline'];
        $content = $_POST['content'];
        $caption = $_POST['caption'];
        $thedate = date("Y-m-d");
        $banner = $_FILES['banner'];

        //check if a page name was specified
        if(strlen($headline) < 1)
        {
            showAlert("Please enter the headline of this content before saving.");
            return;

        }
        
        $dataWrite = New DataWrite();


        $done = $dataWrite->news_update($mycon, $news_id, $newsdate, $headline, $caption, $content);
        if(!$done)
        {
                showAlert("There was an error saving this news. Please try again.");
                return;
        }
        else
        {
                if(strpos(strtoupper($banner['type']),"IMAGE") > -1) 
                {
                        move_uploaded_file($banner['tmp_name'],"../pictures/news/$news_id.jpg");
                }

                showAlert("Done!!");
                openPage("news-list.php");
        }
    }

    function events_add()
    {
        $mycon = databaseConnect();

        $name = $_POST['name'];
        $content = $_POST['content'];
        $caption = $_POST['caption'];
        $eventdate = $_POST['eventdate'];
        $thedate = date("Y-m-d");
        $banner = $_FILES['banner'];

        //check if a page name was specified
        if(strlen($name) < 1)
        {
            showAlert("Please enter the name of this event before saving.");
            return;

        }
        
        $dataWrite = New DataWrite();


        $event_id = $dataWrite->events_add($mycon, $eventdate, $name, $caption, $content, $thedate);
        if(!$event_id)
        {
                showAlert("There was an error saving this content. Please try again.");
                return;
        }
        else
        {
                if(strpos(strtoupper($banner['type']),"IMAGE") > -1) 
                {
                        move_uploaded_file($banner['tmp_name'],"../pictures/events/$event_id.jpg");
                }

                showAlert("Done!!");
                openPage("events-list.php");
        }
    }
    
     function birthdays_add()
    {
        $mycon = databaseConnect();

        $firstname = $_POST['firstname'];
        $lastname = $_POST['lastname'];
        $birthdaydate = $_POST['birthday'];
        $content = $_POST['content'];
        $thedate = date("Y-m-d");

        //check if a page name was specified
        if(strlen($firstname) < 1)
        {
            showAlert("Please enter the firstname before saving.");
            return;

        }
        
        //check if a page name was specified
        if(strlen($lastname) < 1)
        {
            showAlert("Please enter the lastname before saving.");
            return;

        }
        
        //check if a page name was specified
        if($birthdaydate == '')
        {
            showAlert("Please enter the birthday before saving.");
            return;

        }
        
        $dataWrite = New DataWrite();


        $event_id = $dataWrite->birthdays_add($mycon, $firstname, $lastname, $birthdaydate, $content, $thedate);
        if(!$event_id)
        {
                showAlert("There was an error saving this content. Please try again.");
                return;
        }
        
        showAlert("Done!!");
        openPage("events-birthdays-list.php");
        
    }
    
    function teams_add()
    {
        $mycon = databaseConnect();

        $team = $_POST['name'];
        $leader = $_POST['leader'];
        $composition = $_POST['composition'];
        $thedate = date("Y-m-d");

        //check if a page name was specified
        if(strlen($team) < 1)
        {
            showAlert("Please enter the team before saving.");
            return;

        }
        
        //check if a page name was specified
        if(strlen($leader) < 1)
        {
            showAlert("Please enter the team leader before saving.");
            return;

        }
        
        //check if a page name was specified
        if($composition == '')
        {
            showAlert("Please enter the various composition separated by comma(,) before saving.");
            return;

        }
        
        $dataWrite = New DataWrite();


        $event_id = $dataWrite->teams_add($mycon, $team, $leader, $composition, $thedate);
        if(!$event_id)
        {
                showAlert("There was an error saving this content. Please try again.");
                return;
        }
        
        showAlert("Done!!");
        openPage("events-leaders-list.php");
        
    }

    function events_update()
    {
        $mycon = databaseConnect();

        $event_id = $_POST['event_id'];
        $name = $_POST['name'];
        $content = $_POST['content'];
        $caption = $_POST['caption'];
        $eventdate = $_POST['eventdate'];
        $thedate = date("Y-m-d");
        $banner = $_FILES['banner'];

        //check if a page name was specified
        if(strlen($name) < 1)
        {
            showAlert("Please enter the name of this event before saving.");
            return;

        }
        
        $dataWrite = New DataWrite();


        $done = $dataWrite->events_update($mycon, $event_id, $eventdate, $name, $caption, $content);
        if(!$done)
        {
                showAlert("There was an error saving this content. Please try again.");
                return;
        }
        else
        {
                if(strpos(strtoupper($banner['type']),"IMAGE") > -1) 
                {
                        move_uploaded_file($banner['tmp_name'],"../pictures/events/$event_id.jpg");
                }

                showAlert("Done!!");
                openPage("events-list.php");
        }
    }
    
     function birthdays_update()
    {
        $mycon = databaseConnect();

        $birthday_id = $_POST['birthday_id'];
        $firstname = $_POST['firstname'];
        $lastname = $_POST['lastname'];
        $content = $_POST['content'];
        $birthdaydate = $_POST['birthdaydate'];
        $thedate = date("Y-m-d");

        //check if a page name was specified
        if(strlen($firstname) < 1)
        {
            showAlert("Please enter the firstname before saving.");
            return;

        }
        
        //check if a page name was specified
        if(strlen($lastname) < 1)
        {
            showAlert("Please enter the lastname before saving.");
            return;

        }
        
        //check if a page name was specified
        if($birthdaydate == '')
        {
            showAlert("Please enter the birthday before saving.");
            return;

        }
        
        $dataWrite = New DataWrite();


        $done = $dataWrite->birthdays_update($mycon, $birthday_id, $firstname, $lastname, $birthdaydate, $content);
        if(!$done)
        {
                showAlert("There was an error saving this content. Please try again.");
                return;
        }
        
        showAlert("Done!!");
        openPage("events-birthdays-list.php");
       
    }
    
    
     function teams_update()
    {
        $mycon = databaseConnect();

        $team_id = $_POST['team_id'];
        $name = $_POST['name'];
        $leader = $_POST['leader'];
        $composition = $_POST['composition'];
        $thedate = date("Y-m-d");

        //check if a page name was specified
        if(strlen($name) < 1)
        {
            showAlert("Please enter the name before saving.");
            return;

        }
        
        //check if a page name was specified
        if(strlen($leader) < 1)
        {
            showAlert("Please enter the leader before saving.");
            return;

        }
        
        //check if a page name was specified
        if(strlen($composition) < 1)
        {
            showAlert("Please enter the team composition before saving.");
            return;

        }
        
        $dataWrite = New DataWrite();


        $done = $dataWrite->teams_update($mycon, $team_id, $name, $leader, $composition);
        if(!$done)
        {
                showAlert("There was an error saving this content. Please try again.");
                return;
        }
        
        showAlert("Done!!");
        openPage("events-leaders-list.php");
       
    }

    function blogs_add()
    {
        $mycon = databaseConnect();

        $headline = $_POST['headline'];
        $content = $_POST['content'];
        $caption = $_POST['caption'];
        $thedate = date("Y-m-d");
        $banner = $_FILES['banner'];

        //check if a page name was specified
        if(strlen($headline) < 1)
        {
            showAlert("Please enter the headline of this content before saving.");
            return;

        }
        
        $dataWrite = New DataWrite();


        $blog_id = $dataWrite->blogs_add($mycon, $headline, $caption, $content, $thedate);
        if(!$blog_id)
        {
                showAlert("There was an error saving this content. Please try again.");
                return;
        }
        else
        {
                if(strpos(strtoupper($banner['type']),"IMAGE") > -1) 
                {
                        move_uploaded_file($banner['tmp_name'],"../pictures/blogs/$blog_id.jpg");
                }

                showAlert("Done!!");
                openPage("blogs-list.php");
        }
    }

    function blogs_update()
    {
        $mycon = databaseConnect();

        $blog_id = $_POST['blog_id'];
        $headline = $_POST['headline'];
        $content = $_POST['content'];
        $caption = $_POST['caption'];
        $thedate = date("Y-m-d");
        $banner = $_FILES['banner'];

        //check if a page name was specified
        if(strlen($headline) < 1)
        {
            showAlert("Please enter the headline of this content before saving.");
            return;

        }
        
        $dataWrite = New DataWrite();


        $done = $dataWrite->blogs_update($mycon, $blog_id, $headline, $caption, $content);
        if(!$done)
        {
                showAlert("There was an error saving this content. Please try again.");
                return;
        }
        else
        {
                if(strpos(strtoupper($banner['type']),"IMAGE") > -1) 
                {
                        move_uploaded_file($banner['tmp_name'],"../pictures/blogs/$blog_id.jpg");
                }

                showAlert("Done!!");
                openPage("blogs-list.php");
        }
    }

    function sermons_add()
    {
        $mycon = databaseConnect();

        $headline = $_POST['headline'];
        $content = $_POST['content'];
        $caption = $_POST['caption'];
        $embedcode = $_POST['embedcode'];
        $thedate = $_POST["sermondate"];
        $banner = $_FILES['banner'];

        //check if a page name was specified
        if(strlen($headline) < 1)
        {
            showAlert("Please enter the headline of this content before saving.");
            return;

        }
        
        $dataWrite = New DataWrite();


        $sermon_id = $dataWrite->sermons_add($mycon, $headline, $caption, $content, $embedcode, $thedate);
        if(!$sermon_id)
        {
                showAlert("There was an error saving this content. Please try again.");
                return;
        }
        else
        {
                if(strpos(strtoupper($banner['type']),"IMAGE") > -1) 
                {
                        move_uploaded_file($banner['tmp_name'],"../pictures/sermons/$sermon_id.jpg");
                }

                showAlert("Done!!");
                openPage("sermons-list.php");
        }
    }
    
    function mp3_sermons_add()
    {
        $mycon = databaseConnect();
        $title = $_POST['title'];
        $minister = $_POST['minister'];
        $thedate = $_POST['sermondate'];
        $mp3 = $_FILES['mp3'];
        
        //check if a page name was specified
        if(strlen($title) < 1)
        {
            showAlert("Please enter the title of this content before saving.");
            return;

        }
        
        //check if a page name was specified
        if(strlen($minister) < 1)
        {
            showAlert("Please enter the minister name of this content before saving.");
            return;

        }
        
        
        
        $dataWrite = New DataWrite();


        $sermon_id = $dataWrite->mp3_sermons_add($mycon, $title, $minister, $thedate);
        if(!$sermon_id)
        {
                showAlert("There was an error saving this content. Please try again.");
                return;
        }
        else
        {
                if(strpos(strtoupper($mp3['type']),"AUDIO") > -1) 
                {
                        move_uploaded_file($mp3['tmp_name'],"../pictures/sermons_mp3/$sermon_id.mp3");
                }

                showAlert("Done!!");
                openPage("sermons-list-mp3.php");
        }
    }


    function sermons_update()
    {
        $mycon = databaseConnect();

        $sermon_id = $_POST['sermon_id'];
        $headline = $_POST['headline'];
        $content = $_POST['content'];
        $caption = $_POST['caption'];
        $embedcode = $_POST['embedcode'];
        $thedate = $_POST["sermondate"];
        $banner = $_FILES['banner'];

        //check if a page name was specified
        if(strlen($headline) < 1)
        {
            showAlert("Please enter the headline of this content before saving.");
            return;

        }
        
        $dataWrite = New DataWrite();


        $done = $dataWrite->sermons_update($mycon, $sermon_id, $headline, $caption, $content, $embedcode, $thedate);
        if(!$done)
        {
                showAlert("There was an error saving this content. Please try again.");
                return;
        }
        else
        {
                if(strpos(strtoupper($banner['type']),"IMAGE") > -1) 
                {
                        move_uploaded_file($banner['tmp_name'],"../pictures/sermons/$sermon_id.jpg");
                }

                showAlert("Done!!");
                openPage("sermons-list.php");
        }
    }
    
    
    function mp3_sermons_update()
    {
        $mycon = databaseConnect();

        $sermon_id = $_POST['sermon_id'];
        $title = $_POST['title'];
        $minister = $_POST['minister'];
        $thedate = $_POST['sermondate'];
        $mp3 = $_FILES['mp3'];
        
        
        //check if a page name was specified
        if(strlen($title) < 1)
        {
            showAlert("Please enter the title of this content before saving.");
            return;

        }
        
        //check if a page name was specified
        if(strlen($minister) < 1)
        {
            showAlert("Please enter the minister name of this content before saving.");
            return;

        }
        
        //check if a page name was specified
        if($mp3 == null)
        {
            showAlert("Please upload the sermon mp3 of this content before saving.");
            return;

        }
        
        $dataWrite = New DataWrite();


        $done = $dataWrite->mp3_sermons_update($mycon, $sermon_id, $title, $minister, $thedate);
        if(!$done)
        {
                showAlert("There was an error saving this content. Please try again.");
                return;
        }
        else
        {
                if(strpos(strtoupper($mp3['type']),"AUDIO") > -1) 
                {
                        move_uploaded_file($banner['tmp_name'],"../pictures/sermons_mp3/$sermon_id.jpg");
                }

                showAlert("Done!!");
                openPage("sermons-list-mp3.php");
        }
    }

    function departments_add()
    {
        $mycon = databaseConnect();

        $name = $_POST['name'];
        $caption = $_POST['caption'];
        $username = $_POST['username'];
        $password = $_POST['password'];
        $thedate = date("Y-m-d");
        $banner = $_FILES['banner'];

        //check if a page name was specified
        if(strlen($name) < 1)
        {
            showAlert("Please enter the name of this department before saving.");
            return;

        }
        
        $dataWrite = New DataWrite();
        $dataRead = New DataRead();
        
        //check if username exists
        $details = $dataRead->departments_getbyusername($mycon, $username);
        if($details != false)
        {
            showAlert("The specified username is already assigned to a department. Please use a different username.");
            return;            
        }
        
        $password = generatePassword($password);

        $department_id = $dataWrite->departments_add($mycon, $name, $caption, $username, $password);
        if(!$department_id)
        {
                showAlert("There was an error saving this department. Please try again.");
                return;
        }
        else
        {
                if(strpos(strtoupper($banner['type']),"IMAGE") > -1) 
                {
                        move_uploaded_file($banner['tmp_name'],"../pictures/departments/{$department_id}_1.jpg");
                }

                showAlert("Done!!");
                openPage("departments-list.php");
        }
    }


    function departments_update()
    {
        $mycon = databaseConnect();

        $department_id = $_POST['department_id'];
        $name = $_POST['name'];
        $caption = $_POST['caption'];
        $password = $_POST['password'];
        $banner = $_FILES['banner'];

        //check if a page name was specified
        if(strlen($name) < 1)
        {
            showAlert("Please enter the name of this department before saving.");
            return;

        }
        
        $dataWrite = New DataWrite();
        $dataRead = New DataRead();
        
        //check if username exists
        $details = $dataRead->departments_get($mycon, $department_id);
        if(!$details)
        {
            showAlert("The details of this department could not be loaed. Please refresh this page and try again.");
            return;            
        }
        
        $password = (trim($password) == "") ? generatePassword($password) : $details['password'];

        $done = $dataWrite->departments_update($mycon, $department_id, $name, $caption, $password);
        if(!$done)
        {
                showAlert("There was an error saving this department. Please try again.");
                return;
        }
        else
        {
                if(strpos(strtoupper($banner['type']),"IMAGE") > -1) 
                {
                        move_uploaded_file($banner['tmp_name'],"../pictures/departments/{$department_id}_1.jpg");
                }

                showAlert("Done!!");
                openPage("departments-list.php");
        }
    }

    function departments_content_update()
    {
        $mycon = databaseConnect();

        $department_id = $_POST['department_id'];
        $content = $_POST['content'];
        $banner = $_FILES['banner'];
        
        $dataWrite = New DataWrite();
        $dataRead = New DataRead();
        
        //check if username exists
        $details = $dataRead->departments_get($mycon, $department_id);
        if(!$details)
        {
            showAlert("The details of this department could not be loaed. Please refresh this page and try again.");
            return;            
        }

        $done = $dataWrite->departments_content_update($mycon, $department_id, $content);
        if(!$done)
        {
                showAlert("There was an error saving this department. Please try again.");
                return;
        }
        else
        {
                if(strpos(strtoupper($banner['type']),"IMAGE") > -1) 
                {
                        move_uploaded_file($banner['tmp_name'],"../pictures/departments/{$department_id}_2.jpg");
                }

                showAlert("Done!!");
                openPage("departments-content.php");
        }
    }



    function departments_gallery_add()
    {
	$department_id = $_POST['department_id'];
	$name = $_POST['name'];
	$picture = $_FILES['picture'];
        
        $currentuserid = getCookie("userid");
        
        
        $thedate = date("Y-m-d");

	//check if the fields where filled
	$msg = "";
        if(!strpos(strtoupper($picture['type']),"IMAGE") > -1) $msg .= "**Select the picture image to upload";
	
	if($msg != "")
	{
		showAlert("Please correct the following before saving:**$msg");
		return;
	}

        $mycon = databaseConnect();
        $datawrite = New DataWrite();
                
        $gallery_id = $datawrite->departments_gallery_add($mycon, $department_id, $name);

        if(!$gallery_id)
        {
            showAlert("There was an error saving this picture. Please try again.");
            return;
        }
        else
        {
             move_uploaded_file($picture['tmp_name'],"../pictures/departments_gallery/$gallery_id.jpg");
                        
            openPage("departments-gallery.php");
        }
    }

    function departments_articles_add()
    {
        $mycon = databaseConnect();

        $department_id = $_POST['department_id'];
        $headline = $_POST['headline'];
        $content = $_POST['content'];
        $caption = $_POST['caption'];
        $thedate = date("Y-m-d");
        $banner = $_FILES['banner'];

        //check if a page name was specified
        if(strlen($headline) < 1)
        {
            showAlert("Please enter the headline of this content before saving.");
            return;

        }
        
        $dataWrite = New DataWrite();


        $article_id = $dataWrite->departments_articles_add($mycon, $department_id, $headline, $caption, $content);
        if(!$article_id)
        {
                showAlert("There was an error saving this content. Please try again.");
                return;
        }
        else
        {
                if(strpos(strtoupper($banner['type']),"IMAGE") > -1) 
                {
                        move_uploaded_file($banner['tmp_name'],"../pictures/departments_articles/$article_id.jpg");
                }

                showAlert("Done!!");
                openPage("departments-articles-list.php");
        }
    }

    function departments_articles_update()
    {
        $mycon = databaseConnect();

        $department_id = $_POST['department_id'];
        $article_id = $_POST['article_id'];
        $headline = $_POST['headline'];
        $content = $_POST['content'];
        $caption = $_POST['caption'];
        $thedate = date("Y-m-d");
        $banner = $_FILES['banner'];

        //check if a page name was specified
        if(strlen($headline) < 1)
        {
            showAlert("Please enter the headline of this content before saving.");
            return;

        }
        
        $dataWrite = New DataWrite();


        $done = $dataWrite->departments_articles_update($mycon, $department_id, $article_id, $headline, $caption, $content);
        if(!$done)
        {
                showAlert("There was an error saving this content. Please try again.");
                return;
        }
        else
        {
                if(strpos(strtoupper($banner['type']),"IMAGE") > -1) 
                {
                        move_uploaded_file($banner['tmp_name'],"../pictures/departments_articles/$article_id.jpg");
                }

                showAlert("Done!!");
                openPage("departments-articles-list.php");
        }
    }


    

    function pastors_add()
    {
	$name = $_POST['name'];
	$rank = $_POST['rank'];
	$position = $_POST['position'];
	$photo = $_FILES['photo'];
        
        $currentuserid = getCookie("userid");
        
        
        $thedate = date("Y-m-d");

	//check if the fields where filled
	$msg = "";
	if(strlen($name) < 1) $msg .= "**Enter the name";
	if(strlen($name) < 1) $msg .= "**Enter the rank";
        if(!strpos(strtoupper($photo['type']),"IMAGE") > -1) $msg .= "**Select the photo image to upload";
	
	if($msg != "")
	{
		showAlert("Please correct the following before saving:**$msg");
		return;
	}

        $mycon = databaseConnect();
        $datawrite = New DataWrite();
                
        $pastor_id = $datawrite->pastors_add($mycon, $name, $rank, $position);

        if(!$pastor_id)
        {
            showAlert("There was an error saving this banner. Please try again.");
            return;
        }
        else
        {
             move_uploaded_file($photo['tmp_name'],"../pictures/pastors/$pastor_id.jpg");
            
            $datawrite->admin_activities($mycon,$currentuserid,"Created new pastor #$pastor_id ($name)");
                        
            openPage("pastors.php");
        }
    }

    
    
    

    function admins_add()
    {
	$surname = $_POST['surname'];
	$firstname = $_POST['firstname'];
	$othernames = $_POST['othernames'];
	$email = $_POST['email'];
	$phone = $_POST['phone'];
	$username = $_POST['username'];
	$password = $_POST['password'];
	$password2 = $_POST['password2'];
	$group_id = "";//$_POST['group_id'];
        
        $currentuserid = getCookie("userid");
	if(getUserAccessRight($currentuserid,"users_add") <> 1)
	{
		showAlert("You do not have access to perform this action");
		return;
	}
        
        $thedate = date("Y-m-d");

	//check if the fields where filled
	$msg = "";
	if(strlen($surname) < 1) $msg .= "**Enter the surname.";
	if(strlen($firstname) < 1) $msg .= "**Enter the firstname.";
	if(strlen($username) < 1) $msg .= "**Enter the login username.";
	if(strlen($password) < 1) $msg .= "**Enter the login password.";
	if(strlen($password) > 1 && $password != $password2) $msg .= "**Login Password confirmation does not match.";
	
	if($msg != "")
	{
		showAlert("Please correct the following before saving:**$msg");
		return;
	}
	

        $password = generatePassword($password);

        $mycon = databaseConnect();
        $dataread = New DataRead();
        if($dataread->admins_getbyusername($mycon, $username) != false)
	{
		showAlert("The specified username already exists. Please use a different username.");
		return;
	}
        
        $datawrite = New DataWrite();
        $admin_id = $datawrite->admins_add($mycon, $surname, $firstname, $othernames, $email, $phone, $group_id, $username, $password, $currentuserid, $thedate);
        
        if(!$admin_id)
        {
            showAlert("There was an error saving this admin. Please try again.");
            return;
        }
        else
        {

            $datawrite->admin_activities($mycon,$currentuserid,"Created new admin #$admin_id ($surname $firstname)");
            openPage("admins-list.php");
        }
    }


    function admins_update()
    {
	$admin_id = $_POST['admin_id'];
        $surname = $_POST['surname'];
	$firstname = $_POST['firstname'];
	$othernames = $_POST['othernames'];
	$email = $_POST['email'];
	$phone = $_POST['phone'];
	$status = $_POST['status'];
	$group_id = "";//$_POST['group_id'];
	$password = $_POST['password'];
	$password2 = $_POST['password2'];
        
        $currentuserid = getCookie("userid");
	if(getUserAccessRight($currentuserid,"users_edit") <> 1)
	{
		showAlert("You do not have access to perform this action");
		return;
	}
        
        $thedate = date("Y-m-d");

	//check if the fields where filled
	$msg = "";
	if(strlen($surname) < 1) $msg .= "**Enter the surname.";
	if(strlen($firstname) < 1) $msg .= "**Enter the firstname.";
	if(strlen($password) > 0 && $password2 != $password) $msg .= "**The password confirmation does not match.";
	
	if($msg != "")
	{
		showAlert("Please correct the following before saving:**$msg");
		return;
	}

        $mycon = databaseConnect();        
        $dataread = New DataRead();        
        $datawrite = New DataWrite();
	
        $admindetails = $dataread->admins_get($mycon, $admin_id);
        if(!$admindetails)
        {
            showAlert("The details of this user could not be loaded. Please refresh this page and try again.");
            return;
        }
        
        if(strlen($password) > 0)
        {
            $password = generatePassword($password);
        }
        else
        {
            $password = $admindetails['password'];
        }
        
        $done = $datawrite->admins_update($mycon, $admin_id, $surname, $firstname, $othernames, $email, $phone, $group_id, $password, $status, $currentuserid);
        
        if(!$done)
        {
            showAlert("There was an error saving this admin. Please try again.");
            return;
        }
        else
        {
            showAlert("The details has been updated.");
            $datawrite->admin_activities($mycon,$currentuserid,"Updated admin details #$admin_id ($surname $firstname)");
            openPage("admins-list.php");
        }
    }

    //Process new user usroup form
    function admins_groups_add()
    {
            $mycon = databaseConnect();
            $name = $_POST['name'];
            $description = $_POST['description'];
            $thedate = date("Y-m-d H:i:s");

            $currentuserid = getCookie("userid");
            if(getUserAccessRight($currentuserid,"users_groups") <> 1)
            {
                    showAlert("You do not have access to perform this action");
                    return;
            }

            //check if the fields where filled
            $msg = "";
            if(strlen($name) < 3) $msg .= "**Enter the name for  this group.";

            if($msg != "")
            {
                    showAlert("Please correct the following before saving:**$msg");
                    return;
            }

            $mycon = databaseConnect();
            $dataread = New DataRead();
            if($dataread->admins_groups_getbyname($mycon, $name) != false)
            {
                    showAlert("There is an existing usergroup with the name provided.");
                    return;
            }

            //getthe list of selected rights
            $selected = "";
            if(isset($_POST['chk']) && is_array($_POST['chk']))
            {
                    $rights = $_POST['chk'];
                    foreach($rights as $right)
                    {
                            $selected .= "| ".$right;
                    }
            }

            $datawrite = New DataWrite();
            $group_id = $datawrite->admins_groups_add($mycon, $name, $selected, $description, $currentuserid);

            if(!$group_id)
            {
                showAlert("There was an error saving this group. Please try again.");
                return;
            }
            else
            {

                $datawrite->admin_activities($mycon,$currentuserid,"Created new admin group #$group_id ($name)");
                openPage("admins-groups-list.php");
            }

    }
    //End pro_users_groups_add()//////////////////
    
    function admins_groups_update()
    {
            $mycon = databaseConnect();
            $group_id = $_POST['group_id'];
            $name = $_POST['name'];
            $description = $_POST['description'];
            $thedate = date("Y-m-d H:i:s");

            $currentuserid = getCookie("userid");
            if(getUserAccessRight($currentuserid,"users_groups") <> 1)
            {
                    showAlert("You do not have access to perform this action");
                    return;
            }

            //check if the fields where filled
            $msg = "";
            if(strlen($name) < 3) $msg .= "**Enter the name for  this group.";

            if($msg != "")
            {
                    showAlert("Please correct the following before saving:**$msg");
                    return;
            }

            $mycon = databaseConnect();
            $dataread = New DataRead();
            $found = $dataread->admins_groups_getbyname($mycon, $name);
            if($found != false && $found['group_id'] != $group_id)
            {
                    showAlert("There is an existing usergroup with the name provided.");
                    return;
            }

            //getthe list of selected rights
            $selected = "";
            if(isset($_POST['chk']) && is_array($_POST['chk']))
            {
                    $rights = $_POST['chk'];
                    foreach($rights as $right)
                    {
                            $selected .= "| ".$right;
                    }
            }

            $datawrite = New DataWrite();
            $group_id = $datawrite->admins_groups_update($mycon, $group_id, $name, $selected, $description, $currentuserid);

            if(!$group_id)
            {
                showAlert("There was an error saving this group. Please try again.");
                return;
            }
            else
            {
                showAlert("Admin group has been updated.");
                $datawrite->admin_activities($mycon,$currentuserid,"Updated admin group #$group_id ($name)");
                openPage("admins-groups-list.php");
            }

    }
    //End pro_users_groups_add()//////////////////
    
    function profile_update()
    {
	$admin_id = $_POST['admin_id'];
        $surname = $_POST['surname'];
	$firstname = $_POST['firstname'];
	$othernames = $_POST['othernames'];
	$phone = $_POST['phone'];
        
        
        $currentuserid = getCookie("userid");
	if(getCookie("userlogin") != "YES")
	{
		showAlert("Your login session could not be validated. Please refresh this page or login again.");
		return;
	}
        if($admin_id != $currentuserid)
	{
		showAlert("Your account could not be validated. Please refresh this page and try again.");
		return;
	}
        
        $thedate = date("Y-m-d");

	//check if the fields where filled
	$msg = "";
	if(strlen($surname) < 1) $msg .= "**Enter the surname.";
	if(strlen($firstname) < 1) $msg .= "**Enter the firstname.";
	
	if($msg != "")
	{
		showAlert("Please correct the following before saving:**$msg");
		return;
	}
	

        $mycon = databaseConnect();        
        $datawrite = New DataWrite();
        $done = $datawrite->profile_update($mycon, $admin_id, $surname, $firstname, $othernames, $phone);
        
        if(!$done)
        {
            showAlert("There was an error saving your profile. Please try again.");
            return;
        }
        else
        {
            showAlert("Your profile has been updated.");
            $datawrite->admin_activities($mycon,$currentuserid,"Updated profile details #$admin_id ($surname $firstname)");
            openPage("profile-view.php");
        }
    }


    function profile_update_password()
    {
	$admin_id = $_POST['admin_id'];
	$password = $_POST['password'];
	$password2 = $_POST['password2'];
        
        $currentuserid = getCookie("userid");
	if(getCookie("userlogin") != "YES")
	{
		showAlert("Your login session could not be validated. Please refresh this page or login again.");
		return;
	}
        if($admin_id != $currentuserid)
	{
		showAlert("Your account could not be validated. Please refresh this page and try again.");
		return;
	}
        
        $thedate = date("Y-m-d");
        
        if(strlen($password) == 0) return false;

	//check if the fields where filled
	$msg = "";
	if(strlen($password) > 0 && $password != $password2) $msg .= "**Your password confirmation does not match.";
	
	if($msg != "")
	{
		showAlert("Please correct the following before saving:**$msg");
		return;
	}
	
        $mycon = databaseConnect();
        $datawrite = New DataWrite();
        $dataRead = New DataRead();
        
        $admindetails = $dataRead->admins_get($mycon, $admin_id);
        if(!$admindetails)
        {
		showAlert("Your account details could not be loaded. Please refresh this page and try again.");
		return;            
        }
        
        $newpassword = $admindetails['password'];
        if(strlen($password) > 0) $newpassword = generatePassword ($password);
        
        $done = $datawrite->profile_update_password($mycon, $admin_id, $newpassword);
        
        if(!$done)
        {
            showAlert("There was an error updating your login information. Please try again.");
            return;
        }
        else
        {

            $datawrite->admin_activities($mycon,$currentuserid,"Updated Login information");
            showAlert("Your login account information has been updated.");
            openPage("profile-view.php");
        }
    }

    
    
    
}///// end class




?>