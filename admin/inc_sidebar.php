        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Christ Baptist Church</a> 
            </div>
  <div style="color: white;
padding: 15px 50px 5px 50px;
float: right;
font-size: 16px;"> Welcome <?php echo getCookie("fullname") ?> &nbsp; <a href="index.php" class="btn btn-danger square-btn-adjust">Logout</a> </div>
        </nav>   
           <!-- /. NAV TOP  -->
                <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
				<li class="text-center">
                                    <img src="" class="user-image img-responsive"/>
					</li>
				
	<?php if(getCookie("adminlogin") == "YES") { ?>				
                    <li>
                        <a class=" <?php if(strpos(CurrentPageURL(),"banners.php") !== false) echo 'active-menu' ?>"  href="banners.php"><i class="fa fa-arrow-circle-right fa-3x"></i> Banners</a>
                    </li>
                    <li>
                        <a class=" <?php if(strpos(CurrentPageURL(),"gallery.php") !== false) echo 'active-menu' ?>"  href="gallery.php"><i class="fa fa-arrow-circle-right fa-3x"></i> Photo Gallery</a>
                    </li>
                    <li>
                        <a class=" <?php if(strpos(CurrentPageURL(),"homepage.php") !== false) echo 'active-menu' ?>"  href="homepage.php"><i class="fa fa-arrow-circle-right fa-3x"></i> Home page</a>
                    </li>
                    <li>
                        <a class=" <?php if(strpos(CurrentPageURL(),"aboutpage.php") !== false) echo 'active-menu' ?>"  href="aboutpage.php"><i class="fa fa-arrow-circle-right fa-3x"></i> About page</a>
                    </li>
                    <li>
                        <a class=" <?php if(strpos(CurrentPageURL(),"pastors.php") !== false) echo 'active-menu' ?>"  href="pastors.php"><i class="fa fa-arrow-circle-right fa-3x"></i> Pastors/Workers</a>
                    </li>
                    <li>
                        <a class=" <?php if(strpos(CurrentPageURL(),"departments-") !== false) echo 'active-menu' ?>" href="#"><i class="fa fa-arrow-circle-right fa-3x"></i> Departments<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level <?php if(strpos(CurrentPageURL(),"departments-") !== false) echo 'collapse in' ?>">
                            <li>
                                <a href="departments-list.php">View Departments</a>
                            </li>
                            <li>
                                <a href="departments-add.php">Add Department</a>
                            </li>
                            <li>
                                <a href="events-leaders-add.php">Add Team & Leaders</a>
                            </li>
                            <li>
                                <a href="events-leaders-list.php">View Team & Leaders</a>
                            </li>
                        </ul>
                    </li>  
                    <li>
                        <a class=" <?php if(strpos(CurrentPageURL(),"events-") !== false) echo 'active-menu' ?>" href="#"><i class="fa fa-arrow-circle-right fa-3x"></i> Events<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level <?php if(strpos(CurrentPageURL(),"events-") !== false) echo 'collapse in' ?>">
                            <li>
                                <a href="events-list.php">View Events</a>
                            </li>
                            <li>
                                <a href="events-add.php">Add Event</a>
                            </li>
                            <li>
                                <a href="events-birthdays-add.php">Add Birthdays</a>
                            </li>
                            <li>
                                <a href="events-birthdays-list.php">View Birthdays</a>
                            </li>
                        </ul>
                    </li>  
                    <li>
                        <a class=" <?php if(strpos(CurrentPageURL(),"news-") !== false) echo 'active-menu' ?>" href="#"><i class="fa fa-arrow-circle-right fa-3x"></i> News<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level <?php if(strpos(CurrentPageURL(),"news-") !== false) echo 'collapse in' ?>">
                            <li>
                                <a href="news-list.php">View News</a>
                            </li>
                            <li>
                                <a href="news-add.php">Add News</a>
                            </li>
                        </ul>
                    </li>  
                    <li>
                        <a class=" <?php if(strpos(CurrentPageURL(),"blogs-") !== false) echo 'active-menu' ?>" href="#"><i class="fa fa-arrow-circle-right fa-3x"></i> Blog<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level <?php if(strpos(CurrentPageURL(),"blogs-") !== false) echo 'collapse in' ?>">
                            <li>
                                <a href="blogs-list.php">View Blog posts</a>
                            </li>
                            <li>
                                <a href="blogs-add.php">Create new post</a>
                            </li>
                        </ul>
                    </li>  
                    <li>
                        <a class=" <?php if(strpos(CurrentPageURL(),"sermons-") !== false) echo 'active-menu' ?>" href="#"><i class="fa fa-arrow-circle-right fa-3x"></i> Sermons<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level <?php if(strpos(CurrentPageURL(),"sermons-") !== false) echo 'collapse in' ?>">
                            <li>
                                <a href="sermons-list.php">View Sermons</a>
                            </li>
                            <li>
                                <a href="sermons-add.php">Add Sermon</a>
                            </li>
                            <li>
                                <a href="sermons-list-mp3.php">View MP3 Sermon</a>
                            </li>
                            <li>
                                <a href="sermons-add-mp3.php">Add MP3 Sermon</a>
                            </li>
                        </ul>
                    </li>  
                    <li>
                        <a class=" <?php if(strpos(CurrentPageURL(),"admins-") !== false) echo 'active-menu' ?>" href="#"><i class="fa fa-users fa-3x"></i> Administrators<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level <?php if(strpos(CurrentPageURL(),"admins-") !== false) echo 'collapse in' ?>">
                            <li>
                                <a href="admins-list.php">View Administrators</a>
                            </li>
                            <li>
                                <a href="admins-add.php">Add New Administrator</a>
                            </li>
                        </ul>
                    </li>  
        <?php } if(getCookie("departmentlogin") == "YES") { ?>				
                    <li>
                        <a class=" <?php if(strpos(CurrentPageURL(),"gallery.php") !== false) echo 'active-menu' ?>"  href="departments-gallery.php"><i class="fa fa-arrow-circle-right fa-3x"></i> Photo Gallery</a>
                    </li>
                    <li>
                        <a class=" <?php if(strpos(CurrentPageURL(),"content.php") !== false) echo 'active-menu' ?>"  href="departments-content.php"><i class="fa fa-arrow-circle-right fa-3x"></i> Content page</a>
                    </li>
                    <li>
                        <a class=" <?php if(strpos(CurrentPageURL(),"articles-") !== false) echo 'active-menu' ?>" href="#"><i class="fa fa-arrow-circle-right fa-3x"></i> Articles<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level <?php if(strpos(CurrentPageURL(),"articles-") !== false) echo 'collapse in' ?>">
                            <li>
                                <a href="departments-articles-list.php">View Articles</a>
                            </li>
                            <li>
                                <a href="departments-articles-add.php">Create new article</a>
                            </li>
                        </ul>
                    </li>  
                    
        <?php } ?>            
                </ul>
               
            </div>
<iframe name="actionframe" id="actionframe" width="300px" height="300px" frameborder="0"></iframe> 
            
        </nav>  
           
<script type="text/javascript">
	window.setTimeout(function(){
    	document.location.href="index.php?logout=yes";
    },900000);
</script>