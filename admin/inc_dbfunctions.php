<?php
	require_once("config.php");


class DataWrite
{
    //Process last login
    function lastLogin($mycon, $admin_id)
    {

        $thedate = date("Y-m-d H:i:s");

        $sql = "UPDATE `admins` SET `lastlogin` = :thedate WHERE `admin_id` = :admin_id";
        $myrec =  $mycon->prepare($sql);
        $myrec->bindValue(":thedate",$thedate);
        $myrec->bindValue(":admin_id",$admin_id);
        $myrec->execute();

        if($myrec->rowCount() < 1)
        {
                return false;
        }
        
        return $mycon->lastInsertId();
    }


    function banners_add($mycon, $name, $caption, $link)
    {

	$sql = "INSERT INTO `banners` SET `name` = :name
			,`caption` = :caption
			,`link` = :link";
        
	$myrec =  $mycon->prepare($sql);
	$myrec->bindValue(":name",$name);
	$myrec->bindValue(":caption",$caption);
	$myrec->bindValue(":link",$link);
	$myrec->execute();

        if($myrec->rowCount() < 1)
        {
                return false;
        }
        
        return $mycon->lastInsertId();
        
    }

    function banners_delete($mycon, $banner_id)
    {

	$sql = "DELETE FROM `banners` WHERE `banner_id` = :banner_id";
        
	$myrec =  $mycon->prepare($sql);
	$myrec->bindValue(":banner_id",$banner_id);

        if(!$myrec->execute())
        {
                return false;
        }
        
        return true;
    }

    function gallery_add($mycon, $name, $category)
    {

	$sql = "INSERT INTO `gallery` SET `name` = :name
			,`category` = :category";
        
	$myrec =  $mycon->prepare($sql);
	$myrec->bindValue(":name",$name);
	$myrec->bindValue(":category",$category);
	$myrec->execute();

        if($myrec->rowCount() < 1)
        {
                return false;
        }
        
        return $mycon->lastInsertId();
        
    }

    function gallery_delete($mycon, $gallery_id)
    {

	$sql = "DELETE FROM `gallery` WHERE `gallery_id` = :gallery_id";
        
	$myrec =  $mycon->prepare($sql);
	$myrec->bindValue(":gallery_id",$gallery_id);

        if(!$myrec->execute())
        {
                return false;
        }
        
        return true;
    }
        
        

    function contents_update($mycon, $key, $value)
    {

	$sql = "REPLACE INTO `contents` SET `item_key` = :key, `item_value` = :value";
        
	$myrec =  $mycon->prepare($sql);
	$myrec->bindValue(":key",$key);
	$myrec->bindValue(":value",$value);

        if(!$myrec->execute())
        {
                return false;
        }
        
        return true;
        
    }

    function news_add($mycon, $newsdate, $headline, $caption, $content, $thedate)
    {

	$sql = "INSERT INTO `news` SET `newsdate` = :newsdate
			,`headline` = :headline
			,`caption` = :caption
			,`content` = :content
			,`thedate` = :thedate";
	$myrec =  $mycon->prepare($sql);
	$myrec->bindValue(":newsdate",$newsdate);
	$myrec->bindValue(":headline",$headline);
	$myrec->bindValue(":caption",$caption);
	$myrec->bindValue(":content",$content);
	$myrec->bindValue(":thedate",$thedate);
	$myrec->execute();

        if($myrec->rowCount() < 1)
        {
                return false;
        }
        
        return $mycon->lastInsertId();
        
    }

    function news_update($mycon, $news_id, $newsdate, $headline, $caption, $content)
    {

	$sql = "UPDATE `news` SET `newsdate` = :newsdate
			,`headline` = :headline
			,`caption` = :caption
			,`content` = :content WHERE `news_id` = :news_id";
	$myrec =  $mycon->prepare($sql);
	$myrec->bindValue(":newsdate",$newsdate);
	$myrec->bindValue(":headline",$headline);
	$myrec->bindValue(":caption",$caption);
	$myrec->bindValue(":content",$content);
	$myrec->bindValue(":news_id",$news_id);

        if(!$myrec->execute())
        {
                return false;
        }
        
        return true;
        
    }

    function news_delete($mycon, $news_id)
    {

	$sql = "DELETE FROM `news` WHERE `news_id` = :news_id";
        
	$myrec =  $mycon->prepare($sql);
	$myrec->bindValue(":news_id",$news_id);

        if(!$myrec->execute())
        {
                return false;
        }
        
        return true;
    }
    
        function mp3_sermons_delete($mycon, $sermon_id)
    {

	$sql = "DELETE FROM `sermons_mp3` WHERE `sermon_id` = :sermon_id";
        
	$myrec =  $mycon->prepare($sql);
	$myrec->bindValue(":sermon_id",$sermon_id);

        if(!$myrec->execute())
        {
                return false;
        }
        
        return true;
    }

    function events_add($mycon, $eventdate, $name, $caption, $content, $thedate)
    {

	$sql = "INSERT INTO `events` SET `name` = :name
			,`caption` = :caption
			,`content` = :content
			,`eventdate` = :eventdate
			,`thedate` = :thedate";
	$myrec =  $mycon->prepare($sql);
	$myrec->bindValue(":name",$name);
	$myrec->bindValue(":eventdate",$eventdate);
	$myrec->bindValue(":caption",$caption);
	$myrec->bindValue(":content",$content);
	$myrec->bindValue(":thedate",$thedate);
	$myrec->execute();

        if($myrec->rowCount() < 1)
        {
                return false;
        }
        
        return $mycon->lastInsertId();
        
    }
    
    function birthdays_add($mycon, $firstname, $lastname, $birthdaydate, $content, $thedate)
    {

	$sql = "INSERT INTO `birthdays` SET `firstname` = :firstname
			,`lastname` = :lastname
			,`birthdaydate` = :birthdaydate
			,`content` = :content
			,`thedate` = :thedate";
	$myrec =  $mycon->prepare($sql);
	$myrec->bindValue(":firstname",$firstname);
        $myrec->bindValue(":lastname",$lastname);
	$myrec->bindValue(":birthdaydate",$birthdaydate);
	$myrec->bindValue(":content",$content);
	$myrec->bindValue(":thedate",$thedate);
	$myrec->execute();

        if($myrec->rowCount() < 1)
        {
                return false;
        }
        
        return $mycon->lastInsertId();
        
    }
    
    
     function teams_add($mycon, $name, $leader, $content, $thedate)
    {

	$sql = "INSERT INTO `teams` SET `name` = :name
			,`leader` = :leader
			,`content` = :content
			,`thedate` = :thedate";
	$myrec =  $mycon->prepare($sql);
	$myrec->bindValue(":name",$name);
        $myrec->bindValue(":leader",$leader);
	$myrec->bindValue(":content",$content);
	$myrec->bindValue(":thedate",$thedate);
	$myrec->execute();

        if($myrec->rowCount() < 1)
        {
                return false;
        }
        
        return $mycon->lastInsertId();
        
    }


    function events_update($mycon, $event_id, $eventdate, $name, $caption, $content)
    {

	$sql = "UPDATE `events` SET `name` = :name
			,`eventdate` = :eventdate
			,`caption` = :caption
			,`content` = :content WHERE `event_id` = :event_id";
	$myrec =  $mycon->prepare($sql);
	$myrec->bindValue(":name",$name);
	$myrec->bindValue(":eventdate",$eventdate);
	$myrec->bindValue(":caption",$caption);
	$myrec->bindValue(":content",$content);
	$myrec->bindValue(":event_id",$event_id);

        if(!$myrec->execute())
        {
                return false;
        }
        
        return true;
        
    }
    
        function birthdays_update($mycon, $birthday_id, $firstname, $lastname, $birthdaydate, $content)
    {

	$sql = "UPDATE `birthdays` SET `firstname` = :firstname
			,`lastname` = :lastname
			,`birthdaydate` = :birthdaydate
			,`content` = :content WHERE `birthday_id` = :birthday_id";
	$myrec =  $mycon->prepare($sql);
	$myrec->bindValue(":firstname",$firstname);
	$myrec->bindValue(":lastname",$lastname);
	$myrec->bindValue(":birthdaydate",$birthdaydate);
	$myrec->bindValue(":content",$content);
	$myrec->bindValue(":birthday_id",$birthday_id);

        if(!$myrec->execute())
        {
                return false;
        }
        
        return true;
        
    }
    
    function teams_update($mycon, $team_id, $name, $leader, $composition)
    {

	$sql = "UPDATE `teams` SET `name` = :name
			,`leader` = :leader
			,`content` = :content WHERE `team_id` = :team_id";
	$myrec =  $mycon->prepare($sql);
	$myrec->bindValue(":name",$name);
	$myrec->bindValue(":leader",$leader);
	$myrec->bindValue(":content",$composition);
	$myrec->bindValue(":team_id",$team_id);

        if(!$myrec->execute())
        {
                return false;
        }
        
        return true;
        
    }



    function events_delete($mycon, $event_id)
    {

	$sql = "DELETE FROM `events` WHERE `event_id` = :event_id";
        
	$myrec =  $mycon->prepare($sql);
	$myrec->bindValue(":event_id",$event_id);

        if(!$myrec->execute())
        {
                return false;
        }
        
        return true;
    }
    
        function birthdays_delete($mycon, $birthday_id)
    {

	$sql = "DELETE FROM `birthdays` WHERE `birthday_id` = :birthday_id";
        
	$myrec =  $mycon->prepare($sql);
	$myrec->bindValue(":birthday_id",$birthday_id);

        if(!$myrec->execute())
        {
                return false;
        }
        
        return true;
    }

    
    function teams_delete($mycon, $team_id)
    {

	$sql = "DELETE FROM `teams` WHERE `team_id` = :team_id";
        
	$myrec =  $mycon->prepare($sql);
	$myrec->bindValue(":team_id",$team_id);

        if(!$myrec->execute())
        {
                return false;
        }
        
        return true;
    }



    function blogs_add($mycon, $headline, $caption, $content, $thedate)
    {

	$sql = "INSERT INTO `blogs` SET `headline` = :headline
			,`caption` = :caption
			,`content` = :content
			,`thedate` = :thedate";
	$myrec =  $mycon->prepare($sql);
	$myrec->bindValue(":headline",$headline);
	$myrec->bindValue(":caption",$caption);
	$myrec->bindValue(":content",$content);
	$myrec->bindValue(":thedate",$thedate);
	$myrec->execute();

        if($myrec->rowCount() < 1)
        {
                return false;
        }
        
        return $mycon->lastInsertId();
        
    }

    function blogs_update($mycon, $blog_id, $headline, $caption, $content)
    {

	$sql = "UPDATE `blogs` SET `headline` = :headline
			,`caption` = :caption
			,`content` = :content WHERE `blog_id` = :blog_id";
	$myrec =  $mycon->prepare($sql);
	$myrec->bindValue(":headline",$headline);
	$myrec->bindValue(":caption",$caption);
	$myrec->bindValue(":content",$content);
	$myrec->bindValue(":blog_id",$blog_id);

        if(!$myrec->execute())
        {
                return false;
        }
        
        return true;
        
    }

    function blogs_delete($mycon, $blog_id)
    {

	$sql = "DELETE FROM `blogs` WHERE `blog_id` = :blog_id";
        
	$myrec =  $mycon->prepare($sql);
	$myrec->bindValue(":blog_id",$blog_id);

        if(!$myrec->execute())
        {
                return false;
        }
        
        return true;
    }

    function sermons_add($mycon, $headline, $caption, $content, $embedcode, $thedate)
    {

	$sql = "INSERT INTO `sermons` SET `headline` = :headline
			,`caption` = :caption
			,`content` = :content
			,`embedcode` = :embedcode
			,`thedate` = :thedate";
	$myrec =  $mycon->prepare($sql);
	$myrec->bindValue(":headline",$headline);
	$myrec->bindValue(":caption",$caption);
	$myrec->bindValue(":content",$content);
	$myrec->bindValue(":embedcode",$embedcode);
	$myrec->bindValue(":thedate",$thedate);
	$myrec->execute();

        if($myrec->rowCount() < 1)
        {
                return false;
        }
        
        return $mycon->lastInsertId();
        
    }
    
    function mp3_sermons_add($mycon, $title, $minister, $thedate)
    {

	$sql = "INSERT INTO `sermons_mp3` SET `title` = :title
			,`minister` = :minister
                        ,`thedate` = :thedate";
	$myrec =  $mycon->prepare($sql);
	$myrec->bindValue(":title",$title);
	$myrec->bindValue(":minister",$minister);
	$myrec->bindValue(":thedate",$thedate);
	$myrec->execute();

        if($myrec->rowCount() < 1)
        {
                return false;
        }
        
        return $mycon->lastInsertId();
        
    }

    function sermons_update($mycon, $sermon_id, $headline, $caption, $content, $embedcode, $thedate)
    {

	$sql = "UPDATE `sermons` SET `headline` = :headline
			,`caption` = :caption
			,`embedcode` = :embedcode
			,`content` = :content
                        ,`thedate` = :thedate WHERE `sermon_id` = :sermon_id";
	$myrec =  $mycon->prepare($sql);
	$myrec->bindValue(":headline",$headline);
	$myrec->bindValue(":caption",$caption);
	$myrec->bindValue(":content",$content);
	$myrec->bindValue(":embedcode",$embedcode);
	$myrec->bindValue(":thedate",$thedate);
	$myrec->bindValue(":sermon_id",$sermon_id);

        if(!$myrec->execute())
        {
                return false;
        }
        
        return true;
        
    }
    
    function mp3_sermons_update($mycon, $sermon_id, $title, $minister, $thedate)
    {

	$sql = "UPDATE `sermons_mp3` SET `title` = :title
			,`minister` = :minister
                        ,`thedate` = :thedate WHERE `sermon_id` = :sermon_id";
	$myrec =  $mycon->prepare($sql);
	$myrec->bindValue(":title",$title);
	$myrec->bindValue(":minister",$minister);
	$myrec->bindValue(":thedate",$thedate);
	$myrec->bindValue(":sermon_id",$sermon_id);

        if(!$myrec->execute())
        {
                return false;
        }
        
        return true;
        
    }

    function sermons_delete($mycon, $sermon_id)
    {

	$sql = "DELETE FROM `sermons` WHERE `sermon_id` = :sermon_id";
        
	$myrec =  $mycon->prepare($sql);
	$myrec->bindValue(":sermon_id",$sermon_id);

        if(!$myrec->execute())
        {
                return false;
        }
        
        return true;
    }
        

    function departments_add($mycon, $name, $caption, $username, $password)
    {

	$sql = "INSERT INTO `departments` SET `name` = :name
			,`caption` = :caption
			,`username` = :username
			,`password` = :password";
	$myrec =  $mycon->prepare($sql);
	$myrec->bindValue(":name",$name);
	$myrec->bindValue(":caption",$caption);
	$myrec->bindValue(":username",$username);
	$myrec->bindValue(":password",$password);
	$myrec->execute();

        if($myrec->rowCount() < 1)
        {
                return false;
        }
        
        return $mycon->lastInsertId();
        
    }

    function departments_update($mycon, $department_id, $name, $caption, $password)
    {

	$sql = "UPDATE `departments` SET `name` = :name
			,`caption` = :caption
			,`password` = :password WHERE `department_id` = :department_id";
	$myrec =  $mycon->prepare($sql);
	$myrec->bindValue(":name",$name);
	$myrec->bindValue(":caption",$caption);
	$myrec->bindValue(":password",$password);
	$myrec->bindValue(":department_id",$department_id);

        if(!$myrec->execute())
        {
                return false;
        }
        
        return true;
        
    }

    function departments_content_update($mycon, $department_id, $content)
    {

	$sql = "UPDATE `departments` SET `content` = :content WHERE `department_id` = :department_id";
	$myrec =  $mycon->prepare($sql);
	$myrec->bindValue(":content",$content);
	$myrec->bindValue(":department_id",$department_id);

        if(!$myrec->execute())
        {
                return false;
        }
        
        return true;
        
    }


    function departments_delete($mycon, $department_id)
    {

	$sql = "DELETE FROM `departments` WHERE `department_id` = :department_id";
        
	$myrec =  $mycon->prepare($sql);
	$myrec->bindValue(":department_id",$department_id);

        if(!$myrec->execute())
        {
                return false;
        }
        
        return true;
    }


    function departments_gallery_add($mycon, $department_id, $name)
    {

	$sql = "INSERT INTO `departments_gallery` SET `name` = :name
			,`department_id` = :department_id";
        
	$myrec =  $mycon->prepare($sql);
	$myrec->bindValue(":name",$name);
	$myrec->bindValue(":department_id",$department_id);
	$myrec->execute();

        if($myrec->rowCount() < 1)
        {
                return false;
        }
        
        return $mycon->lastInsertId();
        
    }

    function departments_gallery_delete($mycon, $department_id, $gallery_id)
    {

	$sql = "DELETE FROM `departments_gallery` WHERE `department_id` = :department_id AND `gallery_id` = :gallery_id";
        
	$myrec =  $mycon->prepare($sql);
	$myrec->bindValue(":gallery_id",$gallery_id);
	$myrec->bindValue(":department_id",$department_id);

        if(!$myrec->execute())
        {
                return false;
        }
        
        return true;
    }
        
    
    function departments_articles_add($mycon, $department_id, $name, $caption, $content)
    {
	$sql = "INSERT INTO `departments_articles` SET `department_id` = :department_id
			,`name` = :name
			,`caption` = :caption
			,`content` = :content";
	$myrec =  $mycon->prepare($sql);
	$myrec->bindValue(":department_id",$department_id);
	$myrec->bindValue(":name",$name);
	$myrec->bindValue(":caption",$caption);
	$myrec->bindValue(":content",$content);
	$myrec->execute();

        if($myrec->rowCount() < 1)
        {
                return false;
        }
        
        return $mycon->lastInsertId();
        
    }

    function departments_articles_update($mycon, $department_id, $article_id, $name, $caption, $content)
    {

	$sql = "UPDATE `departments_articles` SET `name` = :name
			,`caption` = :caption
			,`content` = :content WHERE `department_id` = :department_id AND `article_id` = :article_id";
	$myrec =  $mycon->prepare($sql);
	$myrec->bindValue(":name",$name);
	$myrec->bindValue(":caption",$caption);
	$myrec->bindValue(":content",$content);
	$myrec->bindValue(":department_id",$department_id);
	$myrec->bindValue(":article_id",$article_id);

        if(!$myrec->execute())
        {
                return false;
        }
        
        return true;
        
    }

    function departments_articles_delete($mycon, $department_id, $article_id)
    {

	$sql = "DELETE FROM `articles` WHERE `department_id` = :department_id AND `article_id` = :article_id";
        
	$myrec =  $mycon->prepare($sql);
	$myrec->bindValue(":article_id",$article_id);
	$myrec->bindValue(":department_id",$department_id);

        if(!$myrec->execute())
        {
                return false;
        }
        
        return true;
    }



    function pastors_add($mycon, $name, $rank, $position)
    {

	$sql = "INSERT INTO `pastors` SET `name` = :name
			,`rank` = :rank
			,`position` = :position";
        
	$myrec =  $mycon->prepare($sql);
	$myrec->bindValue(":name",$name);
	$myrec->bindValue(":rank",$rank);
	$myrec->bindValue(":position",$position);
	$myrec->execute();

        if($myrec->rowCount() < 1)
        {
                return false;
        }
        
        return $mycon->lastInsertId();
        
    }

    function pastors_delete($mycon, $pastor_id)
    {

	$sql = "DELETE FROM `pastors` WHERE `pastor_id` = :pastor_id";
        
	$myrec =  $mycon->prepare($sql);
	$myrec->bindValue(":pastor_id",$pastor_id);

        if(!$myrec->execute())
        {
                return false;
        }
        
        return true;
    }
        
        
        

    
//////////////////////////////////////////////////////////    


    function admins_groups_add($mycon, $name, $rights, $description, $modifiedby)
    {

            $thedate = date("Y-m-d H:i:s");

            $sql = "INSERT INTO `admins_groups` SET `name` = :name
                    ,`rights` = :rights
                    ,`description` = :description
                    ,`modifiedby` = :modifiedby
                    ,`modifiedon` = :modifiedon";

            $myrec = $mycon->prepare($sql);
            $myrec->bindValue(":name",$name,PDO::PARAM_STR);
            $myrec->bindValue(":rights",$rights,PDO::PARAM_STR);
            $myrec->bindValue(":description",$description,PDO::PARAM_STR);
            $myrec->bindValue(":modifiedby",$modifiedby,PDO::PARAM_STR);
            $myrec->bindValue(":modifiedon",$thedate,PDO::PARAM_STR);

            if(!$myrec->execute()) return false;

            return $mycon->lastInsertId();

    }

    function admins_groups_update($mycon, $group_id, $name, $rights, $description, $modifiedby)
    {
            $thedate = date("Y-m-d H:i:s");

            $sql = "UPDATE `admins_groups` SET `name` = :name
                    ,`rights` = :rights
                    ,`description` = :description
                    ,`modifiedby` = :modifiedby
                    ,`modifiedon` = :modifiedon WHERE `group_id` = :group_id";

            $myrec = $mycon->prepare($sql);
            $myrec->bindValue(":group_id",$group_id,PDO::PARAM_STR);
            $myrec->bindValue(":name",$name,PDO::PARAM_STR);
            $myrec->bindValue(":rights",$rights,PDO::PARAM_STR);
            $myrec->bindValue(":description",$description,PDO::PARAM_STR);
            $myrec->bindValue(":modifiedby",$modifiedby,PDO::PARAM_STR);
            $myrec->bindValue(":modifiedon",$thedate,PDO::PARAM_STR);

            if(!$myrec->execute()) return false;
            return true;

    }

    function admins_groups_delete($mycon, $group_id)
    {

        $sql = "DELETE FROM `admins_groups` WHERE `group_id` = :group_id";
        $myrec =  $mycon->prepare($sql);
        $myrec->bindValue(":group_id",$group_id);

        if(!$myrec->execute())
        {
                return false;
        }

        return true;
    }

    

    function admins_add($mycon, $surname, $firstname, $othernames, $email, $phone, $group_id, $username, $password, $currentuserid)
    {
            $thedate = date("Y-m-d H:i:s");

            $sql = "INSERT INTO `admins` SET `surname` = :surname
                    ,`firstname` = :firstname
                    ,`othernames` = :othernames
                    ,`phone` = :phone
                    ,`email` = :email
                    ,`username` = :username
                    ,`password` = :password
                    ,`group_id` = :group_id
                    ,`token` = '1234'
                    ,`status` = '5'
                    ,`thedate` = :thedate
                    ,`createdby` = :createdby";

            $myrec = $mycon->prepare($sql);
            $myrec->bindValue(":surname",$surname,PDO::PARAM_STR);
            $myrec->bindValue(":firstname",$firstname,PDO::PARAM_STR);
            $myrec->bindValue(":othernames",$othernames,PDO::PARAM_STR);
            $myrec->bindValue(":phone",$phone,PDO::PARAM_STR);
            $myrec->bindValue(":email",$email,PDO::PARAM_STR);
            $myrec->bindValue(":username",$username,PDO::PARAM_STR);
            $myrec->bindValue(":password",$password,PDO::PARAM_STR);
            $myrec->bindValue(":group_id",$group_id,PDO::PARAM_STR);
            $myrec->bindValue(":thedate",$thedate,PDO::PARAM_STR);
            $myrec->bindValue(":createdby",$currentuserid,PDO::PARAM_STR);

            if(!$myrec->execute()) return false;

            return $mycon->lastInsertId();

    }


    function admins_update($mycon, $admin_id, $surname, $firstname, $othernames, $email, $phone, $group_id, $password, $status, $currentuserid)
    {
            $thedate = date("Y-m-d H:i:s");

            $sql = "UPDATE `admins` SET `surname` = :surname
                    ,`firstname` = :firstname
                    ,`othernames` = :othernames
                    ,`phone` = :phone
                    ,`email` = :email
                    ,`group_id` = :group_id
                    ,`password` = :password
                    ,`status` = :status
                    ,`modifiedby` = :modifiedby
                    ,`modifiedon` = :modifiedon WHERE `admin_id` = :admin_id";

            $myrec = $mycon->prepare($sql);
            $myrec->bindValue(":admin_id",$admin_id,PDO::PARAM_STR);
            $myrec->bindValue(":firstname",$firstname,PDO::PARAM_STR);
            $myrec->bindValue(":surname",$surname,PDO::PARAM_STR);
            $myrec->bindValue(":othernames",$othernames,PDO::PARAM_STR);
            $myrec->bindValue(":phone",$phone,PDO::PARAM_STR);
            $myrec->bindValue(":email",$email,PDO::PARAM_STR);
            $myrec->bindValue(":group_id",$group_id,PDO::PARAM_STR);
            $myrec->bindValue(":password",$password,PDO::PARAM_STR);
            $myrec->bindValue(":status",$status,PDO::PARAM_STR);
            $myrec->bindValue(":modifiedby",$currentuserid,PDO::PARAM_STR);
            $myrec->bindValue(":modifiedon",$thedate,PDO::PARAM_STR);

            if(!$myrec->execute()) return false;

            return true;

    }


    function admins_delete($mycon, $admin_id)
    {

        $sql = "DELETE FROM `admins` WHERE `admin_id` = :admin_id";
        $myrec =  $mycon->prepare($sql);
        $myrec->bindValue(":admin_id",$admin_id);

        if(!$myrec->execute())
        {
                return false;
        }

        return true;
    }


    function admins_logins($mycon, $admin_id, $ip, $device)
    {
            $thedate = date("Y-m-d H:i:s");

            $sql = "INSERT INTO `logins` SET `admin_id` = :admin_id
                    ,`ip` = :ip
                    ,`device` = :device
                    ,`thedate` = :thedate";

            $myrec = $mycon->prepare($sql);
            $myrec->bindValue(":admin_id",$admin_id,PDO::PARAM_STR);
            $myrec->bindValue(":ip",$ip,PDO::PARAM_STR);
            $myrec->bindValue(":device",$device,PDO::PARAM_STR);
            $myrec->bindValue(":thedate",$thedate,PDO::PARAM_STR);

            if(!$myrec->execute()) return false;

            return true;


    }

    function admin_activities($mycon, $admin_id, $action)
    {
        return true;
        
            $thedate = date("Y-m-d H:i:s");

            $sql = "INSERT INTO `activitylogs` SET `admin_id` = :admin_id
                    ,`action` = :action
                    ,`thedate` = :thedate";

            $myrec = $mycon->prepare($sql);
            $myrec->bindValue(":admin_id",$admin_id,PDO::PARAM_STR);
            $myrec->bindValue(":action",$action,PDO::PARAM_STR);
            $myrec->bindValue(":thedate",$thedate,PDO::PARAM_STR);

            if(!$myrec->execute()) return false;

            return true;


    }

    

	function updates($mycon, $item_type, $item_id, $content)
	{
		$thedate = date("Y-m-d H:i:s");
		
		$sql = "INSERT INTO `updates` SET `item_type` = :item_type
			,`item_id` = :item_id
			,`content` = :content
			,`thedate` = :thedate";
		
		$myrec = $mycon->prepare($sql);
		$myrec->bindValue(":item_type",$item_type,PDO::PARAM_STR);
		$myrec->bindValue(":item_id",$item_id,PDO::PARAM_STR);
		$myrec->bindValue(":content",$content,PDO::PARAM_STR);
		$myrec->bindValue(":thedate",$thedate,PDO::PARAM_STR);
		
		if(!$myrec->execute()) return false;
		
		return true;
			
					
	}
    

    function profile_update($mycon, $admin_id, $surname, $firstname, $othernames, $phone)
    {
            $thedate = date("Y-m-d H:i:s");

            $sql = "UPDATE `admins` SET `surname` = :surname
                    ,`firstname` = :firstname
                    ,`othernames` = :othernames
                    ,`phone` = :phone
                    ,`modifiedby` = :modifiedby
                    ,`modifiedon` = :modifiedon WHERE `admin_id` = :admin_id";

            $myrec = $mycon->prepare($sql);
            $myrec->bindValue(":admin_id",$admin_id,PDO::PARAM_STR);
            $myrec->bindValue(":firstname",$firstname,PDO::PARAM_STR);
            $myrec->bindValue(":surname",$surname,PDO::PARAM_STR);
            $myrec->bindValue(":othernames",$othernames,PDO::PARAM_STR);
            $myrec->bindValue(":phone",$phone,PDO::PARAM_STR);
            $myrec->bindValue(":modifiedby",$admin_id,PDO::PARAM_STR);
            $myrec->bindValue(":modifiedon",$thedate,PDO::PARAM_STR);

            if(!$myrec->execute()) return false;

            return true;

    }


    function profile_update_password($mycon, $admin_id, $password)
    {

	$sql = "UPDATE `admins` SET `password` = :password WHERE `admin_id` = :admin_id";
        
	$myrec =  $mycon->prepare($sql);
	$myrec->bindValue(":admin_id",$admin_id);
	$myrec->bindValue(":password",$password);

        if(!$myrec->execute())
        {
                return false;
        }
        
        return true;
    }
            
        

}///// end class

class DataRead
{
        
    function admins_login($mycon,$username,$password)
    {
        $myrec = $mycon->prepare("SELECT * FROM `admins` WHERE `username` = :username AND `password`=:password LIMIT 1");
        $myrec->bindValue(":username",$username);
        $myrec->bindValue(":password",$password);
        $myrec->execute();
        
        if($myrec->rowCount() < 1) 
        {
            return false;
        }
        
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }
 

    function banners_list($mycon, $filter, $param)
    {
		$sql = "SELECT * FROM `banners` WHERE `banner_id` > -1 ";
		if(strlen(trim($sql)) > 3 && is_array($param))
		{
			$sql .= $filter;
			$myrec = $mycon->prepare($sql);
			$myrec->execute($param);
		}
		else
		{
			$sql .= " ORDER BY `banner_id` DESC";
			$myrec = $mycon->query($sql);
		}

        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }

    function gallery_list($mycon, $filter, $param)
    {
		$sql = "SELECT * FROM `gallery` WHERE `gallery_id` > -1 ";
		if(strlen(trim($sql)) > 3 && is_array($param))
		{
			$sql .= $filter;
			$myrec = $mycon->prepare($sql);
			$myrec->execute($param);
		}
		else
		{
			$sql .= " ORDER BY `gallery_id` DESC";
			$myrec = $mycon->query($sql);
		}

        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }

    function gallery_categories($mycon)
    {
        $sql = "SELECT DISTINCT(`category`) FROM `gallery` ORDER BY `category`";
        $myrec = $mycon->query($sql);

        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }
    
    
    
    
    function contents_get($mycon)
    {
        $myrec = $mycon->query("SELECT * FROM `contents`");
        $contents = array();

        //loop through settings
        foreach($myrec->fetchAll(PDO::FETCH_ASSOC) as $row)
        {
            $key = $row['item_key'];
            $value = $row['item_value'];
            $contents[$key] = $value;
        }

        return $contents;
    }

    function news_list($mycon, $filter, $param)
    {
		$sql = "SELECT * FROM `news` WHERE `news_id` > -1 ";
		if(strlen(trim($sql)) > 3 && is_array($param))
		{
			$sql .= $filter;
			$myrec = $mycon->prepare($sql);
			$myrec->execute($param);
		}
		else
		{
			$sql .= " ORDER BY `news_id` DESC";
			$myrec = $mycon->query($sql);
		}

        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }
    
    function news_get($mycon, $news_id)
    {
        $sql = "SELECT * FROM `news` WHERE `news_id` = :news_id ";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":news_id",$news_id);
        $myrec->execute();
        
        if($myrec->rowCount() < 1) return false;
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }

    function events_list($mycon, $filter, $param)
    {
		$sql = "SELECT * FROM `events` WHERE `event_id` > -1 ";
		if(strlen(trim($sql)) > 3 && is_array($param))
		{
			$sql .= $filter;
			$myrec = $mycon->prepare($sql);
			$myrec->execute($param);
		}
		else
		{
			$sql .= " ORDER BY `eventdate` DESC";
			$myrec = $mycon->query($sql);
		}

        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }
    
        function birthdays_list($mycon, $filter, $param)
    {
		$sql = "SELECT * FROM `birthdays` WHERE `birthday_id` > -1 ";
		if(strlen(trim($sql)) > 3 && is_array($param))
		{
			$sql .= $filter;
			$myrec = $mycon->prepare($sql);
			$myrec->execute($param);
		}
		else
		{
			$sql .= " ORDER BY `birthdaydate` DESC";
			$myrec = $mycon->query($sql);
		}

        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }
    
        function teams_list($mycon, $filter, $param)
    {
		$sql = "SELECT * FROM `teams` WHERE `team_id` > -1 ";
		if(strlen(trim($sql)) > 3 && is_array($param))
		{
			$sql .= $filter;
			$myrec = $mycon->prepare($sql);
			$myrec->execute($param);
		}
		else
		{
			$sql .= " ORDER BY `thedate` DESC";
			$myrec = $mycon->query($sql);
		}

        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }

    
    function events_get($mycon, $event_id)
    {
        $sql = "SELECT * FROM `events` WHERE `event_id` = :event_id ";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":event_id",$event_id);
        $myrec->execute();
        
        if($myrec->rowCount() < 1) return false;
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }
    
        function birthdays_get($mycon, $birthday_id)
    {
        $sql = "SELECT * FROM `birthdays` WHERE `birthday_id` = :birthday_id ";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":birthday_id",$birthday_id);
        $myrec->execute();
        
        if($myrec->rowCount() < 1) return false;
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }

    function teams_get($mycon, $team_id)
    {
        $sql = "SELECT * FROM `teams` WHERE `team_id` = :team_id ";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":team_id",$team_id);
        $myrec->execute();
        
        if($myrec->rowCount() < 1) return false;
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }


    function blogs_list($mycon, $filter, $param)
    {
		$sql = "SELECT * FROM `blogs` WHERE `blog_id` > -1 ";
		if(strlen(trim($sql)) > 3 && is_array($param))
		{
			$sql .= $filter;
			$myrec = $mycon->prepare($sql);
			$myrec->execute($param);
		}
		else
		{
			$sql .= " ORDER BY `blog_id` DESC";
			$myrec = $mycon->query($sql);
		}

        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }
    
    function blogs_get($mycon, $blog_id)
    {
        $sql = "SELECT * FROM `blogs` WHERE `blog_id` = :blog_id ";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":blog_id",$blog_id);
        $myrec->execute();
        
        if($myrec->rowCount() < 1) return false;
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }

    function sermons_list($mycon, $filter, $param)
    {
		$sql = "SELECT * FROM `sermons` WHERE `sermon_id` > -1 ";
		if(strlen(trim($sql)) > 3 && is_array($param))
		{
			$sql .= $filter;
			$myrec = $mycon->prepare($sql);
			$myrec->execute($param);
		}
		else
		{
			$sql .= " ORDER BY `sermon_id` DESC";
			$myrec = $mycon->query($sql);
		}

        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }
    
    function sermons_mp3_list($mycon, $filter, $param)
    {
		$sql = "SELECT * FROM `sermons_mp3` WHERE `sermon_id` > -1 ";
		if(strlen(trim($sql)) > 3 && is_array($param))
		{
			$sql .= $filter;
			$myrec = $mycon->prepare($sql);
			$myrec->execute($param);
		}
		else
		{
			$sql .= " ORDER BY `sermon_id` DESC";
			$myrec = $mycon->query($sql);
		}

        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }
    
    function sermons_get($mycon, $sermon_id)
    {
        $sql = "SELECT * FROM `sermons` WHERE `sermon_id` = :sermon_id ";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":sermon_id",$sermon_id);
        $myrec->execute();
        
        if($myrec->rowCount() < 1) return false;
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }
    
     function mp3_sermons_get($mycon, $sermon_id)
    {
        $sql = "SELECT * FROM `sermons_mp3` WHERE `sermon_id` = :sermon_id ";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":sermon_id",$sermon_id);
        $myrec->execute();
        
        if($myrec->rowCount() < 1) return false;
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }

    function departments_list($mycon, $filter, $param)
    {
		$sql = "SELECT * FROM `departments` WHERE `department_id` > -1 ";
		if(strlen(trim($sql)) > 3 && is_array($param))
		{
			$sql .= $filter;
			$myrec = $mycon->prepare($sql);
			$myrec->execute($param);
		}
		else
		{
			$sql .= " ORDER BY `department_id` DESC";
			$myrec = $mycon->query($sql);
		}

        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }
    
    function departments_get($mycon, $department_id)
    {
        $sql = "SELECT * FROM `departments` WHERE `department_id` = :department_id ";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":department_id",$department_id);
        $myrec->execute();
        
        if($myrec->rowCount() < 1) return false;
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }
    function departments_getbyusername($mycon, $username)
    {
        $sql = "SELECT * FROM `departments` WHERE `username` = :username ";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":username",$username);
        $myrec->execute();
        
        if($myrec->rowCount() < 1) return false;
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }
    
    function departments_articles_list($mycon, $department_id, $filter, $param)
    {
		$sql = "SELECT * FROM `departments_articles` WHERE `department_id` = :department_id ";
		if(strlen(trim($sql)) > 3 && is_array($param))
		{
                    $param[':department_id'] = $department_id;
			$sql .= $filter;
			$myrec = $mycon->prepare($sql);
			$myrec->execute($param);
		}
		else
		{
			$sql .= " ORDER BY `article_id` DESC";
			$myrec = $mycon->prepare($sql);
                        $myrec->bindValue(":department_id",$department_id);
			$myrec->execute();
		}

        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }
    
    function departments_articles_get($mycon, $department_id, $article_id)
    {
        $sql = "SELECT * FROM `departments_articles` WHERE `department_id` = :department_id AND `article_id` = :article_id ";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":department_id",$department_id);
        $myrec->bindValue(":article_id",$article_id);
        $myrec->execute();
        
        if($myrec->rowCount() < 1) return false;
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }


    function departments_gallery_list($mycon, $department_id, $filter, $param)
    {
		$sql = "SELECT * FROM `departments_gallery` WHERE `department_id` = :department_id ";
		if(strlen(trim($sql)) > 3 && is_array($param))
		{
                    $param[':department_id'] = $department_id;
			$sql .= $filter;
			$myrec = $mycon->prepare($sql);
			$myrec->execute($param);
		}
		else
		{
			$sql .= " ORDER BY `gallery_id` DESC";
			$myrec = $mycon->prepare($sql);
                        $myrec->bindValue(":department_id",$department_id);
			$myrec->execute();
		}

        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }



    function pastors_list($mycon, $filter, $param)
    {
		$sql = "SELECT * FROM `pastors` WHERE `pastor_id` > -1 ";
		if(strlen(trim($sql)) > 3 && is_array($param))
		{
			$sql .= $filter;
			$myrec = $mycon->prepare($sql);
			$myrec->execute($param);
		}
		else
		{
			$sql .= " ORDER BY `position` ASC";
			$myrec = $mycon->query($sql);
		}

        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }
    


    function admins_list($mycon)
    {
	$sql = "SELECT a.*, g.name AS groupname FROM `admins` a LEFT JOIN `admins_groups` g ON g.group_id = a.group_id ORDER BY `admin_id` ASC";
        $myrec = $mycon->query($sql);
                
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }

    function admins_listbygroup($mycon, $group_id)
    {
	$sql = "SELECT a.*, g.name AS groupname FROM `admins` a LEFT JOIN `admins_groups` g ON g.group_id = a.group_id WHERE a.`group_id` = :group_id ORDER BY `admin_id` ASC";
        $myrec = $mycon->prepare($sql);
        $myrec->bindValue(":group_id",$group_id);
        $myrec->execute();
                
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }
    
    function admins_get($mycon, $admin_id)
    {
        $myrec = $mycon->prepare("SELECT a.*, g.name AS groupname FROM `admins` a LEFT JOIN `admins_groups` g ON g.group_id = a.group_id WHERE a.`admin_id` = :admin_id LIMIT 1");
        $myrec->bindValue(":admin_id",$admin_id);
        $myrec->execute();
        
        if($myrec->rowCount() < 1) return false;
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }
    
    function admins_getbyusername($mycon, $username)
    {
        $myrec = $mycon->prepare("SELECT a.*, g.name AS groupname FROM `admins` a LEFT JOIN `admins_groups` g ON g.group_id = a.group_id WHERE a.`username` = :username LIMIT 1");
        $myrec->bindValue(":username",$username);
        $myrec->execute();
        
        if($myrec->rowCount() < 1) return false;
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }

    function admins_groups_list($mycon)
    {
	$sql = "SELECT g.*, (SELECT COUNT(admin_id) FROM `admins` WHERE `group_id` = g.group_id) AS usercount FROM `admins_groups` g ORDER BY `name` ASC";
        $myrec = $mycon->query($sql);
                
        return $myrec->fetchAll(PDO::FETCH_ASSOC);
    }
    
    function admins_groups_get($mycon, $group_id)
    {
        $myrec = $mycon->prepare("SELECT g.*, (SELECT COUNT(admin_id) FROM `admins` WHERE `group_id` = g.group_id) AS usercount FROM `admins_groups` g WHERE `group_id` = :group_id LIMIT 1");
        $myrec->bindValue(":group_id",$group_id);
        $myrec->execute();
        
        if($myrec->rowCount() < 1) return false;
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }
    
    function admins_groups_getbyname($mycon, $name)
    {
        $myrec = $mycon->prepare("SELECT g.*, (SELECT COUNT(admin_id) FROM `admins` WHERE `group_id` = g.group_id) AS usercount FROM `admins_groups` g WHERE `name` = :name LIMIT 1");
        $myrec->bindValue(":name",$name);
        $myrec->execute();
        
        if($myrec->rowCount() < 1) return false;
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }
    
    function admins_groups_getbyuserid($mycon, $user_id)
    {
        $myrec = $mycon->prepare("SELECT g.*, u.username FROM `admins` u LEFT JOIN `admins_groups` g ON g.group_id = u.group_id WHERE u.admin_id = :user_id LIMIT 1");
        $myrec->bindValue(":user_id",$user_id);
        $myrec->execute();
        
        if($myrec->rowCount() < 1) return false;
        
        return $myrec->fetch(PDO::FETCH_ASSOC);
    }
    
    
    
    
}///// end class
?>