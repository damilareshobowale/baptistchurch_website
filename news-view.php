<?php
require_once("config.php");
require_once("inc_dbfunctions.php");
$mycon = databaseConnect();

if(!isset($_GET['code']) || $_GET['code'] == "")
{
    openPage("news.php");
    exit;
}

$dataRead = New DataRead();

$contentsdetails = $dataRead->news_get($mycon,$_GET['code']);
if(!$contentsdetails)
{
    showAlert("The requested content was not found");
    openPage("news.php");
    exit;
}

$contentslist = $dataRead->news_list($mycon, " LIMIT 7", Array());

?>
<!DOCTYPE html>
<html lang="en">
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="<?php seoPageContent() ?>" />
	<meta name="description" content="<?php seoPageDescriptions() ?>">
	<title><?php pageTitle() ?></title>

	<!-- Bootstrap core CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<!-- Full Calender CSS -->
	<link href="css/fullcalendar.css" rel="stylesheet">
	<!-- Owl Carousel CSS -->
	<link href="css/owl.carousel.css" rel="stylesheet">
	<!-- Pretty Photo CSS -->
	<link href="css/prettyPhoto.css" rel="stylesheet">
	<!-- Bx-Slider StyleSheet CSS -->
	<link href="css/jquery.bxslider.css" rel="stylesheet"> 
	<!-- Font Awesome StyleSheet CSS -->
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="svg/style.css" rel="stylesheet">
	<!-- Widget CSS -->
	<link href="css/widget.css" rel="stylesheet">
	<!-- Typography CSS -->
	<link href="css/typography.css" rel="stylesheet">
	<!-- Shortcodes CSS -->
	<link href="css/shortcodes.css" rel="stylesheet">
	<!-- Custom Main StyleSheet CSS -->
	<link href="style.css" rel="stylesheet">
	<!-- Color CSS -->
	<link href="css/color.css" rel="stylesheet">
	<!-- Responsive CSS -->
	<link href="css/responsive.css" rel="stylesheet">
	<!-- SELECT MENU -->
	<link href="css/selectric.css" rel="stylesheet">
	<!-- SIDE MENU -->
	<link rel="stylesheet" href="css/jquery.sidr.dark.css">

</head>

<body>
	<!--KF KODE WRAPPER WRAP START-->
    <div class="kode_wrapper">
    	<!--HEADER START-->
            <?php require_once("inc_header.php"); ?>
	<!--HEADER END-->

        <!--Banner Wrap Start-->
        <div class="kf_inr_banner">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                    	<!--KF INR BANNER DES Wrap Start-->
                        <div class="kf_inr_ban_des">
                        	<div class="inr_banner_heading">
								<h3><?php echo $contentsdetails['headline'] ?></h3>
                        	</div>
                           
                            <div class="kf_inr_breadcrumb">
								<ul>
									<li><a href="index.php">Home</a></li>
									<li><a href="#">News</a></li>
								</ul>
							</div>
                        </div>
                        <!--KF INR BANNER DES Wrap End-->
                    </div>
                </div>
            </div>
        </div>

        <!--Banner Wrap End-->

    	<!--Content Wrap Start-->
    	<div class="kf_content_wrap">
    		<section>
    			<div class="container">
    				<div class="row">
    					<div class="col-md-8">

    						<!--KF_BLOG DETAIL_WRAP START-->
    						<div class="kf_blog_detail_wrap">

    							<!-- BLOG DETAIL THUMBNAIL START-->
    							<div class="blog_detail_thumbnail">
	    							<figure>
	    								<img src="pictures/news/<?php echo $contentsdetails['news_id'] ?>.jpg" alt=""/>
	    							</figure>
	    						</div>
	    						<!-- BLOG DETAIL THUMBNAIL END-->

    							<!--KF_BLOG DETAIL_DES START-->
    							<div class="kf_blog_detail_des">
	    							<div class="blog-detl_heading">
	    								<h5><?php echo $contentsdetails['headline'] ?></h5>
	    							</div>

    								<ul class="blog_detail_meta">
    									<li><i class="fa fa-calendar"></i><?php echo formatDate($contentsdetails['thedate'],"no") ?></li>
    								</ul>

                                                                <p><i><?php echo $contentsdetails['caption'] ?></i></p>
	    							<p class="margin-bottom"><?php echo $contentsdetails['content'] ?></p>
	    						</div>
	    						<!--KF_BLOG DETAIL_DES END-->

	    						<!--SECTION COMMENT START-->
	    						<div class="section-comment">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.6";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script> 
<div class="fb-comments" data-href="<?php echo CurrentPageURL() ?>" data-width="100%" data-numposts="10"></div>                                    

                                                        </div>
	    						<!--SECTION COMMENT END-->
    						</div>
    						<!--KF_BLOG DETAIL_WRAP END-->
    					</div>

    					<!--KF_EDU_SIDEBAR_WRAP START-->
    					<div class="col-md-4">
    						<div class="kf-sidebar">
    							<!--KF SIDEBAR RECENT POST WRAP START-->
    							<div class="widget widget-recent-posts">
    								<h2>Recent News</h2>
    								<ul class="sidebar_rpost_des">
                                    	<!--LIST ITEM START-->
                                <?php
                                    foreach($contentslist as $row)
                                    {
                                ?>
                                        
                                   		<li>
                                            <figure style="height: 85px">
                                            	<img src="pictures/news/<?php echo $row['news_id'] ?>.jpg" alt="">
                                                <figcaption><a href="news-view.php?code=<?php echo $row['news_id'] ?>"><i class="fa fa-search-plus"></i></a></figcaption>
                                            </figure>
                                            <div class="kode-text">
                                                <h6><a href="news-view.php?code=<?php echo $row['news_id'] ?>"><?php echo $row['headline'] ?></a></h6>
                                                <span><i class="fa fa-clock-o"></i><?php echo formatDate($row['thedate'],"no") ?></span>
                                            </div>
    									</li>
                                <?php
                                    }
                                ?>
                                        <!--LIST ITEM START-->
										<!--LIST ITEM START-->
    								</ul>
    							</div>
    							<!--KF SIDEBAR RECENT POST WRAP END-->

    						</div>
    					</div>
						<!--KF EDU SIDEBAR WRAP END-->

    				</div>
    			</div>
    		</section>
    				
    		<!--ABOUT UNIVERSITY START-->
    		<section>
    			<div class="container">
    				<div class="row">

                                </div>
    			</div>
    		</section>
    		<!--ABOUT UNIVERSITY END-->
    	</div>
        <!--Content Wrap End-->
        
        <?php require_once("inc_footer.php"); ?>
                
    </div>
    <!--KF KODE WRAPPER WRAP END-->

    

	<!--Bootstrap core JavaScript-->
	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<!--Bx-Slider JavaScript-->
	<script src="js/jquery.bxslider.min.js"></script>
	<!--Owl Carousel JavaScript-->
	<script src="js/owl.carousel.min.js"></script>
	<!--Pretty Photo JavaScript-->
	<script src="js/jquery.prettyPhoto.js"></script>
	<!--Full Calender JavaScript-->
	<script src="js/moment.min.js"></script>
	<script src="js/fullcalendar.min.js"></script>
	<script src="js/jquery.downCount.js"></script>
	<!--Image Filterable JavaScript-->
	<script src="js/jquery-filterable.js"></script>
	<!--Accordian JavaScript-->
	<script src="js/jquery.accordion.js"></script>
	<!--Number Count (Waypoints) JavaScript-->
	<script src="js/waypoints-min.js"></script>
	<!--v ticker-->
	<script src="js/jquery.vticker.min.js"></script>
	<!--select menu-->
	<script src="js/jquery.selectric.min.js"></script>
	<!--Side Menu-->
	<script src="js/jquery.sidr.min.js"></script>
	<!--Custom JavaScript-->
	<script src="js/custom.js"></script>

    
</body>
</html>
