<?php
require_once("config.php");
require_once("inc_dbfunctions.php");
$mycon = databaseConnect();

$dataRead = New DataRead();

$contentsdetails = $dataRead->contents_get($mycon);
$pastorslist = $dataRead->pastors_list($mycon, "", "");

?>
<!DOCTYPE html>
<html lang="en">
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="<?php seoPageContent() ?>" />
	<meta name="description" content="<?php seoPageDescriptions() ?>">
	<title><?php pageTitle() ?></title>

	<!-- Bootstrap core CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<!-- Full Calender CSS -->
	<link href="css/fullcalendar.css" rel="stylesheet">
	<!-- Owl Carousel CSS -->
	<link href="css/owl.carousel.css" rel="stylesheet">
	<!-- Pretty Photo CSS -->
	<link href="css/prettyPhoto.css" rel="stylesheet">
	<!-- Bx-Slider StyleSheet CSS -->
	<link href="css/jquery.bxslider.css" rel="stylesheet"> 
	<!-- Font Awesome StyleSheet CSS -->
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="svg/style.css" rel="stylesheet">
	<!-- Widget CSS -->
	<link href="css/widget.css" rel="stylesheet">
	<!-- Typography CSS -->
	<link href="css/typography.css" rel="stylesheet">
	<!-- Shortcodes CSS -->
	<link href="css/shortcodes.css" rel="stylesheet">
	<!-- Custom Main StyleSheet CSS -->
	<link href="style.css" rel="stylesheet">
	<!-- Color CSS -->
	<link href="css/color.css" rel="stylesheet">
	<!-- Responsive CSS -->
	<link href="css/responsive.css" rel="stylesheet">
	<!-- SELECT MENU -->
	<link href="css/selectric.css" rel="stylesheet">
	<!-- SIDE MENU -->
	<link rel="stylesheet" href="css/jquery.sidr.dark.css">

</head>

<body>
	<!--KF KODE WRAPPER WRAP START-->
    <div class="kode_wrapper">
    	<!--HEADER START-->
            <?php require_once("inc_header.php"); ?>
	<!--HEADER END-->

        <!--Banner Wrap Start-->
        <div class="kf_inr_banner">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                    	<!--KF INR BANNER DES Wrap Start-->
                        <div class="kf_inr_ban_des">
                        	<div class="inr_banner_heading">
								<h3>about us</h3>
                        	</div>
                           
                            <div class="kf_inr_breadcrumb">
								<ul>
									<li><a href="index.php">Home</a></li>
									<li><a href="#">about us</a></li>
								</ul>
							</div>
                        </div>
                        <!--KF INR BANNER DES Wrap End-->
                    </div>
                </div>
            </div>
        </div>

        <!--Banner Wrap End-->

    	<!--Content Wrap Start-->
    	<div class="kf_content_wrap">
    				
    		<!--ABOUT UNIVERSITY START-->
    		<section>
    			<div class="container">
    				<div class="row">
    					<div class="col-md-6">
    						<div class="abt_univ_wrap">
								<!-- HEADING 1 START-->
								<div class="kf_edu2_heading1">
									<h3><?php echo $contentsdetails['aboutpage_title'] ?></h3>
								</div>
								<!-- HEADING 1 END-->

								<div class="abt_univ_des">

									<?php echo $contentsdetails['aboutpage_content'] ?>

								</div>
    						</div>
    					</div>

    					<div class="col-md-6">
    						<div class="abt_univ_thumb">
    							<figure>
    								<img src="pictures/aboutpage_image.jpg" alt=""/>
    							</figure>
    						</div>
    					</div>

    				</div>
    			</div>
    		</section>
    		<!--ABOUT UNIVERSITY END-->

			<!--KF INTRO WRAP START-->
			<section class="abut-padiing" style="background-color: rgb(247, 247, 247)">
				<div class="kf_intro_des_wrap aboutus_page">
					<div class="container">
						<div class="row">
							<!-- HEADING 2 START-->
							<div class="col-md-12">
								<div class="kf_edu2_heading2">
									<h3><?php echo $contentsdetails['aboutpage_missionvision_title'] ?></h3>
								</div>
							</div>
							<!-- HEADING 2 END-->

							<div class="col-md-4 col-sm-6">
								<!-- INTERO DES START-->
								<div class="kf_intro_des">
									<div class="kf_intro_des_caption">
										<span><i class=" icon-book200 "></i></span>
                                                                                <h6><?php echo $contentsdetails['aboutpage_box1_title'] ?></h6>
                                                                                <p><?php echo $contentsdetails['aboutpage_box1_caption'] ?></p>
									</div>
								</div>
								<!-- INTERO DES END-->
							</div>

							<div class="col-md-4 col-sm-6">
								<!-- INTERO DES START-->
								<div class="kf_intro_des">
									<div class="kf_intro_des_caption">
										<span><i class=" icon-book200"></i></span>
                                                                                <h6><?php echo $contentsdetails['aboutpage_box2_title'] ?></h6>
                                                                                <p><?php echo $contentsdetails['aboutpage_box2_caption'] ?></p>
									</div>
								</div>
								<!-- INTERO DES END-->
							</div>

							<div class="col-md-4 col-sm-6">
								<!-- INTERO DES START-->
								<div class="kf_intro_des">
									<div class="kf_intro_des_caption">
										<span><i class="icon-book200"></i></span>
                                                                                <h6><?php echo $contentsdetails['aboutpage_box3_title'] ?></h6>
                                                                                <p><?php echo $contentsdetails['aboutpage_box3_caption'] ?></p>
									</div>
								</div>
								<!-- INTERO DES END-->
							</div>

						</div>
					</div>
				</div>
			</section>
			<!--KF INTRO WRAP END-->

			<!-- FACULTY WRAP START-->
			<section>
				<div class="container">
					<div class="row">
						<!-- HEADING 1 START-->
						<div class="col-md-12">
							<div class="kf_edu2_heading1">
								<h3>Pastors & workers</h3>
							</div>
						</div>
						<!-- HEADING 1 END-->

						<!-- FACULTY SLIDER WRAP START-->
						<div class="edu2_faculty_wrap">
							<div id="owl-demo-8" class="owl-demo-slider owl-carousel owl-theme">
                                                    <?php
                                                        foreach($pastorslist as $row)
                                                        {
                                                    ?>
								<div class="item">
									<!-- FACULTY DES START-->
									<div class="edu2_faculty_des">
										<figure style="height: 200px; overflow: hidden;"><img src="pictures/pastors/<?php echo $row['pastor_id'] ?>.jpg" alt=""/>
										</figure>
										<div class="edu2_faculty_des2">
											<h6><a href="#"><?php echo $row['name'] ?></a></h6>
											<strong><?php echo $row['rank'] ?></strong>
											<p></p>
										</div>
									</div>
									<!-- FACULTY DES END-->
								</div>
                                                    <?php
                                                        }
                                                    ?>

							</div>
						</div>
						<!-- FACULTY SLIDER WRAP END-->
					</div>
				</div>
			</section>
			<!-- FACULTY WRAP START-->
    	</div>
        <!--Content Wrap End-->
        
        <?php require_once("inc_footer.php"); ?>
                
    </div>
    <!--KF KODE WRAPPER WRAP END-->

    

	<!--Bootstrap core JavaScript-->
	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<!--Bx-Slider JavaScript-->
	<script src="js/jquery.bxslider.min.js"></script>
	<!--Owl Carousel JavaScript-->
	<script src="js/owl.carousel.min.js"></script>
	<!--Pretty Photo JavaScript-->
	<script src="js/jquery.prettyPhoto.js"></script>
	<!--Full Calender JavaScript-->
	<script src="js/moment.min.js"></script>
	<script src="js/fullcalendar.min.js"></script>
	<script src="js/jquery.downCount.js"></script>
	<!--Image Filterable JavaScript-->
	<script src="js/jquery-filterable.js"></script>
	<!--Accordian JavaScript-->
	<script src="js/jquery.accordion.js"></script>
	<!--Number Count (Waypoints) JavaScript-->
	<script src="js/waypoints-min.js"></script>
	<!--v ticker-->
	<script src="js/jquery.vticker.min.js"></script>
	<!--select menu-->
	<script src="js/jquery.selectric.min.js"></script>
	<!--Side Menu-->
	<script src="js/jquery.sidr.min.js"></script>
	<!--Custom JavaScript-->
	<script src="js/custom.js"></script>

    
</body>
</html>
