<?php
require_once("config.php");
require_once("inc_dbfunctions.php");
$mycon = databaseConnect();

$dataRead = New DataRead();
$bannerslist = $dataRead->banners_list($mycon);

$contentsdetails = $dataRead->contents_get($mycon);
$newslist = $dataRead->news_list($mycon, " LIMIT 4", Array());
$sermonslist = $dataRead->sermons_list($mycon, " LIMIT 3", Array());

$month = date("m");

$birthdaylist = $dataRead->birthdays_list($mycon, " LIMIT 8", Array());

$teamlist = $dataRead->teams_list($mycon, "", "");

$mp3sermonslist = $dataRead->mp3_sermons_list($mycon, " LIMIT 3", Array());

?>
<!DOCTYPE html>
<html lang="en">
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="<?php seoPageContent() ?>" />
	<meta name="description" content="<?php seoPageDescriptions() ?>">
	<title><?php pageTitle() ?></title>
	<!-- Bootstrap core CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<!-- Full Calender CSS -->
	<link href="css/fullcalendar.css" rel="stylesheet">
	<!-- Owl Carousel CSS -->
	<link href="css/owl.carousel.css" rel="stylesheet">
	<!-- Pretty Photo CSS -->
	<link href="css/prettyPhoto.css" rel="stylesheet">
	<!-- Bx-Slider StyleSheet CSS -->
	<link href="css/jquery.bxslider.css" rel="stylesheet"> 
	<!-- Font Awesome StyleSheet CSS -->
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="svg/style.css" rel="stylesheet">
	<!-- Widget CSS -->
	<link href="css/widget.css" rel="stylesheet">
	<!-- Typography CSS -->
	<link href="css/typography.css" rel="stylesheet">
	<!-- Shortcodes CSS -->
	<link href="css/shortcodes.css" rel="stylesheet">
	<!-- Custom Main StyleSheet CSS -->
	<link href="style.css" rel="stylesheet">
	<!-- Color CSS -->
	<link href="css/color.css" rel="stylesheet">
	<!-- Responsive CSS -->
	<link href="css/responsive.css" rel="stylesheet">
	<!-- SELECT MENU -->
	<link href="css/selectric.css" rel="stylesheet">
	<!-- SIDE MENU -->
	<link rel="stylesheet" href="css/jquery.sidr.dark.css">
</head>

<body>
	<!--KF KODE WRAPPER WRAP START-->
    <div class="kode_wrapper">
    
    	<!--HEADER START-->
            <?php require_once("inc_header.php"); ?>
	<!--HEADER END-->


		<div class="edu2_main_bn_wrap">
			<div id="owl-demo-main" class="owl-carousel owl-theme">
<?php
foreach($bannerslist as $row)
{
?>            
				<div class="item">
					<figure style="max-height: 600px; overflow: hidden;">
						<img src="pictures/banners/<?php echo $row['banner_id'] ?>.jpg" alt=""/>
						<figcaption>
							<h2><?php echo $row['name'] ?></h2>
							<p><?php echo $row['caption'] ?></p>
                                                        <?php if(strlen(trim($row['link'])) > 5) { ?><a href="<?php echo $row['link'] ?>" target="_blank" class="btn-1">read more</a><?php } ?>
						</figcaption>
					</figure>
				</div>
<?php
}
?>
                            
			</div>
		</div>


		<div class="kf_content_wrap">

			<!--KF COURSES CATEGORIES WRAP START-->
			<section>
				<div class="container">
					<div class="row">
						<!-- HEADING 1 START-->
						<div class="col-md-12">
				<div class="kf_intro_des_wrap kf_edu2_intro_wrap">
					<!-- HEADING 2 START-->
					<div class="col-md-12">
						<div class="kf_edu2_heading2">
							<h3><?php echo $contentsdetails['home_welcome_title'] ?></h3>
						</div>
					</div>
					<!-- HEADING 2 END-->
					<!-- INTERO DES START-->
					<div class="kf_intro_des">
						<div class="kf_intro_des_caption">
							<span><i class="icon-book200"></i></span>
							<h6><?php echo $contentsdetails['home_box1_title'] ?></h6>
							<p style="height: 120px;overflow: hidden;"><?php echo $contentsdetails['home_box1_caption'] ?></p>
							<a href="aboutus.php">view more</a>
						</div>
						<figure style="height: 250px;">
							<img src="pictures/home_box1_image.jpg" alt=""/>
						</figure>
					</div>
					<!-- INTERO DES END-->
					<!-- INTERO DES START-->
					<div class="kf_intro_des">
						<div class="kf_intro_des_caption">
							<span><i class="icon-book200"></i></span>
							<h6><?php echo $contentsdetails['home_box2_title'] ?></h6>
							<p style="height: 120px;overflow: hidden;"><?php echo $contentsdetails['home_box2_caption'] ?></p>
                                                        <a href="aboutus.php">view more</a>
						</div>
						<figure style="height: 250px;">
							<img src="pictures/home_box2_image.jpg" alt=""/>
						</figure>
					</div>
					<!-- INTERO DES END-->

					<!-- INTERO DES START-->
					<div class="kf_intro_des">
						<div class="kf_intro_des_caption">
							<span><i class="icon-book200"></i></span>
							<h6><?php echo $contentsdetails['home_box3_title'] ?></h6>
							<p style="height: 120px;overflow: hidden;"><?php echo $contentsdetails['home_box3_caption'] ?></p>
                                                        <a href="aboutus.php">view more</a>
						</div>
						<figure style="height: 250px;">
							<img src="pictures/home_box3_image.jpg" alt=""/>
						</figure>
					</div>
					<!-- INTERO DES END-->
				</div>
						</div>
						<!-- HEADING 1 END-->
						<!--EDU2 COURSES TAB WRAP END-->
					</div>
				</div>
			</section>
			<!--KF COURSES CATEGORIES WRAP END-->
			
			<!-- LATEST NEWS AND EVENT WRAP START-->
			<section class="edu2_new_wrap">
				<div class="container">
					<!-- HEADING 2 START-->
					<div class="col-md-12">
						<div class="kf_edu2_heading2">
							<h3>Latest News &amp; Event</h3>
						</div>
					</div>
					<!-- HEADING 2 END-->
					<div class="row">
                                                <?php
                                                    $count = 0;
                                                    foreach($newslist as $row)
                                                    {
                                                        $count++;
                                                ?>
						<!-- EDU2 NEW DES START-->
						<div class="col-md-6">
							<div class="edu2_new_des">
								<div class="row">
                                                                    <?php if($count%2 == 0) { ?>
									<div class="col-md-6 col-sm-6 thumb" style="height: 195px; overflow: hidden;">
										<figure><img src="pictures/news/<?php echo $row['news_id'] ?>.jpg" alt=""/>
											<figcaption><a href="news-view.php?code=<?php echo $row['news_id'] ?>"><i class="fa fa-plus"></i></a></figcaption>
										</figure>
									</div>
                                                                    <?php } ?>
									<div class="col-md-6 col-sm-6">
										<div class="edu2_event_des">
											<h4><?php echo date("M",strtotime($row['thedate'])) ?></h4>
											<p><?php echo $row['headline'] ?></p>
											<a href="news-view.php?code=<?php echo $row['news_id'] ?>" class="readmore">read more<i class="fa fa-long-arrow-right"></i></a>
											<span>24</span>
										</div>
									</div>
                                                                    <?php if($count%2 != 0) { ?>
									<div class="col-md-6 col-sm-6 thumb" style="height: 195px; overflow: hidden;">
										<figure><img src="pictures/news/<?php echo $row['news_id'] ?>.jpg" alt=""/>
											<figcaption><a href="news-view.php?code=<?php echo $row['news_id'] ?>"><i class="fa fa-plus"></i></a></figcaption>
										</figure>
									</div>
                                                                    <?php } ?>
								</div>
							</div>
						</div>
						<!-- EDU2 NEW DES END-->
                                                <?php 
                                                    } 
                                                ?>                    


					</div>
				</div>
			</section>
			<!-- LATEST NEWS AND EVENT WRAP END-->
			<!--KF COURSES CATEGORIES WRAP START-->
			<section>
				<div class="container">
					<div class="row">
                                            <div class="col-md-8">
						<!-- HEADING 1 START-->
						<div class="col-md-12">
							<div class="kf_edu2_heading1">
								<h3>Video Sermons</h3>
							</div>
						</div>
						<!-- HEADING 1 END-->
						<div class="kf_edu2_tab_des">
                                                <?php
                                                    $count = 0;
                                                    foreach($sermonslist as $row)
                                                    {
                                                        $count++;
                                                ?>
                                                    
                                                <!-- EDU COURSES WRAP START -->			
                                                    <div class="col-md-4">
                                                        <div class="edu2_cur_wrap">
                                                            <figure>
                                                                <img src="pictures/sermons/<?php echo $row['sermon_id'] ?>.jpg" alt="">
                                                                <figcaption><a href="sermons-view.php?code=<?php echo $row['sermon_id'] ?>">See More</a></figcaption>
                                                            </figure>
                                                            <div class="edu2_cur_des">
                                                                <h5><a href="sermons-view.php?code=<?php echo $row['sermon_id'] ?>"><?php echo $row['headline'] ?></a></h5>
                                                                <strong>
                                                                    <span><?php echo date("M d, Y",strtotime($row['thedate'])) ?></span>
                                                                </strong>
                                                                <p><?php echo $row['caption'] ?></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <!-- EDU COURSES WRAP END -->
                                                <?php
                                                    }
                                                ?>
						</div>
						<!--EDU2 COURSES TAB WRAP END-->
                                            </div>
                                            <div class="col-md-4">
                                                <div class="col-md-12">
							<div class="kf_edu2_heading1">
								<h3>MP3 Sermons</h3>
							</div>
                                                      <div class="abt_univ_wrap">
                                             <?php
                                               foreach($mp3sermonslist as $row)
                                    {
                                            ?>
                                            <div class="blog_3_wrap">
    							<!--BLOG 3 SIDE BAR START-->
    							<ul class="blog_3_sidebar">
    								<li>
										<a href="sermons-view-mp3.php?code=<?php echo $row['sermon_id'] ?>">
											<?php echo date("d",strtotime($row['thedate'])) ?>
											<span><?php echo date("M",strtotime($row['thedate'])) ?></span>
										</a>
									</li>
    							</ul>
    							<!--BLOG 3 SIDE BAR END-->
    							<!--BLOG 3 DES START-->
								<div class="">
                                                                    <a href="pictures/sermons_mp3/<?php echo $row['sermon_id'] ?>.mp3" target="_blank"><h5><?php echo $row['title'] ?></h5></a>
                                                                    <p>by <?php echo $row['minister'] ?> - <?php echo formatDate($row['thedate']) ?></p>
                                                                    <div style="float: right; width: 50%">
                                                                        <a href="pictures/sermons_mp3/<?php echo $row['sermon_id'] ?>.mp3" target="_blank"><span style="text-decoration: underline; color: #000000">Play </span></a> || 
                                                                        <a href="pictures/sermons_mp3/<?php echo $row['sermon_id'] ?>.mp3" download><span style="text-decoration: underline; color: #000000"> Download </span></a>
                                                                    </div>
                                                                    <audio controls preload="none" style="width: 50%; float: left">
                                                                            <source src="pictures/sermons_mp3/<?php echo $row['sermon_id'] ?>.mp3" type="audio/mp3">
                                                                            Your browser does not support the audio element.
                                                                        </audio> 
									
								</div>
								<!--BLOG 3 DES END-->
    						</div>
                                            <?php
                                    }
                                    ?>
                                        </div>
						</div>
                                            </div>
					</div>
				</div>
			</section>
			<!--KF COURSES CATEGORIES WRAP END-->
                        <!--KF COURSES CATEGORIES WRAP START-->
			<section>
				<div class="container">
					<div class="row">
                                            <div class="col-md-6">
						<!-- HEADING 1 START-->
							<div class="kf_edu2_heading1">
								<h3>Birthdays this week</h3>
							</div>
						<!-- HEADING 1 END-->
                                                <div class="kf_edu2_tab_des" style="padding-bottom: 70px">
                                                <?php
                                                    $count = 0;
                                                    $monday = date("d",strtotime('monday this week'));
                                                    $sunday = date("d",strtotime('sunday this week'));
                                                    foreach($birthdaylist as $row)
                                                    {
                                                        if (date("d", strtotime($row["birthdaydate"])) >= $monday && date("d", strtotime($row["birthdaydate"])) <= $sunday)
                                                        {
                                                            
                                                        $count++;
                                                ?>
                                                    
                                                <!-- EDU COURSES WRAP START -->			
                                                    <div class="blog_3_wrap">
    							<!--BLOG 3 SIDE BAR START-->
    							<ul class="blog_3_sidebar">
    								<li>
										<a href="#">
											<?php echo date("d",strtotime($row['birthdaydate'])) ?>
											<span><?php echo date("M",strtotime($row['birthdaydate'])) ?></span>
                                                                                </a>
									</li>
    							</ul>
    							<!--BLOG 3 SIDE BAR END-->
    							<!--BLOG 3 DES START-->
								<div class="">
                                                                    <h5><?php echo $row['lastname']. " ".$row['firstname'] ?></h5>
                                                                    
                                                                    
								</div>
								<!--BLOG 3 DES END-->
    						</div>
                                                <!-- EDU COURSES WRAP END -->
                                                <?php
                                                    }
                                                    }
                                                        
                                                        ?>
                                                <a href="birthdays-view.php" class="readmore">birthday this month<i class="fa fa-long-arrow-right"></i></a>
						</div>
						<!--EDU2 COURSES TAB WRAP END-->
                                                
					</div>
                                            <div class="col-md-6">
                                                <!-- HEADING 1 START-->
							<div class="kf_edu2_heading1">
								<h3>Team and Leaders</h3>
							</div>
                                                <div class="panel-body table-responsive" style="max-height: 250px">
                                                <table id="tbl_lgas" class="table table-bordered table-striped">
                                                  <thead>
                                                    <tr>
                                                      <th>Team</th>
                                                      <th>Composition</th>
                                                      <th>Team Leader</th>
                                                    </tr>
                                                  </thead>
                                                  <tbody>
                                                <?php
                                                    $count = 0;
                                                    foreach($teamlist as $row)
                                                    {
                                                      
                                                ?>
                                                    
                                                 <tr>
                                                    <td><?php echo $row['name'] ?></td>
                                                    <td><?php echo $row['content'] ?></a></td>
                                                    <td><?php echo $row['leader'] ?></a></td>
                                                  </tr>
                                                <?php
                                                }
                                                ?>                          
                                                    </tbody>
                                                    </table>
                                                </div>
                                                <!-- /.box-body -->
						</div>
						<!--EDU2 COURSES TAB WRAP END-->
                                            </div>
                                        </div>
				</div>
			</section>
			<!--KF COURSES CATEGORIES WRAP END-->

		</div>
        <?php require_once("inc_footer.php"); ?>

    </div>
    <!--KF KODE WRAPPER WRAP END-->
	<!--Bootstrap core JavaScript-->
	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<!--Bx-Slider JavaScript-->
	<script src="js/jquery.bxslider.min.js"></script>
	<!--Owl Carousel JavaScript-->
	<script src="js/owl.carousel.min.js"></script>
	<!--Pretty Photo JavaScript-->
	<script src="js/jquery.prettyPhoto.js"></script>
	<!--Full Calender JavaScript-->
	<script src="js/moment.min.js"></script>
	<script src="js/fullcalendar.min.js"></script>
	<script src="js/jquery.downCount.js"></script>
	<!--Image Filterable JavaScript-->
	<script src="js/jquery-filterable.js"></script>
	<!--Accordian JavaScript-->
	<script src="js/jquery.accordion.js"></script>
	<!--Number Count (Waypoints) JavaScript-->
	<script src="js/waypoints-min.js"></script>
	<!--v ticker-->
	<script src="js/jquery.vticker.min.js"></script>
	<!--select menu-->
	<script src="js/jquery.selectric.min.js"></script>
	<!--Side Menu-->
	<script src="js/jquery.sidr.min.js"></script>
	<!--Custom JavaScript-->
	<script src="js/custom.js"></script>

    
</body>
</html>
